## Version 2.1.0:
- Refactored the library to be header-only.
- Removed `CL_Program::GetBinarySizes()`. Compensated for this by changing the argument in `CL_Program::GetBinaries()` to a vector of dynamic string objects.
- Added `CL_Context::CreateFromType()` for creating a CL context from a specific device type.
- Fixed the author name.
- Fixed the 'fastdebug' build configuration.
- Fixed compiler warnings.
- Minor code style cleanup.

## Version 2.0.0:

- Completely rewrote the entire library.  The old API will no longer compile with under version 2.0 and up.
- Removed the "object pool" types in favor of `CLcpp::Vector`, which by default is aliased to `std::vector`.
- Removed the requirement of manual memory management from strings returned from objects by adding `CLcpp::String`, which by default is aliased to `std::string`.
- Unified the build system by switching to csbuild.
- Added support for easily switching between OpenCL versions.  Refer the section labeled **VERSION SWITCHING** in the README for more details.
- Added support for some OpenCL 2.0 and 2.1 features.
- Replaced FreeGLUT with GLFW in the `particles` demo.


## Version 1.1.1:

- Changed the name and description of the 3rd argument in `CL_Kernel::SetArg(cl_uint, const void*, cl_uint)` to be more descriptive of its actual purpose.
- Modified how the local work size of `CL_Kernel` is handled.  Now, simply set each dimension to 0 to have OpenCL determine the correct work load per group (equivalent of passing in `NULL` for the local work size in `clEnqueueNDRangeKernel()`).


## Version 1.1.0:

- Added partial compliance for OpenCL 1.2.  Most new features have been added, but some have been intentionally left out to be added in a future update.
- Added `CL_MemObjectPool` for building arrays of `cl_mem` objects.  Currently, this is only needed by the new method `CL_CommandQueue::EnqueueMigrateMemObjects()`, but it will remain as part of the core CLcpp API regardless of version since no part of it depends on version 1.2.
- Added the ability to enable compiling deprecated OpenCL 1.1 features in CLcpp.  When not enabled, their CLcpp counterparts will also not be compiled.
- Added a temporary work-around for OpenCL distributions that contain headers for OpenCL 1.2, but only have libs for 1.1 (see `CLcpp_internal.h` for more information).
- `CL_Image2D` and `CL_Image3D` are being deprecated, but will remain functional.  The reason being that the OpenCL 1.2 spec has changed how images are created in addition to adding new image types.  Now, all images are created through the function `clCreateImage()` rather than having separate functions for each type.  In a future CLcpp update, a new, generic image class (`CL_Image`) will be officially replacing its deprecated counterparts.


## Version 1.0.5:

- Added support for compiling under OpenCL 1.0.


## Version 1.0.4:

- Corrected some variable in `CL_Kernel` that we mistakenly `cl_uint` instead of `size_t`.
- Added temporary support for deprecated functionality when building under OpenCL 1.2 (this will be changed to be more streamlined with the next update).


## Version 1.0.3:

- Fixed a bug in `CL_EventPool::RemoveEvent()`.  The loop over the event list was not iterating properly and would have resulted in an infinite loop.  Also, added some comments to the methods in that class.


## Version 1.0.2:

- Fixed `CL_Free()` which was incorrectly attempting to return a value.
- Changed how `CL_Program::GetBuildLog()` is used.  Before, it would initialize the string to contain the build log and the programmer would be required to free the string.  Now, when calling this method, the string must be initialized beforehand (how it's initialized doesn't matter as long as it is).  To get the exact length of the build log, use `CL_Program::GetBuildLogLength()`.


## Version 1.0.1:

- Added `set_path.bat`.  When executed, it will set the current directory to the environment variable `CLCPP_ROOT_PATH`.
- Fixed the line endings for each solution file.
- Fixed up the Visual Studio 2005 & 2008 solutions.  They now match the output of the 2010 solution (outputing both static and dynamic libraries).
- Corrected the peak allocation size in `opencl_test`.  It was incorrectly adding each input size to the peak size rather than only setting it to the largest input size.
- Fixed a memory leak in `opencl_test`.  When displaying the program build log, the string allocated for the log itself was never being freed.
- Fixed the path to the `array.cl` program file in `opencl_test`.


## Version 1.0.0:

- Changed licensing from older pre-release versions to the zlib/libpng license.