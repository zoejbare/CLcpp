/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

#ifdef __APPLE__
	// Apple has their own OpenCL framework, so we can include from that directly.
	#include <OpenCL/cl.h>
	#include <OpenCL/cl_ext.h>
#else
	// Include the embedded OpenCL headers.
	#include <CL/cl.h>
	#include <CL/cl_ext.h>
#endif

// Include all required STL headers.
#include <functional>
#include <string>
#include <vector>

#ifndef CL_CPP_ENABLE_DEPRECATED_1_0_APIS
	#define CL_CPP_ENABLE_DEPRECATED_1_0_APIS 0
#endif

#ifdef CL_VERSION_1_1
	#define CL_CPP_ENABLE_OPENCL_1_1          1
	#define CL_CPP_ENABLE_DEPRECATED_1_1_APIS 0
#endif

#if defined(CL_VERSION_1_2) && CL_CPP_ENABLE_OPENCL_1_1
	#define CL_CPP_ENABLE_OPENCL_1_2          1
	#define CL_CPP_ENABLE_DEPRECATED_1_2_APIS 0
#endif

#if defined(CL_VERSION_2_0) && CL_CPP_ENABLE_OPENCL_1_2
	#define CL_CPP_ENABLE_OPENCL_2_0          1
	#define CL_CPP_ENABLE_DEPRECATED_2_0_APIS 0
#endif

#if defined(CL_VERSION_2_1) && CL_CPP_ENABLE_OPENCL_2_0
	#define CL_CPP_ENABLE_OPENCL_2_1          1
	#define CL_CPP_ENABLE_DEPRECATED_2_1_APIS 0
#endif

#ifndef CL_VERSION_1_1
	// CL_CALLBACK isn't defined until OpenCL 1.1.  And prior to version 1.1, callbacks were not __stdcall on Windows.
	#define CL_CALLBACK
#endif

#ifndef CL_VERSION_1_2
	// The cl_image_desc structure didn't exist until OpenCL 1.2, but since we use it in our API for creating images,
	// we should ensure that it exists in versions prior to that.  This version of the struct was copied from the
	// OpenCL 2.1 header.  Documentation has been added to the struct members, but only applies to OpenCL versions
	// prior to 1.2.
	typedef struct _cl_image_desc {
		cl_mem_object_type      image_type; //!< Should be either CL_MEM_OBJECT_IMAGE2D or CL_MEM_OBJECT_IMAGE3D.
		size_t                  image_width; //!< Image width.
		size_t                  image_height; //!< Image height.
		size_t                  image_depth; //!< Image depth.  Is assumed to be 1 for 2D images.
		size_t                  image_array_size; //!< Unused.
		size_t                  image_row_pitch; //!< Length in pixels of a single row in a 2D image.
		size_t                  image_slice_pitch; //!< Size in bytes of a 2D slice of a 3D image.  Is assumed to be 0 for 2D images.
		cl_uint                 num_mip_levels; //!< Unused.
		cl_uint                 num_samples; //!< Unused.
		#ifdef __GNUC__
			__extension__   /* Prevents warnings about anonymous union in -pedantic builds */
		#endif
		union {
			cl_mem              buffer;
			cl_mem              mem_object;
		}; //!< Unused.
	} cl_image_desc;
	#define CL_INVALID_IMAGE_DESCRIPTOR  -65
#endif

#define CL_CPP_GET_INFO_CSTRING_1ARG(info_func_, obj1_, enum_, var_, errCode_) \
	{ \
		size_t infoSize = 0; \
		(errCode_) = info_func_((obj1_), (enum_), 0, nullptr, &infoSize); \
		if(infoSize > 0) \
		{ \
			char* const temp__ = (char*) CLcpp::Util::Alloc(infoSize + 1); \
			if(temp__) \
			{ \
				temp__[infoSize] = 0; \
				(errCode_) = info_func_((obj1_), (enum_), infoSize, temp__, nullptr); \
			} \
			(var_) = temp__; \
		} \
		else \
		{ \
			(var_) = nullptr; \
		} \
	}

#define CL_CPP_GET_INFO_CSTRING_2ARG(info_func_, obj1_, obj2_, enum_, var_, errCode_) \
	{ \
		size_t infoSize = 0; \
		(errCode_) = info_func_((obj1_), (obj2_), (enum_), 0, nullptr, &infoSize); \
		if(infoSize > 0) \
		{ \
			char* const temp__ = (char*) CLcpp::Util::Alloc(infoSize + 1); \
			if(temp__) \
			{ \
				temp__[infoSize] = 0; \
				(errCode_) = info_func_((obj1_), (obj2_), (enum_), infoSize, temp__, nullptr); \
			} \
			(var_) = temp__; \
		} \
		else \
		{ \
			(var_) = nullptr; \
		} \
	}

#define CL_CPP_GET_INFO_ARRAY(out_vector_, array_type_, info_func_, obj_, info_, errCode_) \
	{ \
		(out_vector_).clear(); \
		size_t sizeInBytes = 0; \
		(errCode_) = info_func_(obj_, info_, 0, nullptr, &sizeInBytes); \
		if(sizeInBytes > 0) \
		{ \
			const size_t numItems = sizeInBytes / sizeof(array_type_); \
			array_type_* const itemArray = (array_type_*) Util::Alloc(sizeInBytes); \
			(out_vector_).resize(numItems); \
			(errCode_) = info_func_(obj_, info_, sizeInBytes, itemArray, nullptr); \
			for(size_t index = 0; index < numItems; ++index) \
			{ \
				(out_vector_)[index] = itemArray[index]; \
			} \
			Util::Free(itemArray); \
		} \
	}

#define CL_CPP_GET_INFO_ARRAY_CUSTOM_TYPE(out_vector_, array_type_, out_type_, info_func_, obj_, info_, errCode_) \
	{ \
		(out_vector_).clear(); \
		size_t sizeInBytes = 0; \
		(errCode_) = info_func_(obj_, info_, 0, nullptr, &sizeInBytes); \
		if(sizeInBytes > 0) \
		{ \
			const size_t numItems = sizeInBytes / sizeof(array_type_); \
			array_type_* const itemArray = (array_type_*) Util::Alloc(sizeInBytes); \
			(out_vector_).resize(numItems); \
			(errCode_) = info_func_(obj_, info_, sizeInBytes, itemArray, nullptr); \
			for(size_t index = 0; index < numItems; ++index) \
			{ \
				(out_vector_)[index] = out_type_(itemArray[index]); \
			} \
			Util::Free(itemArray); \
		} \
	}
