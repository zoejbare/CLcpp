/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

namespace CLcpp
{
	class Platform;
}

class CLcpp::Platform
{
public:

	Platform();
	Platform(const cl_platform_id id);

	operator cl_platform_id() const;

	/**
	 * @brief   Load all available platforms.
	 * @return  OpenCL error code returned by clGetPlatformIDs();
	 * @param[out]  outPlatformVector  Output parameter for containing each available platform.
	 */
	static cl_int Load(std::vector<Platform>& outPlatformVector);

	/**
	 * @brief    Retrieve the profile name supported by this platform's implementation.
	 * @details  Corresponds to CL_PLATFORM_PROFILE.
	 * @return   OpenCL error code returned by clGetPlatformInfo().
	 * @param[out]  out  Output parameter for profile name.
	 */
	cl_int GetProfile(std::string& out) const;

	/**
	 * @brief    Retrieve the OpenCL version string.
	 * @details  Corresponds to CL_PLATFORM_VERSION.
	 * @return   OpenCL error code returned by clGetPlatformInfo().
	 * @param[out]  out  Output parameter for OpenCL version string.
	 */
	cl_int GetVersion(std::string& out) const;

	/**
	 * @brief    Retrieve the name of this platform.
	 * @details  Corresponds to CL_PLATFORM_NAME.
	 * @return   OpenCL error code returned by clGetPlatformInfo().
	 * @param[out]  out  Output parameter for platform name.
	 */
	cl_int GetName(std::string& out) const;

	/**
	 * @brief    Retrieve the vendor string for this platform.
	 * @details  Corresponds to CL_PLATFORM_VENDOR.
	 * @return   OpenCL error code returned by clGetPlatformInfo().
	 * @param[out]  out  Output parameter for vendor string.
	 */
	cl_int GetVendor(std::string& out) const;

	/**
	 * @brief    Retrieve the extensions supported by this platform.
	 * @details  Corresponds to CL_PLATFORM_EXTENSIONS.
	 * @return   OpenCL error code returned by clGetPlatformInfo().
	 * @param[out]  out  Output parameter for extensions string.
	 */
	cl_int GetExtensions(std::string& out) const;


private:

	cl_int prv_getString(std::string&, const cl_platform_info) const;

	cl_platform_id m_id;
};
