/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

namespace CLcpp
{
	class Kernel;
	class Context;
	class Event;
	class MemBuffer;
	class Program;
	class Sampler;
}

class CLcpp::Kernel
{
public:

	Kernel();
	Kernel(const Kernel& other);
	Kernel(Kernel&& other);
	explicit Kernel(cl_kernel kernel);
	~Kernel();

	operator cl_kernel() const;
	Kernel& operator=(const Kernel& other);
	Kernel& operator=(Kernel&& other);

	/**
	 * @brief   Create a single kernel object from a program.
	 * @return  OpenCL error code returned by clCreateKernel.
	 * @param[out]  outKernel     Output parameter for the new kernel object.
	 * @param[in]   program       Program containing the kernel function.
	 * @param[in]   functionName  Name of the kernel function within the program.
	 */
	static cl_int Create(Kernel& outKernel, const Program& program, const char* functionName);

	/**
	 * @brief   Create objects for all kernels within a program.
	 * @return  OpenCL error code returned by clCreateKernelsInProgram.
	 * @param[out]  outKernelVector  Vector containing the new kernel objects.
	 * @param[in]   program          Program containing kernel functions.
	 */
	static cl_int CreateAll(std::vector<Kernel>& outKernelVector, const Program& program);

	/**
	 * @brief    Retrieve the name of this kernel's function.
	 * @details  Corresponds to CL_KERNEL_FUNCTION_NAME.
	 * @return   OpenCL error code returned by clGetKernelInfo().
	 * @param[out]  out  Output parameter for kernel function's name.
	 */
	cl_int GetFunctionName(std::string& out) const;

	/**
	 * @brief    Retrieve the number of parameter arguments on this kernel's function.
	 * @details  Corresponds to CL_KERNEL_NUM_ARGS.
	 * @return   OpenCL error code returned by clGetKernelInfo().
	 * @param[out]  out  Output parameter for kernel function's number of parameter arguments.
	 */
	cl_int GetNumArgs(cl_uint& out) const;

	/**
	 * @brief    Retrieve the current reference count for this kernel.
	 * @details  Corresponds to CL_KERNEL_REFERENCE_COUNT.
	 * @return   OpenCL error code returned by clGetKernelInfo().
	 * @param[out]  out  Output parameter for kernel's reference count.
	 */
	cl_int GetReferenceCount(cl_uint& out) const;

	/**
	 * @brief    Retrieve the context associated with this kernel.
	 * @details  Corresponds to CL_KERNEL_CONTEXT.
	 * @return   OpenCL error code returned by clGetKernelInfo().
	 * @param[out]  out  Output parameter for kernel's context.
	 */
	cl_int GetContext(Context& out) const;

	/**
	 * @brief    Retrieve the program associated with this kernel.
	 * @details  Corresponds to CL_KERNEL_PROGRAM.
	 * @return   OpenCL error code returned by clGetKernelInfo().
	 * @param[out]  out  Output parameter for kernel's associated program.
	 */
	cl_int GetProgram(Program& out) const;

	/**
	 * @brief   Set a parameter argument on the kernel function.
	 * @return  OpenCL error code returned by clSetKernelArg().
	 * @param[in]  argIndex  Argument index.
	 * @param[in]  memBuffer  Argument as a buffer object.
	 */
	cl_int SetArgument(const cl_uint argIndex, const MemBuffer& memBuffer) const;

	/**
	 * @brief   Set a parameter argument on the kernel function.
	 * @return  OpenCL error code returned by clSetKernelArg().
	 * @param[in]  argIndex  Argument index.
	 * @param[in]  memBuffer  Argument as a sampler object.
	 */
	cl_int SetArgument(const cl_uint argIndex, const Sampler& sampler) const;

	/**
	 * @brief   Set a parameter argument on the kernel function.
	 * @return  OpenCL error code returned by clSetKernelArg().
	 * @param[in]  argIndex  Argument index.
	 * @param[in]  data      Argument as a cl_char.
	 */
	cl_int SetArgument(const cl_uint argIndex, const cl_char data) const;

	/**
	 * @brief   Set a parameter argument on the kernel function.
	 * @return  OpenCL error code returned by clSetKernelArg().
	 * @param[in]  argIndex  Argument index.
	 * @param[in]  data      Argument as a cl_uchar.
	 */
	cl_int SetArgument(const cl_uint argIndex, const cl_uchar data) const;

	/**
	 * @brief   Set a parameter argument on the kernel function.
	 * @return  OpenCL error code returned by clSetKernelArg().
	 * @param[in]  argIndex  Argument index.
	 * @param[in]  data      Argument as a cl_short.
	 */
	cl_int SetArgument(const cl_uint argIndex, const cl_short data) const;

	/**
	 * @brief   Set a parameter argument on the kernel function.
	 * @return  OpenCL error code returned by clSetKernelArg().
	 * @param[in]  argIndex  Argument index.
	 * @param[in]  data      Argument as a cl_ushort.
	 */
	cl_int SetArgument(const cl_uint argIndex, const cl_ushort data) const;

	/**
	 * @brief   Set a parameter argument on the kernel function.
	 * @return  OpenCL error code returned by clSetKernelArg().
	 * @param[in]  argIndex  Argument index.
	 * @param[in]  data      Argument as a cl_int.
	 */
	cl_int SetArgument(const cl_uint argIndex, const cl_int data) const;

	/**
	 * @brief   Set a parameter argument on the kernel function.
	 * @return  OpenCL error code returned by clSetKernelArg().
	 * @param[in]  argIndex  Argument index.
	 * @param[in]  data      Argument as a cl_uint.
	 */
	cl_int SetArgument(const cl_uint argIndex, const cl_uint data) const;

	/**
	 * @brief   Set a parameter argument on the kernel function.
	 * @return  OpenCL error code returned by clSetKernelArg().
	 * @param[in]  argIndex  Argument index.
	 * @param[in]  data      Argument as a cl_long.
	 */
	cl_int SetArgument(const cl_uint argIndex, const cl_long data) const;

	/**
	 * @brief   Set a parameter argument on the kernel function.
	 * @return  OpenCL error code returned by clSetKernelArg().
	 * @param[in]  argIndex  Argument index.
	 * @param[in]  data      Argument as a cl_ulong.
	 */
	cl_int SetArgument(const cl_uint argIndex, const cl_ulong data) const;

	/**
	 * @brief   Set a parameter argument on the kernel function.
	 * @return  OpenCL error code returned by clSetKernelArg().
	 * @param[in]  argIndex  Argument index.
	 * @param[in]  data      Argument as a cl_float.
	 */
	cl_int SetArgument(const cl_uint argIndex, const cl_float data) const;

	/**
	 * @brief   Set a parameter argument on the kernel function.
	 * @return  OpenCL error code returned by clSetKernelArg().
	 * @param[in]  argIndex  Argument index.
	 * @param[in]  data      Argument as a cl_double.
	 */
	cl_int SetArgument(const cl_uint argIndex, const cl_double data) const;

	/**
	 * @brief   Set a parameter argument on the kernel function.
	 * @return  OpenCL error code returned by clSetKernelArg().
	 * @param[in]  argIndex  Argument index.
	 * @param[in]  argSize   Size in the bytes of the argument data.
	 * @param[in]  argData   Pointer to the argument data.
	 */
	cl_int SetArgument(const cl_uint argIndex, const size_t argSize, const void* const argData) const;

#if CL_CPP_ENABLE_OPENCL_1_2
	/**
	 * @brief    Retrieve any attributes applied to this kernel's function.
	 * @details  Corresponds to CL_KERNEL_ATTRIBUTES.
	 * @return   OpenCL error code returned by clGetKernelInfo().
	 * @param[out]  out  Output parameter for kernel function's attributes as specified using the __attribute__ qualifier.
	 */
	cl_int GetAttributes(std::string& out) const;
#endif


private:

	cl_int prv_getInfoString(std::string&, const cl_kernel_info) const;

	cl_kernel m_kernel;
};
