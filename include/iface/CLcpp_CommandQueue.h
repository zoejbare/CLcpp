/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

namespace CLcpp
{
	class CommandQueue;
	class Context;
	class Device;
	class Kernel;
	class Event;
	class MemBuffer;
}

class CLcpp::CommandQueue
{
public:

	CommandQueue();
	CommandQueue(const CommandQueue& other);
	CommandQueue(CommandQueue&& other);
	CommandQueue(cl_command_queue queue);
	~CommandQueue();

	operator cl_command_queue() const;
	CommandQueue& operator=(const CommandQueue& other);
	CommandQueue& operator=(CommandQueue&& other);

	/**
	 * @brief    Creates a command queue on a specific device.
	 * @details  When compiling with OpenCL 2.0 or greater, this will internally call clCreateCommandQueueWithProperties().
	 *           Prior to version 2.0, clCreateCommandQueue() will be used instead.  For more information, see documentation
	 *           for either function.
	 * @return   OpenCL error code returned by clCreateCommandQueue() or clCreateCommandQueueWithProperties().
	 * @param[out]  outQueue      Output parameter for the new command queue.
	 * @param[in]   context       Context under which the command queue will be execute commands.
	 * @param[in]   device        Device to where the new command queue commands will be executed.  Must be a device associated with the context parameter.
	 * @param[in]   propertyMask  Bitfield of command queue properties.  Corresponds to property value CL_QUEUE_PROPERTIES.
	 * @param[in]   queueSize     Specifies the size of the device command queue in bytes.  Corresponds to property value CL_QUEUE_SIZE.  This value will be ignored when compiling under any version prior to OpenCL 2.0.
	 */
	static cl_int Create(
		CommandQueue& outQueue,
		const Context& context,
		const Device& device,
		const cl_command_queue_properties propertyMask,
		const cl_uint queueSize
	);

	/**
	 * @brief    Retrieve the context associated with this command queue.
	 * @details  Corresponds to CL_QUEUE_CONTEXT.
	 * @return   OpenCL error code returned by clGetCommandQueueInfo().
	 * @param[out]  out  Output parameter for the command queue's context.
	 */
	cl_int GetContext(Context& out) const;

	/**
	 * @brief    Retrieve the device associated with this command queue.
	 * @details  Corresponds to CL_QUEUE_DEVICE.
	 * @return   OpenCL error code returned by clGetCommandQueueInfo().
	 * @param[out]  out  Output parameter for the command queue's device.
	 */
	cl_int GetDevice(Device& out) const;

	/**
	 * @brief    Retrieve the current reference count for this command queue.
	 * @details  Corresponds to CL_QUEUE_REFERENCE_COUNT.
	 * @return   OpenCL error code returned by clGetCommandQueueInfo().
	 * @param[out]  out  Output parameter for the command queue's reference count.
	 */
	cl_int GetReferenceCount(cl_uint& out) const;

	/**
	 * @brief    Retrieve the properties bitfield for this command queue.
	 * @details  Corresponds to CL_QUEUE_PROPERTIES.
	 * @return   OpenCL error code returned by clGetCommandQueueInfo().
	 * @param[out]  out  Output parameter for the command queue's properties.
	 */
	cl_int GetProperties(cl_command_queue_properties& out) const;

	/**
	 * @brief   Issues all previously queued OpenCL commands in this command queue to the device associated with it.
	 * @return  OpenCL error code returned by clFlush().
	 */
	cl_int Flush() const;

	/**
	 * @brief   Blocks until all previously queued OpenCL commands in this command queue are issued to its associated device and have completed.
	 * @return  OpenCL error code returned by clFinish().
	 */
	cl_int Finish() const;

	/**
	 * @brief   Enqueues a command to execute a kernel on a device.
	 * @return  OpenCL error code returned by clEnqueueNDRangeKernel().
	 * @param[out]  outEvent             Output parameter for the event generated by clEnqueueNDRangeKernel();
	 * @param[in]   waitListEventVector  Vector of events to use as a wait list.
	 * @param[in]   numWorkDimensions    The number of dimensions used to specify the global work items and work items in the work group.  Must be greater than 0 and less than or equal to 3.
	 * @param[in]   globalWorkOffset     An array of values that describe the offset used to calculate the global ID of a work item.  Use nullptr to ignore.
	 * @param[in]   globalWorkSize       An array of values that describe the number of global work items in the specified number of dimensions that will execute the kernel function.
	 * @param[in]   localWorkSize        An array of values that describe the number of work items that make up a work group.  Use nullptr to ignore.
	 */
	cl_int EnqueueNDKernel(
		Event& outEvent,
		const std::vector<Event>& waitListEventVector,
		const Kernel& kernel,
		const cl_uint numWorkDimensions,
		const size_t* globalWorkOffset,
		const size_t* globalWorkSize,
		const size_t* localWorkSize
	) const;

	/**
	 * @brief   Enqueues a command to copy a buffer object to another buffer object.
	 * @return  OpenCL error code returned by clEnqueueCopyBuffer().
	 * @param[out]  outEvent             Output parameter for the event generated by clEnqueueCopyBuffer().
	 * @param[in]   waitListEventVector  Vector of events to use as a wait list for this command.
	 * @param[in]   destBuffer           Destination buffer object.
	 * @param[in]   srcBuffer            Source buffer object.
	 * @param[in]   destOffset           Offset of where to begin copying data within destBuffer.
	 * @param[in]   srcOffset            Offset of where to begin copying data within srcBuffer.
	 * @param[in]   copySize             Total size in bytes of the data to copy.
	 */
	cl_int EnqueueCopyBuffer(
		Event& outEvent,
		const std::vector<Event>& waitListEventVector,
		const MemBuffer& destBuffer,
		const MemBuffer& srcBuffer,
		const size_t destOffset,
		const size_t srcOffset,
		const size_t copySize
	);

	/**
	 * @brief   Enqueue a command to write to a buffer object from host memory.
	 * @return  OpenCL error code returned by clEnqueueWriteBuffer().
	 * @param[out]  outEvent             Output parameter for the event generated by clEnqueueWriteBuffer().
	 * @param[in]   waitListEventVector  Vector of events to use as a wait list.
	 * @param[in]   memBuffer            Buffer object to write data into.
	 * @param[in]   isBlocking           True to block until the operation has completed, false otherwise.
	 * @param[in]   destOffset           Offset in bytes in the buffer object being written to.
	 * @param[in]   srcSize              Size in bytes of data being written.
	 * @param[in]   srcBuffer            Pointer to a buffer in host memory where data is to be written from.
	 */
	cl_int EnqueueWriteBuffer(
		Event& outEvent,
		const std::vector<Event>& waitListEventVector,
		const MemBuffer& memBuffer,
		const bool isBlocking,
		const size_t destOffset,
		const size_t srcSize,
		const void* const srcBuffer
	) const;

	/**
	 * @brief   Enqueues a command to write to an image object from host memory.
	 * @return  OpenCL error code returned by clEnqueueWriteImage().
	 * @param[out]  outEvent             Output parameter for the event generated by clEnqueueWriteImage().
	 * @param[in]   waitListEventVector  Vector of events to use as a wait list.
	 * @param[in]   memBuffer            Image object to write data into.
	 * @param[in]   isBlocking           True to block until the operation has completed, false otherwise.
	 * @param[in]   origin               Defines the (x, y, z) offset in pixels in the image from where to write.  If image is a 2D image object, the z value given by origin[2] must be 0.
	 * @param[in]   regionSize           Defines the (width, height, depth) in pixels of the 2D or 3D rectangle being written.  If image is a 2D image object, the depth value given by region[2] must be 1.
	 * @param[in]   rowPitch             The length of each row in bytes.  This value must be greater than or equal to the element size in bytes * width.  If input_row_pitch is set to 0, the appropriate row pitch is calculated based on the size of each element in bytes multiplied by width.
	 * @param[in]   slicePitch           Size in bytes of the 2D slice of the 3D region of a 3D image being written. This must be 0 if image is a 2D image. This value must be greater than or equal to row_pitch * height.  If input_slice_pitch is set to 0, the appropriate slice pitch is calculated based on the row_pitch * height.
	 * @param[in]   srcImageData         Pointer to a buffer in host memory where image data is to be written from.
	 */
	cl_int EnqueueWriteImage(
		Event& outEvent,
		const std::vector<Event>& waitListEventVector,
		const MemBuffer& memBuffer,
		const bool isBlocking,
		const size_t origin[3],
		const size_t regionSize[3],
		const size_t rowPitch,
		const size_t slicePitch,
		const void* const srcImageData
	) const;

	/**
	 * @brief   Enqueue commands to read data from a buffer object into host memory.
	 * @return  OpenCL error code returned by clEnqueueReadBuffer().
	 * @param[out]  outEvent             Output parameter for the event generated by clEnqueueReadBuffer().
	 * @param[in]   waitListEventVector  Vector of events to use as a wait list.
	 * @param[in]   memBuffer            Buffer object to read data from.
	 * @param[in]   isBlocking           True to block until the operation has completed, false otherwise.
	 * @param[in]   srcOffset            Offset in bytes in the buffer object to read from.
	 * @param[in]   destSize             Size in bytes of data being read.
	 * @param[in]   destBuffer           Pointer to a buffer in host memory where data is to be read into.
	 */
	cl_int EnqueueReadBuffer(
		Event& outEvent,
		const std::vector<Event>& waitListEventVector,
		const MemBuffer& memBuffer,
		const bool isBlocking,
		const size_t srcOffset,
		const size_t destSize,
		void* const destBuffer
	) const;

	/**
	 * @brief   Enqueue commands to read from an image object to host memory.
	 * @return  OpenCL error code returned by clEnqueueReadImage().
	 * @param[out]  outEvent             Output parameter for the event generated by clEnqueueReadImage().
	 * @param[in]   waitListEventVector  Vector of events to use as a wait list.
	 * @param[in]   memBuffer            Buffer object to read data from.
	 * @param[in]   isBlocking           True to block until the operation has completed, false otherwise.
	 * @param[in]   origin               Defines the (x, y, z) offset in pixels in the image from where to read.  If image is a 2D image object, the z value given by origin[2] must be 0.
	 * @param[in]   regionSize           Defines the (width, height, depth) in pixels of the 2D or 3D rectangle being read.  If image is a 2D image object, the depth value given by region[2] must be 1.
	 * @param[in]   rowPitch             The length of each row in bytes. This value must be greater than or equal to the element size in bytes * width.  If row_pitch is set to 0, the appropriate row pitch is calculated based on the size of each element in bytes multiplied by width.
	 * @param[in]   slicePitch           Size in bytes of the 2D slice of the 3D region of a 3D image being read. This must be 0 if image is a 2D image.  This value must be greater than or equal to row_pitch * height.  If slice_pitch is set to 0, the appropriate slice pitch is calculated based on the row_pitch * height.
	 * @param[in]   destImageData        Pointer to a buffer in host memory where the image object data is to be read into.
	 */
	cl_int EnqueueReadImage(
		Event& outEvent,
		const std::vector<Event>& waitListEventVector,
		const MemBuffer& memBuffer,
		const bool isBlocking,
		const size_t origin[3],
		const size_t regionSize[3],
		const size_t rowPitch,
		const size_t slicePitch,
		void* const destImageData
	) const;

	/**
	 * @brief   Enqueues a command to map a region of a buffer object into host address space.
	 * @return  OpenCL error code returned by clEnqueueMapBuffer().
	 * @param[out]  outMappedRegion      Pointer to the mapped region.
	 * @param[out]  outEvent             Output parameter for the event generated by clEnqueueMapBuffer().
	 * @param[in]   waitListEventVector  Vector of events to use as a wait list.
	 * @param[in]   memBuffer            Buffer object to map.
	 * @param[in]   isBlocking           True to block until the operation has completed, false otherwise.
	 * @param[in]   mapFlags             A bitfield describing properties of the mapped memory.
	 * @param[in]   bufferOffset         Offset in bytes in the buffer object that is being mapped.
	 * @param[in]   regionSize           Size in bytes of the region in the buffer object that is being mapped.
	 */
	cl_int EnqueueMapBuffer(
		void** const outMappedRegion,
		Event& outEvent,
		const std::vector<Event>& waitListEventVector,
		const MemBuffer& memBuffer,
		const bool isBlocking,
		const cl_mem_flags mapFlags,
		const size_t bufferOffset,
		const size_t regionSize
	) const;

	/**
	 * @brief   Enqueues a command to map a region of an image object into host address space.
	 * @return  OpenCL error code returned by clEnqueueMapImage().
	 * @param[out]  outMappedRegion      Pointer to the mapped region.
	 * @param[out]  outRowPitch          Output parameter for the scanline pitch in bytes for the mapped region.
	 * @param[out]  outSlicePitch        Output parameter for the size in bytes of each 2D slice for the mapped region.  This value will always be 0 for 2D images.
	 * @param[out]  outEvent             Output parameter for the event generated by clEnqueueMapImage().
	 * @param[in]   waitListEventVector  Vector of events to use as a wait list.
	 * @param[in]   memBuffer            Buffer object to map.
	 * @param[in]   isBlocking           True to block until the operation has completed, false otherwise.
	 * @param[in]   mapFlags             A bitfield describing properties of the mapped memory.
	 * @param[in]   origin               Defines the (x, y, z) offset in pixels in the image.  If image is a 2D image object, the z value given by origin[2] must be 0.
	 * @param[in]   regionSize           Defines the (width, height, depth) in pixels of the 2D or 3D rectangle.  If image is a 2D image object, the depth value given by region[2] must be 1.
	 */
	cl_int EnqueueMapImage(
		void** const outMappedRegion,
		size_t& outRowPitch,
		size_t& outSlicePitch,
		Event& outEvent,
		const std::vector<Event>& waitListEventVector,
		const MemBuffer& memBuffer,
		const bool isBlocking,
		const cl_mem_flags mapFlags,
		const size_t origin[3],
		const size_t regionSize[3]
	) const;

	/**
	 * @brief   Enqueues a command to unmap a previously mapped region of a memory object.
	 * @return  OpenCL error code returned by clEnqueueUnmapMemObject().
	 * @param[out]  outEvent             Output parameter for the event generated by clEnqueueUnmapMemObject().
	 * @param[in]   waitListEventVector  Vector of events to use as a wait list.
	 * @param[in]   memBuffer            Buffer object to unmap.
	 * @param[in]   mappedRegion         Pointer to region of memory that has been mapped from the input buffer object.
	 */
	cl_int EnqueueUnmapMemObject(
		Event& outEvent,
		const std::vector<Event>& waitListEventVector,
		const MemBuffer& memBuffer,
		void* const mappedRegion
	) const;

#if CL_CPP_ENABLE_OPENCL_1_1
	#if CL_CPP_ENABLE_DEPRECATED_1_1_APIS
		/**
		 * @brief    Enqueues a command to act as a synchronization point.
		 * @details  This method internally calls clEnqueueBarrier() which has been deprecated.
		 *           Calling EnqueueBarrierWithWaitList() is preferred.
		 * @return   OpenCL error code returned by clEnqueueBarrier().
		 */
		cl_int EnqueueBarrier() const;

		/**
		 * @brief    Enqueues a wait command for a specific list of events to be completed before any future queued command may be executed.
		 * @details  This method internally calls clEnqueueWaitForEvents() which has been deprecated.
		 *           Calling any method that takes a wait list is preferred.
		 * @return   OpenCL error code returned by clEnqueueWaitForEvents().
		 * @param[in]  waitListEventVector  Vector of events to use as a wait list.
		 */
		cl_int EnqueueWaitForEvents(const std::vector<Event>& waitListEventVector) const;

		/**
		 * @brief    Enqueues a marker command.
		 * @details  This method internally calls clEnqueueMarker() which has been deprecated.
		 *           Calling EnqueueMarkerWithWaitList() is preferred.
		 * @return   OpenCL error code returned by clEnqueueMarker().
		 * @param[out]  outEvent  Output parameter for the event generated by clEnqueueMarker().
		 */
		cl_int EnqueueMarker(Event& outEvent) const;
	#endif
#endif

#if CL_CPP_ENABLE_OPENCL_1_2
	/**
	 * @brief   Enqueues a command to act as a synchronization point.
	 * @return  OpenCL error code returned by clEnqueueBarrierWithWaitList().
	 * @param[out]  outEvent             Output parameter for the event generated by clEnqueueBarrierWithWaitList().
	 * @param[in]   waitListEventVector  Vector of events to use as a wait list.
	 */
	cl_int EnqueueBarrierWithWaitList(Event& outEvent, const std::vector<Event>& waitListEventVector) const;

	/**
	 * @brief   Enqueues a marker command.
	 * @return  OpenCL error code returned by clEnqueueMarkerWithWaitList().
	 * @param[out]  outEvent             Output parameter for the event generated by clEnqueueMarkerWithWaitList().
	 * @param[in]   waitListEventVector  Vector of events to use as a wait list.
	 */
	cl_int EnqueueMarkerWithWaitList(Event& outEvent, const std::vector<Event>& waitListEventVector) const;

	/**
	 * @brief    Enqueues a command to indicate which device a set of memory objects should be associated with.
	 * @details  The command queue used to call this method will be considered the target command queue,
	 *           migrating the set of buffer objects to its associated device.
	 * @return   OpenCL error code returned by clEnqueueMigrateMemObjects().
	 * @param[out]  outEvent             Output parameter for the event generated by clEnqueueMigrateMemObjects().
	 * @param[in]   waitListEventVector  Vector of events to use as a wait list.
	 * @param[in]   memBufferVector      Vector of buffer objects to be migrated.
	 * @param[in]   migrationFlags       Bitfield of flags to configure the migration.
	 */
	cl_int EnqueueMigrateMemObjects(
		Event& outEvent,
		const std::vector<Event>& waitListEventVector,
		const std::vector<MemBuffer>& memBufferVector,
		const cl_mem_migration_flags migrationFlags
	) const;

	/**
	 * @brief   Enqueues a command to fill a buffer object with a pattern of a given size.
	 * @return  OpenCL error code returned by clEnqueueFillBuffer().
	 * @param[out]  outEvent             Output parameter for the event generated by clEnqueueFillBuffer().
	 * @param[in]   waitListEventVector  Vector of events to use as a wait list.
	 * @param[in]   memBuffer            Buffer object to be filled with the input pattern.
	 * @param[in]   patternData          A pointer to the pattern data used to fill memBuffer starting at the input regionOffset
	 *                                   up to the input regionSize.  For more information on the restrictions of the pattern data
	 *                                   format and allowed sizes, refer to the documentation for clEnqueueFillBuffer().
	 * @param[in]   patternSize          Total size in bytes of the pattern data.
	 * @param[in]   regionOffset         Offset of where to begin copying the pattern data into memBuffer.
	 * @param[in]   regionSize           Total size in bytes of the region to fill in memBuffer.
	 */
	cl_int EnqueueFillBuffer(
		Event& outEvent,
		const std::vector<Event>& waitListEventVector,
		const MemBuffer& memBuffer,
		const void* const patternData,
		const size_t patternSize,
		const size_t regionOffset,
		const size_t regionSize
	) const;
#endif

#if CL_CPP_ENABLE_OPENCL_2_0
	/**
	 * @brief    Retrieve the size in bytes of the command queue.
	 * @details  Only applies to device command queues.
	 * @return   OpenCL error code returned by clGetCommandQueueInfo().
	 * @param[out]  out  Output parameter for the command queue's reference count.
	 */
	cl_int GetSize(cl_uint& out) const;
#endif


private:

	cl_command_queue m_queue;
};
