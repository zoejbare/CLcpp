/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

namespace CLcpp
{
	class Event;
	class CommandQueue;
	class Context;

	typedef void (CL_CALLBACK *EventNotifyCallback)(cl_event evt, cl_int commandStatus, void* userData);
}

class CLcpp::Event
{
public:

	Event();
	Event(const Event& other);
	Event(Event&& other);
	explicit Event(cl_event evt);
	~Event();

	operator cl_event() const;
	Event& operator=(const Event& other);
	Event& operator=(Event&& other);

	/**
	 * @brief   Waits on the host thread for commands identified by event objects to complete.
	 * @return  OpenCL error code returned by clWaitForEvents().
	 * @param[in]  eventVector  Vector of events to use as a wait list.
	 */
	static cl_int WaitForEvents(const std::vector<Event>& eventVector);

	/**
	 * @brief   Returns profiling information for the command associated with this event if profiling is enabled.
	 * @return  OpenCL error code returned by clEventProfilingInfo().
	 * @param[out]  out            Output parameter for the selected profiling info.
	 * @param[in]   profilingInfo  Value specifying the profiling data to query.
	 */
	cl_int GetProfileTime(cl_ulong& out, const cl_profiling_info profilingInfo) const;

	/**
	 * @brief    Retrieve the command queue associated with this event.
	 * @details  Corresponds to CL_EVENT_COMMAND_QUEUE.
	 * @return   OpenCL error code returned by clGetEventInfo().
	 * @param[out]  out  Output parameter for the event's command queue.
	 */
	cl_int GetCommandQueue(CommandQueue& out) const;

	/**
	 * @brief    Retrieve the command type identified by this event.
	 * @details  Corresponds to CL_EVENT_COMMAND_TYPE.
	 * @return   OpenCL error code returned by clGetEventInfo().
	 * @param[out]  out  Output parameter for the value identifying the type of command that created the event.
	 */
	cl_int GetCommandType(cl_command_type& out) const;

	/**
	 * @brief    Retrieve the current reference count of this event.
	 * @details  Corresponds to CL_EVENT_REFERENCE_COUNT.
	 * @return   OpenCL error code returned by clGetEventInfo().
	 * @param[out]  out  Output parameter for the event's reference count.
	 */
	cl_int GetReferenceCount(cl_uint& out) const;

	/**
	 * @brief    Retrieve the current execution status of the command identified by this event.
	 * @details  Corresponds to CL_EVENT_COMMAND_EXECUTION_STATUS.
	 * @return   OpenCL error code returned by clGetEventInfo().
	 * @param[out]  out  Output parameter for the event's command execution status.
	 */
	cl_int GetCommandExecutionStatus(cl_int& out) const;

#if CL_CPP_ENABLE_OPENCL_1_1
	/**
	 * @brief    Retrieve the context that owns the command queue associated with this event.
	 * @details  Corresponds to CL_EVENT_CONTEXT.
	 * @return   OpenCL error code returned by clGetEventInfo().
	 * @param[out]  out  Output parameter for the event's context.
	 */
	cl_int GetContext(Context& out) const;

	/**
	 * @brief   Create a user event object.
	 * @return  OpenCL error code returned by clCreateUserEvent().
	 * @param[out]  outEvent  Output parameter for the new user event.
	 * @param[in]   context   Context under which to create the new user event.
	 */
	static cl_int CreateUserEvent(Event& outEvent, const Context& context);

	/**
	 * @brief    Sets the execution status of this user event object.
	 * @details  This method may only be used if the calling event object was created through CreateUserEvent().
	 * @return   OpenCL error code returned by clSetUserEventStatus().
	 * @param[in]  status  New user event status.
	 */
	cl_int SetUserEventStatus(const cl_int status) const;

	/**
	 * @brief   Registers a user callback function for a specific command execution status on this event.
	 * @return  OpenCL error code returned by clSetNotifyCallback().
	 * @param[in]  execStatus  The status value under which to register the callback.
	 * @param[in]  callback    The callback that will be called when the input status has been set on the event.
	 * @param[in]  userData    Pointer to generic data that will be passed to the callback.
	 */
	cl_int SetNotifyCallback(const cl_int execStatus, const EventNotifyCallback callback, void* const userData) const;
#endif


private:

	cl_event m_event;
};
