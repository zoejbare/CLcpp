/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

namespace CLcpp
{
	class Device;
	class Platform;
}

class CLcpp::Device
{
public:

	Device();
	Device(const Device& other);
	Device(Device&& other);
	Device(const cl_device_id id);
	~Device();

	operator cl_device_id() const;
	Device& operator=(const Device& other);
	Device& operator=(Device&& other);

	/**
	 * @brief   Load all available devices of a specific type for a given platform.
	 * @return  OpenCL error code returned by clGetDeviceIDs();
	 * @param[out]  outDeviceVector  Output parameter for containing each available device.
	 * @param[in]   platform         Platform from which to retrieve the devices.
	 * @param[in]   typeMask         Bitfield representing the types of devices to load.
	 */
	static cl_int Load(std::vector<Device>& outDeviceVector, const Platform& platform, const cl_device_type typeMask);

	/**
	 * @brief    Retrieve the type of this device
	 * @details  Correspondes to CL_DEVICE_TYPE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's type.
	 */
	cl_int GetDeviceType(cl_device_type& out) const;

	/**
	 * @brief    Retrieve the unique, vendor supplied ID for this device.
	 * @details  Correspondes to CL_DEVICE_VENDOR_ID.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for device's vendor ID.
	 */
	cl_int GetVendorId(cl_uint& out) const;

	/**
	 * @brief    Retrieve the number of parallel compute units on this device.
	 * @details  Correspondes to CL_DEVICE_MAX_COMPUTE_UNITS.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for device's number of compute units.
	 */
	cl_int GetMaxComputeUnits(cl_uint& out) const;

	/**
	 * @brief    Retrieve the maximum number of dimensions of this device.
	 * @details  Correspondes to CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS.  This value specifies the global and local
	 *           work item IDs used by the data parallel execution model.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for device's maximum number of work dimensions.
	 */
	cl_int GetMaxWorkItemDimensions(cl_uint& out) const;

	/**
	 * @brief    Retrieve the maximum number of work items in a single work group for this device.
	 * @details  Correspondes to CL_DEVICE_MAX_WORK_GROUP_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for device's maximum size of a work group.
	 */
	cl_int GetMaxWorkGroupSize(size_t& out) const;

	/**
	 * @brief    Retrieve the maximum number of work items per dimension of a single work group for this device.
	 * @details  Correspondes to CL_DEVICE_MAX_WORK_ITEM_SIZES.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for device's maximum number of work items per dimension.
	 */
	cl_int GetMaxWorkItemSizes(std::vector<size_t>& out) const;

	/**
	 * @brief    Retrieve this device's implementation preferred vector width for the cl_char scalar type.
	 * @details  Correspondes to CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's preferred cl_char vector width.
	 */
	cl_int GetPreferredVectorWidthChar(cl_uint& out) const;

	/**
	 * @brief    Retrieve this device's implementation preferred vector width for the cl_short scalar type.
	 * @details  Correspondes to CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's preferred cl_short vector width.
	 */
	cl_int GetPreferredVectorWidthShort(cl_uint& out) const;

	/**
	 * @brief    Retrieve this device's implementation preferred vector width for the cl_int scalar type.
	 * @details  Correspondes to CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's preferred cl_int vector width.
	 */
	cl_int GetPreferredVectorWidthInt(cl_uint& out) const;

	/**
	 * @brief    Retrieve this device's implementation preferred vector width for the cl_long scalar type.
	 * @details  Correspondes to CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's preferred cl_long vector width.
	 */
	cl_int GetPreferredVectorWidthLong(cl_uint& out) const;

	/**
	 * @brief    Retrieve this device's implementation preferred vector width for the cl_float scalar type.
	 * @details  Correspondes to CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's preferred cl_float vector width.
	 */
	cl_int GetPreferredVectorWidthFloat(cl_uint& out) const;

	/**
	 * @brief    Retrieve this device's implementation preferred vector width for the cl_double scalar type.
	 * @details  Correspondes to CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's preferred cl_double vector width.
	 */
	cl_int GetPreferredVectorWidthDouble(cl_uint& out) const;

	/**
	 * @brief    Retrieve the maximum configured clock frequency for this device in MHz.
	 * @details  Correspondes to CL_DEVICE_MAX_CLOCK_FREQUENCY.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum clock frequency.
	 */
	cl_int GetMaxClockFrequency(cl_uint& out) const;

	/**
	 * @brief    Retrieve the default address space size as a value in bits for this device.
	 * @details  Correspondes to CL_DEVICE_ADDRESS_BITS.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for device's number of address space bits.
	 */
	cl_int GetAddressBits(cl_uint& out) const;

	/**
	 * @brief    Retrieve this device's maximum number of image objects arguments of a kernel declared with the read_only qualifier.
	 * @details  Correspondes to CL_DEVICE_MAX_READ_IMAGE_ARGS.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum number of read-only image object arguments.
	 */
	cl_int GetMaxReadImageArgs(cl_uint& out) const;

	/**
	 * @brief    Retrieve this device's maximum number of image objects arguments of a kernel declared with the write_only or read_write qualifiers.
	 * @details  Correspondes to CL_DEVICE_MAX_WRITE_IMAGE_ARGS.  This value is provided for backward compatibility.  CL_DEVICE_MAX_READ_WRITE_IMAGE_ARGS should be used instead.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum number of write-only or read/write image object arguments.
	 */
	cl_int GetMaxWriteImageArgs(cl_uint& out) const;

	/**
	 * @brief    Retrieve the maximum allowable size in bytes of a single memory object on this device.
	 * @details  Correspondes to CL_DEVICE_MAX_MEM_ALLOC_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum memory object size.
	 */
	cl_int GetMaxMemAllocSize(cl_ulong& out) const;

	/**
	 * @brief    Retrieve the maximum width of a 2D image on this device.
	 * @details  Correspondes to CL_DEVICE_IMAGE2D_MAX_WIDTH.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum 2D image width.
	 */
	cl_int GetImage2DMaxWidth(size_t& out) const;

	/**
	 * @brief    Retrieve the maximum height of a 2D image on this device.
	 * @details  Correspondes to CL_DEVICE_IMAGE2D_MAX_HEIGHT.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum 2D image height.
	 */
	cl_int GetImage2DMaxHeight(size_t& out) const;

	/**
	 * @brief    Retrieve the maximum width of a 3D image on this device.
	 * @details  Correspondes to CL_DEVICE_IMAGE3D_MAX_WIDTH.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum 3D image width.
	 */
	cl_int GetImage3DMaxWidth(size_t& out) const;

	/**
	 * @brief    Retrieve the maximum height of a 3D image on this device.
	 * @details  Correspondes to CL_DEVICE_IMAGE3D_MAX_HEIGHT.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum 3D image height.
	 */
	cl_int GetImage3DMaxHeight(size_t& out) const;

	/**
	 * @brief    Retrieve the maximum depth of a 3D image on this device.
	 * @details  Correspondes to CL_DEVICE_IMAGE3D_MAX_DEPTH.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum 3D image depth.
	 */
	cl_int GetImage3DMaxDepth(size_t& out) const;

	/**
	 * @brief    Retrieve a boolean value for whether or not images are supported on this device.
	 * @details  Correspondes to CL_DEVICE_IMAGE_SUPPORT.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's image support; CL_TRUE if images are supported, CL_FALSE otherwise.
	 */
	cl_int GetImageSupport(cl_bool& out) const;

	/**
	 * @brief    Retrieve the maximum size in bytes of all arguments that can be passed to a kernel on this device.
	 * @details  Correspondes to CL_DEVICE_MAX_PARAMETER_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum parameter size.
	 */
	cl_int GetMaxParameterSize(size_t& out) const;

	/**
	 * @brief    Retrieve the maximum number of samplers that can be used in a kernel on this device.
	 * @details  Correspondes to CL_DEVICE_MAX_SAMPLERS.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum number of samplers.
	 */
	cl_int GetMaxSamplers(cl_uint& out) const;

	/**
	 * @brief    Retrieve the required alignment (in bits) for sub-buffer offsets on this device.
	 * @details  Correspondes to CL_DEVICE_MEM_BASE_ADDR_ALIGN.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's required sub-buffer alignment in bits.
	 */
	cl_int GetMemBaseAddrAlign(cl_uint& out) const;

#if CL_CPP_ENABLE_DEPRECATED_1_0_APIS
	/**
	 * @brief    Retrieve the minimum alignment in bytes which can be used for any data type on this device.
	 * @details  Correspondes to CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE.  This value was deprecated in OpenCL 1.2.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's minimum data type alignment.
	 */
	cl_int GetMinDataTypeAlignSize(cl_uint& out) const;
#endif

	/**
	 * @brief    Retrieve a bitfield describing the single precision floating point capabilities of this device.
	 * @details  Correspondes to CL_DEVICE_SINGLE_FP_CONFIG.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's single precision floating point capabilities.
	 */
	cl_int GetSingleFpConfig(cl_device_fp_config& out) const;

	/**
	 * @brief    Retrieve the type of global memory cache supported by this device.
	 * @details  Correspondes to CL_DEVICE_GLOBAL_MEM_CACHE_TYPE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's global memory cache type.
	 */
	cl_int GetGlobalMemCacheType(cl_device_mem_cache_type& out) const;

	/**
	 * @brief    Retrieve the size of the global memory cacheline in bytes on this device.
	 * @details  Correspondes to CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's global memory cacheline size.
	 */
	cl_int GetGlobalMemCachelineSize(cl_uint& out) const;

	/**
	 * @brief    Retrieve the total size in bytes of the global memory cache on this device.
	 * @details  Correspondes to CL_DEVICE_GLOBAL_MEM_CACHE_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's global memory cache size.
	 */
	cl_int GetGlobalMemCacheSize(cl_ulong& out) const;

	/**
	 * @brief    Retrieve the total size in bytes of global memory on this device.
	 * @details  Correspondes to CL_DEVICE_GLOBAL_MEM_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's global memory size.
	 */
	cl_int GetGlobalMemSize(cl_ulong& out) const;

	/**
	 * @brief    Retrieve the maximum size in bytes of a single constant buffer allocation on this device.
	 * @details  Correspondes to CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum constant buffer allocation size.
	 */
	cl_int GetMaxConstantBufferSize(cl_ulong& out) const;

	/**
	 * @brief    Retrieve the maximum number of arguments declared with the __constant qualifier in a kernel on this device.
	 * @details  Correspondes to CL_DEVICE_MAX_CONSTANT_ARGS.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum number of constant arguments.
	 */
	cl_int GetMaxConstantArgs(cl_uint& out) const;

	/**
	 * @brief    Retrieve the memory type supported by local memory on this device.
	 * @details  Correspondes to CL_DEVICE_LOCAL_MEM_TYPE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's local memory type.
	 */
	cl_int GetLocalMemType(cl_device_local_mem_type& out) const;

	/**
	 * @brief    Retrieve the total size in bytes of the local memory arena on this device.
	 * @details  Correspondes to CL_DEVICE_LOCAL_MEM_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's local memory size.
	 */
	cl_int GetLocalMemSize(cl_ulong& out) const;

	/**
	 * @brief    Retrieve a boolean value determining whether or not this device implements error correction for all accesses to compute memory.
	 * @details  Correspondes to CL_DEVICE_ERROR_CORRECTION_SUPPORT.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the boolean value; CL_TRUE if error correction is supported by the device, CL_FALSE otherwise.
	 */
	cl_int GetErrorCorrectionSupport(cl_bool& out) const;

	/**
	 * @brief    Retrieve the resolution in terms of nanoseconds of this device's timer.
	 * @details  Correspondes to CL_DEVICE_PROFILING_TIMER_RESOLUTION.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's timer resolution.
	 */
	cl_int GetProfilingTimerResolution(size_t& out) const;

	/**
	 * @brief    Retrieve a boolean value determining whether or not this device is little endian.
	 * @details  Correspondes to CL_DEVICE_ENDIAN_LITTLE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's endianness; CL_TRUE if the device is little endian, CL_FALSE otherwise.
	 */
	cl_int GetEndianLittle(cl_bool& out) const;

	/**
	 * @brief    Retrieve a boolean value determining whether or not this device is currently available.
	 * @details  Correspondes to CL_DEVICE_AVAILABLE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's availability; CL_TRUE if the device is availabe, CL_FALSE otherwise.
	 */
	cl_int GetDeviceAvailable(cl_bool& out) const;

	/**
	 * @brief    Retrieve a boolean value determining whether or not this device has a compiler available for compiling program source code.
	 * @details  Correspondes to CL_DEVICE_COMPILER_AVAILABLE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's availability of a compiler; CL_TRUE if a compiler is available, CL_FALSE otherwise.
	 */
	cl_int GetCompilerAvailable(cl_bool& out) const;

	/**
	 * @brief    Retrieve a bitfield describing the execution capabilities of this device.
	 * @details  Correspondes to CL_DEVICE_EXECUTION_CAPABILITIES.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's execution capabilities.
	 */
	cl_int GetExecutionCapabilities(cl_device_exec_capabilities& out) const;

#if CL_CPP_ENABLE_DEPRECATED_1_0_APIS
	/**
	 * @brief    Retrieve a bitfield describing the command queue properties supported by this device.
	 * @details  Correspondes to CL_DEVICE_QUEUE_PROPERTIES.  This value was deprecated in OpenCL 2.0 and replaced with CL_DEVICE_QUEUE_ON_HOST_PROPERTIES.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's supported command queue properties.
	 */
	cl_int GetCommandQueueProperties(cl_command_queue_properties& out) const;
#endif

	/**
	 * @brief    Retrieve the name of this device.
	 * @details  Correspondes to CL_DEVICE_NAME.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's name.
	 */
	cl_int GetName(std::string& out) const;

	/**
	 * @brief    Retrieve the name of the vendor for this device.
	 * @details  Correspondes to CL_DEVICE_VENDOR.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's vendor name.
	 */
	cl_int GetVendor(std::string& out) const;

	/**
	 * @brief    Retrieve the software driver version string for this device.
	 * @details  Correspondes to CL_DRIVER_VERSION.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's driver version.
	 */
	cl_int GetDriverVersion(std::string& out) const;

	/**
	 * @brief    Retrieve the profile name supported by this device.
	 * @details  Correspondes to CL_DEVICE_PROFILE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's profile name.
	 */
	cl_int GetProfile(std::string& out) const;

	/**
	 * @brief    Retrieve the OpenCL version supported by this device.
	 * @details  Correspondes to CL_DEVICE_VERSION.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's OpenCL version.
	 */
	cl_int GetApiVersion(std::string& out) const;

	/**
	 * @brief    Retrieve the extensions supported by this device.
	 * @details  Correspondes to CL_DEVICE_EXTENSIONS.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device extensions.
	 */
	cl_int GetExtensions(std::string& out) const;

	/**
	 * @brief    Retrieve the platform associated with this device.
	 * @details  Correspondes to CL_DEVICE_PLATFORM.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's platform.
	 */
	cl_int GetPlatform(Platform& out) const;

	/**
	 * @brief    Retrieve a bitfield describing the double precision floating point capabilities of this device.
	 * @details  Correspondes to CL_DEVICE_DOUBLE_FP_CONFIG.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's double precision floating point capabilities.
	 */
	cl_int GetDoubleFpConfig(cl_device_fp_config& out) const;

	/**
	 * @brief    Retrieve a bitfield describing the half precision floating point capabilities of this device.
	 * @details  Correspondes to CL_DEVICE_HALF_FP_CONFIG.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's half precision floating point capabilities.
	 */
	cl_int GetHalfFpConfig(cl_device_fp_config& out) const;

#if CL_CPP_ENABLE_OPENCL_1_1

	/**
	 * @brief    Retrieve this device's implementation preferred vector width for the cl_half scalar type.
	 * @details  Correspondes to CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's preferred cl_half vector width.
	 */
	cl_int GetPreferredVectorWidthHalf(cl_uint& out) const;

	#if CL_CPP_ENABLE_DEPRECATED_1_1_APIS
		/**
		 * @brief    Retrieve a boolean value determining whether or not this device and the host have a unified memory subsystem.
		 * @details  Correspondes to CL_DEVICE_HOST_UNIFIED_MEMORY.  This value was deprecated in OpenCL 2.0.
		 * @return   OpenCL error code returned by clGetDeviceInfo().
		 * @param[out]  out  Output parameter for the unified memory boolean; CL_TRUE if the device and host have unified memory, CL_FALSE otherwise.
		 */
		cl_int GetHostUnifiedMemory(cl_bool& out) const;
	#endif

	/**
	 * @brief    Retrieve this device's native ISA vector width for the cl_char scalar type.
	 * @details  Correspondes to CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's native cl_char vector width.
	 */
	cl_int GetNativeVectorWidthChar(cl_uint& out) const;

	/**
	 * @brief    Retrieve this device's native ISA vector width for the cl_short scalar type.
	 * @details  Correspondes to CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's native cl_short vector width.
	 */
	cl_int GetNativeVectorWidthShort(cl_uint& out) const;

	/**
	 * @brief    Retrieve this device's native ISA vector width for the cl_int scalar type.
	 * @details  Correspondes to CL_DEVICE_NATIVE_VECTOR_WIDTH_INT.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's native cl_int vector width.
	 */
	cl_int GetNativeVectorWidthInt(cl_uint& out) const;

	/**
	 * @brief    Retrieve this device's native ISA vector width for the cl_long scalar type.
	 * @details  Correspondes to CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's native cl_long vector width.
	 */
	cl_int GetNativeVectorWidthLong(cl_uint& out) const;

	/**
	 * @brief    Retrieve this device's native ISA vector width for the cl_float scalar type.
	 * @details  Correspondes to CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's native cl_float vector width.
	 */
	cl_int GetNativeVectorWidthFloat(cl_uint& out) const;

	/**
	 * @brief    Retrieve this device's native ISA vector width for the cl_double scalar type.
	 * @details  Correspondes to CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's native cl_double vector width.
	 */
	cl_int GetNativeVectorWidthDouble(cl_uint& out) const;

	/**
	 * @brief    Retrieve this device's native ISA vector width for the cl_half scalar type.
	 * @details  Correspondes to CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's native cl_half vector width.
	 */
	cl_int GetNativeVectorWidthHalf(cl_uint& out) const;

	/**
	 * @brief    Retrieve the OpenCL language version supported by the compiler for this device.
	 * @details  Correspondes to CL_DEVICE_OPENCL_C_VERSION.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's OpenCL language version.
	 */
	cl_int GetOpenClCVersion(std::string& out) const;
#endif

#if CL_CPP_ENABLE_OPENCL_1_2
	/**
	 * @brief    Retrieve a boolean value determining whether or not a linker is available for this device implementation.
	 * @details  Correspondes to CL_DEVICE_LINKER_AVAILABLE.  This will always be CL_TRUE if CL_DEVICE_COMPILER_AVAILABLE is also CL_TRUE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for a boolean value; CL_TRUE if the device has a linker available, CL_FALSE otherwise.
	 */
	cl_int GetLinkerAvailable(cl_bool& out) const;

	/**
	 * @brief    Retrieve a semi-colon separated list of built-in kernels supported by this device.
	 * @details  Correspondes to CL_DEVICE_BUILT_IN_KERNELS.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's built-in kernel list.
	 */
	cl_int GetBuiltInKernels(std::string& out) const;

	/**
	 * @brief    Retrieve the maximum number of pixels for a 1D image created from a buffer object on this device.
	 * @details  Correspondes to CL_DEVICE_IMAGE_MAX_BUFFER_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum number of 1D image pixels.
	 */
	cl_int GetImageMaxBufferSize(size_t& out) const;

	/**
	 * @brief    Retrieve the maximum number of images allowed in a single image array on this device.
	 * @details  Correspondes to CL_DEVICE_IMAGE_MAX_ARRAY_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum number of images in an image array.
	 */
	cl_int GetImageMaxArraySize(size_t& out) const;

	/**
	 * @brief    Retrieve the device to which this device belongs.
	 * @details  Correspondes to CL_DEVICE_PARENT_DEVICE.  This method only applies to sub-devices.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's parent device.
	 */
	cl_int GetParentDevice(Device& out) const;

	/**
	 * @brief    Retrieve the maximum number of sub-devices that can be created when this device is partitioned.
	 * @details  Correspondes to CL_DEVICE_PARTITION_MAX_SUB_DEVICES.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum number of sub-devices.
	 */
	cl_int GetPartitionMaxSubDevices(cl_uint& out) const;

	/**
	 * @brief    Retrieve a vector containing an array of partition property values for this device.
	 * @details  Correspondes to CL_DEVICE_PARTITION_PROPERTIES.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's partition property vector.
	 */
	cl_int GetPartitionProperties(std::vector<cl_device_partition_property>& out);

	/**
	 * @brief    Retrieve the
	 * @details  Correspondes to CL_DEVICE_PARTITION_AFFINITY_DOMAIN.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for .
	 */
	cl_int GetAffinityDomain(cl_device_affinity_domain& out) const;

	/**
	 * @brief    Retrieve a bitfield describing the supported affinity domains of this device.
	 * @details  Correspondes to CL_DEVICE_PARTITION_TYPE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's affinity domain bitfield.
	 */
	cl_int GetPartitionTypes(std::vector<cl_device_partition_property>& out);

	/**
	 * @brief    Retrieve the current reference count of this device.
	 * @details  Correspondes to CL_DEVICE_REFERENCE_COUNT.  This only applies to sub-devices.  Root devices will always return a reference count of 1.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's reference count.
	 */
	cl_int GetReferenceCount(cl_uint& out) const;

	/**
	 * @brief    Retrieve a boolean value determining whether or not this device's preference is for the user to be responsible for shared memory object synchronization.
	 * @details  Correspondes to CL_DEVICE_PREFERRED_INTEROP_USER_SYNC.  This applies to shared memory objects between OpenCL and another API such as OpenGL or DirectX.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter a boolean value; CL_TRUE if the implementation wants the user to synchronize shared memory objects, CL_FALSE otherwise.
	 */
	cl_int GetPreferredInteropUserSync(cl_bool& out) const;

	/**
	 * @brief    Retrieve the maximum size in bytes of the internal buffer that holds the output of printf calls from a kernel for this device.
	 * @details  Correspondes to CL_DEVICE_PRINTF_BUFFER_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum printf buffer size.
	 */
	cl_int GetPrintfBufferSize(size_t& out) const;
#endif

#if CL_CPP_ENABLE_OPENCL_2_0
	/**
	 * @brief    Retrieve a bitfield describing the on-host command queue properties supported by this device.
	 * @details  Correspondes to CL_DEVICE_QUEUE_ON_HOST_PROPERTIES.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's supported on-host command queue properties.
	 */
	cl_int GetQueueOnHostProperties(cl_command_queue_properties& out) const;

	/**
	 * @brief    Retrieve the row pitch alignment size in pixels for 2D images created from a buffer on this device.
	 * @details  Correspondes to CL_DEVICE_IMAGE_PITCH_ALIGNMENT.  The returned value is always a power of 2.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's row pitch alignment of 2D images.
	 */
	cl_int GetImagePitchAlignment(cl_uint& out) const;

	/**
	 * @brief    Retrieve the value specifying the minimum alignment in pixels of the host pointer used to create an image on this device.
	 * @details  Correspondes to CL_DEVICE_IMAGE_BASE_ADDRESS_ALIGNMENT.  The returned value is always a power of 2.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for device's minimum pixel alignment for image host pointers.
	 */
	cl_int GetImageBaseAddressAlignment(cl_uint& out) const;

	/**
	 * @brief    Retrieve this device's maximum number of image objects arguments of a kernel declared with the write_only or read_write qualifiers.
	 * @details  Correspondes to CL_DEVICE_MAX_READ_WRITE_IMAGE_ARGS.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum number of write-only or read/write image object arguments.
	 */
	cl_int GetMaxReadWriteImageArgs(cl_uint& out) const;

	/**
	 * @brief    Retrieve the maximum size in bytes that may be allocated for any single variable in a program's global address space for this device.
	 * @details  Correspondes to CL_DEVICE_MAX_GLOBAL_VARIABLE_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum global variable size.
	 */
	cl_int GetMaxGlobalVariableSize(size_t& out) const;

	/**
	 * @brief    Retrieve a bitfield describing the on-device command queue properties supported by this device.
	 * @details  Correspondes to CL_DEVICE_QUEUE_ON_DEVICE_PROPERTIES.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's supported on-device command queue properties.
	 */
	cl_int GetQueueOnDeviceProperties(cl_command_queue_properties& out) const;

	/**
	 * @brief    Retrieve the size in bytes of the on-device command queue preferred by this device's implementation.
	 * @details  Correspondes to CL_DEVICE_QUEUE_ON_DEVICE_PREFERRED_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's preferred on-device queue size.
	 */
	cl_int GetQueueOnDevicePreferredSize(cl_uint& out) const;

	/**
	 * @brief    Retrieve the maximum size in bytes of the on-device command queue on this device.
	 * @details  Correspondes to CL_DEVICE_QUEUE_ON_DEVICE_MAX_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum on-device queue size.
	 */
	cl_int GetQueueOnDeviceMaxSize(cl_uint& out) const;

	/**
	 * @brief    Retrieve the maximum number of on-device command queues that can be created per context for this device.
	 * @details  Correspondes to CL_DEVICE_MAX_ON_DEVICE_QUEUES.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum number of on-device queues.
	 */
	cl_int GetMaxOnDeviceQueues(cl_uint& out) const;

	/**
	 * @brief    Retrieve the maximum number of events that can be in use at any time by an on-device command queue for this device.
	 * @details  Correspondes to CL_DEVICE_MAX_ON_DEVICE_EVENTS.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum number of active events.
	 */
	cl_int GetMaxOnDeviceEvents(cl_uint& out) const;

	/**
	 * @brief    Retrieve a bitfield describing the various shared virtual memory allocation types this device supports.
	 * @details  Correspondes to CL_DEVICE_SVM_CAPABILITIES.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's shared virtual memory capabilities.
	 */
	cl_int GetSvmCapabilities(cl_device_svm_capabilities& out) const;

	/**
	 * @brief    Retrieve the maximum preferred size in bytes of all program variables in a program's global address space for this device.
	 * @details  Correspondes to CL_DEVICE_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's preferred global variable size.
	 */
	cl_int GetGlobalVariablePreferredTotalSize(size_t& out) const;

	/**
	 * @brief    Retrieve the maximum number of pipe objects that can be passed as arguments to a kernel on this device.
	 * @details  Correspondes to CL_DEVICE_MAX_PIPE_ARGS.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum number of pipe object arguments.
	 */
	cl_int GetMaxPipeArgs(cl_uint& out) const;

	/**
	 * @brief    Retrieve the maximum number of reservations that can be active for a pipe per work item in a kernel on this device.
	 * @details  Correspondes to CL_DEVICE_PIPE_MAX_ACTIVE_RESERVATIONS.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum number of active pipe reservations.
	 */
	cl_int GetPipeMaxActiveReservations(cl_uint& out) const;

	/**
	 * @brief    Retrieve the maximum size in bytes of a single pipe packet on this device.
	 * @details  Correspondes to CL_DEVICE_PIPE_MAX_PACKET_SIZE.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum pipe packet size.
	 */
	cl_int GetPipeMaxPacketSize(cl_uint& out) const;

	/**
	 * @brief    Retrieve the value representing the preferred alignment in bytes for OpenCL 2.0 fine-grained SVM atomic types on this device.
	 * @details  Correspondes to CL_DEVICE_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's preferred platform atomic alignment.
	 */
	cl_int GetPreferredPlatformAtomicAlignment(cl_uint& out) const;

	/**
	 * @brief    Retrieve the value representing the preferred alignment in bytes for OpenCL 2.0 atomic types of global memory on this device.
	 * @details  Correspondes to CL_DEVICE_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's preferred global atomic alignment.
	 */
	cl_int GetPreferredGlobalAtomicAlignment(cl_uint& out) const;

	/**
	 * @brief    Retrieve the value representing the preferred alignment in bytes for OpenCL 2.0 atomic types of local memory on this device.
	 * @details  Correspondes to CL_DEVICE_PREFERRED_LOCAL_ATOMIC_ALIGNMENT.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's preferred local atomic alignment.
	 */
	cl_int GetPreferredLocalAtomicAlignment(cl_uint& out) const;
#endif

#if CL_CPP_ENABLE_OPENCL_2_1
	/**
	 * @brief    Retrieve a space separated list of intermediate language versions supported by this device.
	 * @details  Correspondes to CL_DEVICE_IL_VERSION.  All versions will be prefixed with "SPIR-V".
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's supports IL versions.
	 */
	cl_int GetILVersion(std::string& out);

	/**
	 * @brief    Retrieve the maximum number of sub-groups in a work group this device is capable of executing on a single compute unit.
	 * @details  Correspondes to CL_DEVICE_MAX_NUM_SUB_GROUPS.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for the device's maximum number of sub-groups.
	 */
	cl_int GetMaxNumSubGroups(cl_uint& out);

	/**
	 * @brief    Retrieve a boolean value determining whether or not this device supports independent forward progress of its sub-groups.
	 * @details  Correspondes to CL_DEVICE_SUB_GROUP_INDEPENDENT_FORWARD_PROGRESS.
	 * @return   OpenCL error code returned by clGetDeviceInfo().
	 * @param[out]  out  Output parameter for a boolean value; CL_TRUE if the device supports independent forward progress of sub-groups, CL_FALSE otherwise.
	 */
	cl_int GetSubGroupIndependentForwardProgress(cl_bool& out);
#endif


private:

	cl_int prv_getString(std::string&, const cl_device_info) const;

	cl_device_id m_id;
};
