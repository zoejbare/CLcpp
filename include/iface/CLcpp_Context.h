/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

namespace CLcpp
{
	class Context;
	class Device;

	typedef void (CL_CALLBACK *ContextNotifyCallback)(const char* errInfo, const void* privateInfoData, size_t privateInfoSize, void* userData);
}

class CLcpp::Context
{
public:

	Context();
	Context(const Context& other);
	Context(Context&& other);
	explicit Context(cl_context context);
	~Context();

	operator cl_context() const;
	Context& operator=(const Context& other);
	Context& operator=(Context&& other);

	/**
	 * @brief   Create a new OpenCL context.
	 * @return  OpenCL error code returned by clCreateContext().
	 * @param[out]  outContext       Output parameter for the new context.
	 * @param[in]   deviceVector     Vector of unique devices for a single platform.
	 * @param[in]   propertiesArray  Null terminated array of properties to use for creating the context.
	 * @param[in]   notifyCallback   Callback function for OpenCL to use for reporting information on errors that occur within the context.
	 * @param[in]   userData         Pointer to any generic data to be sent to the notify callback.
	 */
	static cl_int Create(
		Context& outContext,
		const std::vector<Device>& deviceVector,
		const cl_context_properties* const propertiesArray,
		const ContextNotifyCallback notifyCallback,
		void* const userData
	);

	/**
	 * @brief   Create a new OpenCL context identifying a specific device type to use.
	 * @return  OpenCL error code returned by clCreateContextFromType().
	 * @param[out]  outContext       Output parameter for the new context.
	 * @param[in]   deviceType       Bitfield that identifies the type of device to use.
	 * @param[in]   propertiesArray  Null terminated array of properties to use for creating the context.
	 * @param[in]   notifyCallback   Callback function for OpenCL to use for reporting information on errors that occur within the context.
	 * @param[in]   userData         Pointer to any generic data to be sent to the notify callback.
	 */
	static cl_int CreateFromType(
		Context& outContext,
		const cl_device_type deviceType,
		const cl_context_properties* const propertiesArray,
		const ContextNotifyCallback notifyCallback,
		void* const userData
	);

	/**
	 * @brief    Retrieve the reference count for this context.
	 * @details  Corresponds to CL_CONTEXT_REFERENCE_COUNT.
	 * @return   OpenCL error code returned by clGetContextInfo().
	 * @param[out]  out  Output parameter for the context's reference count.
	 */
	cl_int GetReferenceCount(cl_uint& out) const;

	/**
	 * @brief    Retrieve a vector of devices associated with this context.
	 * @details  Corresponds to CL_CONTEXT_DEVICES.
	 * @return   OpenCL error code returned by clGetContextInfo().
	 * @param[out]  out  Output parameter to contain the devices associated with this context.
	 */
	cl_int GetDevices(std::vector<Device>& out) const;

	/**
	 * @brief    Retrieve a vector of property values used to create this context.
	 * @details  Corresponds to CL_CONTEXT_PROPERTIES.
	 * @return   OpenCL error code returned by clGetContextInfo().
	 * @param[out]  out  Output parameter to contain the context property information.  The format of the vector will be that
	 *                   every even numbered index will be a property and every odd numbered index will be the data value for
	 *                   the preceding property.
	 */
	cl_int GetProperties(std::vector<cl_context_properties>& out) const;

#if CL_CPP_ENABLE_OPENCL_1_1
	/**
	 * @brief    Retrieve the total count of devices associated with this context.
	 * @details  Corresponds to CL_CONTEXT_NUM_DEVICES.
	 * @return   OpenCL error code returned by clGetContextInfo().
	 * @param[out]  out  Output parameter for the number of associated devices.
	 */
	cl_int GetNumDevices(cl_uint& out) const;
#endif


private:

	cl_context m_context;
};
