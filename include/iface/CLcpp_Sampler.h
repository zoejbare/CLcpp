/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

namespace CLcpp
{
	class Sampler;
	class Context;
}

class CLcpp::Sampler
{
public:

	Sampler();
	Sampler(const Sampler& other);
	Sampler(Sampler&& other);
	explicit Sampler(cl_sampler samp);
	~Sampler();

	operator cl_sampler() const;
	Sampler& operator=(const Sampler& other);
	Sampler& operator=(Sampler&& other);

	/**
	 * @brief    Create a new sampler object.
	 * @details  When compiling with OpenCL 2.0 or greater, this will internally call clCreateSamplerWithProperties().
	 *           Prior to version 2.0, clCreateSampler() will be used instead.  For more information, see documentation
	 *           for either function.
	 * @return   OpenCL error code returned by clCreateSampler() or clCreateSamplerWithProperties.
	 * @param[out]  outSampler           Output parameter for the new sampler object.
	 * @param[in]   context              Context under which to create the sampler.
	 * @param[in]   useNormalizedCoords  True to use normalized coordinates, false otherwise.
	 * @param[in]   addressingMode       Addressing mode for the sampler.
	 * @param[in]   filterMode           Filter mode for the sampler.
	 */
	static cl_int Create(
		Sampler& outSampler,
		const Context& context,
		const bool useNormalizedCoords,
		const cl_addressing_mode addressingMode,
		const cl_filter_mode filterMode
	);

	/**
	 * @brief   Retrieve the current reference count for this sampler.
	 * @return  OpenCL error code returned by clGetSamplerInfo().
	 * @param[out]  out  Output parameter for the sampler's reference count.
	 */
	cl_int GetReferenceCount(cl_uint& out) const;

	/**
	 * @brief   Retrieve the context associated with this sampler.
	 * @return  OpenCL error code returned by clGetSamplerInfo().
	 * @param[out]  out  Output parameter for the sampler's context.
	 */
	cl_int GetContext(Context& out) const;

	/**
	 * @brief   Retrieve the boolean value for using normalized coordinates with this sampler.
	 * @return  OpenCL error code returned by clGetSamplerInfo().
	 * @param[out]  out  Output parameter specifying whether or not the sampler uses normalized coordinates.
	 */
	cl_int GetNormalizedCoords(cl_bool& out) const;

	/**
	 * @brief   Retrieve the addressing mode for this event.
	 * @return  OpenCL error code returned by clGetSamplerInfo().
	 * @param[out]  out  Output parameter for the sampler's addressing mode.
	 */
	cl_int GetAddressingMode(cl_addressing_mode& out) const;

	/**
	 * @brief   Retrieve the filter mode for this event.
	 * @return  OpenCL error code returned by clGetSamplerInfo().
	 * @param[out]  out  Output parameter for the sampler's filter mode.
	 */
	cl_int GetFilterMode(cl_filter_mode& out) const;


private:

	cl_sampler m_sampler;
};
