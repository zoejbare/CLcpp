/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

namespace CLcpp
{
	class MemBuffer;
	class CommandQueue;
	class Context;
	class Event;
}

class CLcpp::MemBuffer
{
public:

	MemBuffer();
	MemBuffer(const MemBuffer& other);
	MemBuffer(MemBuffer&& other);
	MemBuffer(cl_mem mem);
	~MemBuffer();

	operator cl_mem() const;
	MemBuffer& operator=(const MemBuffer& other);
	MemBuffer& operator=(MemBuffer&& other);

	/**
	 * @brief   Create a memory object as a generic buffer.
	 * @return  OpenCL error code returned by clCreateBuffer().
	 * @param[out]  outMemBuffer        Output parameter for the new buffer object.
	 * @param[in]   context             Context under which the new buffer object should be created.
	 * @param[in]   accessStorageFlags  Bitfield describing the access and storage modes allowed on the buffer object.
	 * @param[in]   bufferSize          Total size in bytes of the buffer object.
	 * @param[in]   hostStoragePtr      Pointer to data in host memory that should be copied into the new buffer object.  Using nullptr will skip the initial copy.
	 */
	static cl_int CreateAsBuffer(
		MemBuffer& outMemBuffer,
		const Context& context,
		const cl_mem_flags accessStorageFlags,
		const size_t bufferSize,
		void* const hostStoragePtr
	);

	/**
	 * @brief    Create a memory object as a 2D or 3D image.
	 * @details  When compiling with OpenCL 1.2 or greater, this will internally call clCreateImage().
	 *           Prior to version 1.2, clCreateImage2D/3D() will be used instead.  For more information,
	 *           see documentation for either function.
	 * @return   OpenCL error code returned by clCreateImage() or clCreateImage2D/3D().  In versions prior to OpenCL 1.2,
	 *           CL_INVALID_IMAGE_DESCRIPTOR will be returned if imageDesc.image_type is neither CL_MEM_OBJECT_IMAGE2D
	 *           nor CL_MEM_OBJECT_IMAGE3D.
	 * @param[out]  outMemBuffer        Output parameter for the new buffer object.
	 * @param[in]   context             Context under which the new buffer object should be created.
	 * @param[in]   accessStorageFlags  Bitfield describing the access and storage modes allowed on the buffer object.
	 * @param[in]   imageFormat         Format properties of the image.
	 * @param[in]   imageDesc           Image descriptor containing properties such as the image width and height.
	 * @param[in]   hostStoragePtr      Pointer to data in host memory that should be copied into the new buffer object.  Using nullptr will skip the initial copy.
	 */
	static cl_int CreateAsImage(
		MemBuffer& outMemBuffer,
		const Context& context,
		const cl_mem_flags accessStorageFlags,
		const cl_image_format& imageFormat,
		const cl_image_desc& imageDesc,
		void* const hostStoragePtr
	);

	/**
	 * @brief   Retrieve the type of this memory object
	 * @details Corresponds to CL_MEM_TYPE.
	 * @return  OpenCL error code returned by clGetMemObjectInfo().
	 * @param[out]  out  Output parameter for the memory object's type.
	 */
	cl_int GetMemObjectType(cl_mem_object_type& out) const;

	/**
	 * @brief   Retrieve the flags used to create this memory object.
	 * @details Corresponds to CL_MEM_FLAGS.
	 * @return  OpenCL error code returned by clGetMemObjectInfo().
	 * @param[out]  out  Output parameter for the memory object's creation flags.
	 */
	cl_int GetMemFlags(cl_mem_flags& out) const;

	/**
	 * @brief   Retrieve size in bytes of the data store associated with this memory object.
	 * @details Corresponds to CL_MEM_SIZE.
	 * @return  OpenCL error code returned by clGetMemObjectInfo().
	 * @param[out]  out  Output parameter for memory object's size.
	 */
	cl_int GetSize(size_t& out) const;

	/**
	 * @brief   Retrieve the host memory pointer associated with this memory object if it was created with CL_MEM_USE_HOST_PTR.
	 * @details Corresponds to CL_MEM_HOST_PTR.
	 * @return  OpenCL error code returned by clGetMemObjectInfo().
	 * @param[out]  out  Output parameter for the memory object's host pointer.
	 */
	cl_int GetHostPtr(void*& out) const;

	/**
	 * @brief   Retrieve the current number of times this memory object has been mapped.
	 * @details Corresponds to CL_MEM_MAP_COUNT.
	 * @return  OpenCL error code returned by clGetMemObjectInfo().
	 * @param[out]  out  Output parameter for memory object's map count.
	 */
	cl_int GetMapCount(cl_uint& out) const;

	/**
	 * @brief   Retrieve current reference count of this memory object.
	 * @details Corresponds to CL_MEM_REFERENCE_COUNT.
	 * @return  OpenCL error code returned by clGetMemObjectInfo().
	 * @param[out]  out  Output parameter for the memory object's reference count.
	 */
	cl_int GetReferenceCount(cl_uint& out) const;

	/**
	 * @brief   Retrieve context associated with this memory object.
	 * @details Corresponds to CL_MEM_CONTEXT.
	 * @return  OpenCL error code returned by clGetMemObjectInfo().
	 * @param[out]  out  Output parameter for memory object's context.
	 */
	cl_int GetContext(Context& out) const;

	/**
	 * @brief   Retrieve format of this image object
	 * @details Corresponds to CL_IMAGE_FORMAT.
	 * @return  OpenCL error code returned by clGetImageInfo().
	 * @param[out]  out  Output parameter for object's image format.
	 */
	cl_int GetImageFormat(cl_image_format& out) const;

	/**
	 * @brief   Retrieve the size in bytes of a single element in this image object.
	 * @details Corresponds to CL_IMAGE_ELEMENT_SIZE.
	 * @return  OpenCL error code returned by clGetImageInfo().
	 * @param[out]  out  Output parameter for image object's element size.
	 */
	cl_int GetImageElementSize(size_t& out) const;

	/**
	 * @brief   Retrieve the size in bytes of a single scanline in this image object.
	 * @details Corresponds to CL_IMAGE_ROW_PITCH.
	 * @return  OpenCL error code returned by clGetImageInfo().
	 * @param[out]  out  Output parameter for image object's row pitch.
	 */
	cl_int GetImageRowPitch(size_t& out) const;

	/**
	 * @brief   Retrieve the size in bytes of a single 2D slice in this image object.
	 * @details Corresponds to CL_IMAGE_SLICE_PITCH.
	 * @return  OpenCL error code returned by clGetImageInfo().
	 * @param[out]  out  Output parameter for the image object's slice pitch.
	 */
	cl_int GetImageSlicePitch(size_t& out) const;

	/**
	 * @brief   Retrieve the width of this image object.
	 * @details Corresponds to CL_IMAGE_WIDTH.
	 * @return  OpenCL error code returned by clGetImageInfo().
	 * @param[out]  out  Output parameter for image object's width.
	 */
	cl_int GetImageWidth(size_t& out) const;

	/**
	 * @brief   Retrieve the height of this image object.
	 * @details Corresponds to CL_IMAGE_HEIGHT.
	 * @return  OpenCL error code returned by clGetImageInfo().
	 * @param[out]  out  Output parameter for image object's height.
	 */
	cl_int GetImageHeight(size_t& out) const;

	/**
	 * @brief   Retrieve the depth of this image object.
	 * @details Corresponds to CL_IMAGE_DEPTH.
	 * @return  OpenCL error code returned by clGetImageInfo().
	 * @param[out]  out  Output parameter for image object's depth.
	 */
	cl_int GetImageDepth(size_t& out) const;

#if CL_CPP_ENABLE_OPENCL_1_1
	/**
	 * @brief   Retrieve parent memory object associated with this memory object.
	 * @details Corresponds to CL_MEM_ASSOCIATED_MEMOBJECT.
	 * @return  OpenCL error code returned by clGetMemObjectInfo().
	 * @param[out]  out  Output parameter for memory object's parent object.
	 */
	cl_int GetAssociatedMemObject(MemBuffer& out) const;

	/**
	 * @brief   Retrieve the offset in bytes of this memory object within it's parent memory object.
	 * @details Corresponds to CL_MEM_OFFSET.
	 * @return  OpenCL error code returned by clGetMemObjectInfo().
	 * @param[out]  out  Output parameter for memory object's offset.
	 */
	cl_int GetOffset(size_t& out) const;
#endif

#if CL_CPP_ENABLE_OPENCL_1_2
	/**
	 * @brief   Retrieve the total number of images in this image array object.
	 * @details Corresponds to CL_IMAGE_ARRAY_SIZE.
	 * @return  OpenCL error code returned by clGetImageInfo().
	 * @param[out]  out  Output parameter for size of the image object's internal array.
	 */
	cl_int GetImageArraySize(size_t& out) const;

	#if CL_CPP_ENABLE_DEPRECATED_1_2_APIS
		/**
		 * @brief   Retrieve the parent image object associated with this image object.
		 * @details Corresponds to CL_IMAGE_BUFFER.  This value was deprecated in OpenCL 2.0.
		 * @return  OpenCL error code returned by clGetImageInfo().
		 * @param[out]  out  Output parameter for the object's parent image object.
		 */
		cl_int GetImageAssociatedMemObject(MemBuffer& out) const;
	#endif

	/**
	 * @brief   Retrieve the number of mip levels in this image object.
	 * @details Corresponds to CL_IMAGE_NUM_MIP_LEVELS.
	 * @return  OpenCL error code returned by clGetImageInfo().
	 * @param[out]  out  Output parameter for the image object's mip level count.
	 */
	cl_int GetImageNumMipLevels(cl_uint& out) const;

	/**
	 * @brief   Retrieve number of samples associated with this image object.
	 * @details Corresponds to CL_IMAGE_NUM_SAMPLES.
	 * @return  OpenCL error code returned by clGetImageInfo().
	 * @param[out]  out  Output parameter for the image object's sample count.
	 */
	cl_int GetImageNumSamples(cl_uint& out) const;
#endif

#if CL_CPP_ENABLE_OPENCL_2_0
	/**
	 * @brief   Retrieve a boolean value to determine if this buffer object was created with a Shared Virtual Memory pointer.
	 * @details Corresponds to CL_MEM_USES_SVM_POINTER.
	 * @return  OpenCL error code returned by clGetMemObjectInfo().
	 * @param[out]  out  Output parameter for the memory object's SVM pointer boolean value.
	 */
	cl_int GetUsesSvmPointer(cl_bool& out) const;
#endif


private:

	cl_mem m_memObject;
};
