/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

namespace CLcpp
{
	class Util;
}

class CLcpp::Util
{
public:

	typedef std::function<void* (size_t)> AllocCallback;
	typedef std::function<void (void*)>   FreeCallback;

	/**
	 * @brief  Set the callback used for allocating and freeing manual allocations within the library.
	 * @param[in]  allocCallback  Allocation callback function.
	 * @param[in]  freeCallback   Allocation callback function.
	 */
	static void SetMemoryCallbacks(const AllocCallback allocCallback, const FreeCallback freeCallback);

	/**
	 * @brief   Make an allocation of a specified size in bytes.
	 * @return  Pointer to the allocated memory.
	 * @param[in]  allocSize  Size in bytes.
	 */
	static void* Alloc(const size_t allocSize);

	/**
	 * @brief  Free an allocated block of memory.
	 * @param[in]  pMem  Block of memory to be freed.
	 */
	static void Free(void* const pMem);


private:

	static AllocCallback& prv_getAllocCallback();
	static FreeCallback& prv_getFreeCallback();
};
