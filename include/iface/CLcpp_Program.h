/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

namespace CLcpp
{
	struct ProgramSource;
	struct ProgramBinary;

	class Program;
	class Context;
	class Device;

	typedef void(CL_CALLBACK *ProgramCallback)(cl_program program, void* userData);
}

struct CLcpp::ProgramSource
{
	const char* fileBuffer;
	size_t fileSize;
};


struct CLcpp::ProgramBinary
{
	cl_uchar* binaryBuffer;
	size_t binarySize;
};


class CLcpp::Program
{
public:

	Program();
	Program(const Program& other);
	Program(Program&& other);
	explicit Program(cl_program program);
	~Program();

	operator cl_program() const;
	Program& operator=(const Program& other);
	Program& operator=(Program&& other);

	/**
	 * @brief   Create a program object from raw program source code.
	 * @return  OpenCL error code returned by clCreateProgramWithSource().
	 * @param[out]  outProgram           Output parameter for the new program object.
	 * @param[in]   context              Context under which to create the new program.
	 * @param[in]   programSourceVector  Vector of ProgramSource objects representing the raw source code for each file that should be built into the output program.
	 */
	static cl_int CreateFromSources(Program& outProgram, const Context& context, const std::vector<ProgramSource>& programSourceVector);

	/**
	 * @brief   Create a program object from raw program source code.
	 * @return  OpenCL error code returned by clCreateProgramWithSource().
	 * @param[out]  outProgram           Output parameter for the new program object.
	 * @param[in]   context              Context under which to create the new program.
	 * @param[in]   programSourceVector  Vector of string objects representing the raw source code for each file that should be built into the output program.
	 */
	static cl_int CreateFromSources(Program& outProgram, const Context& context, const std::vector<std::string>& programSourceVector);

	/**
	 * @brief   Create a program object from precompiled program source code.
	 * @return  OpenCL error code returned by clCreateProgramWithBinary().
	 * @param[out]  outProgram           Output parameter for the new program object.
	 * @param[in]   context              Context under which to create the new program.
	 * @param[in]   deviceVector         Vector of devices in the input context.  The program binaries will be loaded for these devices.
	 * @param[in]   programBinaryVector  Vector of ProgramBinary objects representing the precompiled binary data for each file that should be built into the output program.
	 */
	static cl_int CreateFromBinaries(
		Program& outProgram,
		std::vector<cl_int>& outBinaryStatusVector,
		const Context& context,
		const std::vector<Device>& deviceVector,
		const std::vector<ProgramBinary>& programBinaryVector
	);

	/**
	 * @brief   Create a program object from precompiled program source code.
	 * @return  OpenCL error code returned by clCreateProgramWithBinary().
	 * @param[out]  outProgram           Output parameter for the new program object.
	 * @param[in]   context              Context under which to create the new program.
	 * @param[in]   deviceVector         Vector of devices in the input context.  The program binaries will be loaded for these devices.
	 * @param[in]   programBinaryVector  Vector of string objects representing the precompiled binary data for each file that should be built into the output program.
	 */
	static cl_int CreateFromBinaries(
		Program& outProgram,
		std::vector<cl_int>& outBinaryStatusVector,
		const Context& context,
		const std::vector<Device>& deviceVector,
		const std::vector<std::string>& programBinaryVector
	);

	/**
	 * @brief   Builds (compiles and links) this program executable from the program source or binary.
	 * @return  OpenCL error code returned by clBuildProgram().
	 * @param[in]  deviceVector  Vector of devices to build the program for.  If empty, the program will be built for all devices associated with it.
	 * @param[in]  buildOptions  String of options to pass to the OpenCL compiler and linker.
	 * @param[in]  callback      Notification callback used to inform the user of build status.  Use nullptr to ignore status updates.
	 * @parma[in]  userData      Pointer to generic data that will be passed to the notify callback.
	 */
	cl_int Build(const std::vector<Device>& deviceVector, const char* const buildOptions, const ProgramCallback callback, void* const userData);

	/**
	 * @brief    Retrieve the current reference count of this program.
	 * @details  Corresponds to CL_PROGRAM_REFERENCE_COUNT.
	 * @return   OpenCL error code returned by clGetProgramInfo().
	 * @param[out]  out  Output parameter program's reference count.
	 */
	cl_int GetReferenceCount(cl_uint& out) const;

	/**
	 * @brief    Retrieve the context associated with this program.
	 * @details  Corresponds to CL_PROGRAM_CONTEXT.
	 * @return   OpenCL error code returned by clGetProgramInfo().
	 * @param[out]  out  Output parameter program's context.
	 */
	cl_int GetContext(Context& out) const;

	/**
	 * @brief    Retrieve the total number of devices associated with this program.
	 * @details  Corresponds to CL_PROGRAM_NUM_DEVICES.
	 * @return   OpenCL error code returned by clGetProgramInfo().
	 * @param[out]  out  Output parameter the program's number of devices.
	 */
	cl_int GetNumDevices(cl_uint& out) const;

	/**
	 * @brief    Retrieve a vector of devices associated with this program.
	 * @details  Corresponds to CL_PROGRAM_DEVICES.
	 * @return   OpenCL error code returned by clGetProgramInfo().
	 * @param[out]  out  Output parameter the program's devices.
	 */
	cl_int GetDevices(std::vector<Device>& out) const;

	/**
	 * @brief    Retrieve a concatenated string representing all of the source code used to build this program.
	 * @details  Corresponds to CL_PROGRAM_SOURCE.  This is only valid if the program was created from source.
	 * @return   OpenCL error code returned by clGetProgramInfo().
	 * @param[out]  out  Output parameter for the program's source code.
	 */
	cl_int GetSource(std::string& out) const;

	/**
	 * @brief    Retrieve a vector containing strings of the binary data generated when this program was built.
	 * @details  Corresponds to both CL_PROGRAM_BINARY_SIZES and CL_PROGRAM_BINARIES.
	 * @return   OpenCL error code returned by clGetProgramInfo().
	 * @param[out]  out  Output parameter for the vector of the program's binary data.
	 */
	cl_int GetBinaries(std::vector<std::string>& out) const;

	/**
	 * @brief    Retrieve the build status of this program for a given device.
	 * @details  Corresponds to CL_PROGRAM_BUILD_STATUS.
	 * @return   OpenCL error code returned by clGetProgramBuildInfo().
	 * @param[out]  out     Output parameter for the program's build status.
	 * @param[in]   device  Device associated with the program.
	 */
	cl_int GetBuildStatus(cl_build_status& out, const Device& device) const;

	/**
	 * @brief    Retrieve the build options used when building this program for a given device.
	 * @details  Corresponds to CL_PROGRAM_BUILD_OPTIONS.
	 * @return   OpenCL error code returned by clGetProgramBuildInfo().
	 * @param[out]  out     Output parameter for the program's build options.
	 * @param[in]   device  Device associated with the program.
	 */
	cl_int GetBuildOptions(std::string& out, const Device& device) const;

	/**
	 * @brief    Retrieve the build log generated when building this program for a given device.
	 * @details  Corresponds to CL_PROGRAM_BUILD_LOG.
	 * @return   OpenCL error code returned by clGetProgramBuildInfo().
	 * @param[out]  out     Output parameter for the program's build log.
	 * @param[in]   device  Device associated with the program.
	 */
	cl_int GetBuildLog(std::string& out, const Device& device) const;

#if CL_CPP_ENABLE_OPENCL_1_2
	/**
	 * @brief   Creates a program object for a context and loads the information related to the built-in kernels into it.
	 * @return  OpenCL error code returned by clCreateProgramWithBuiltInKernels().
	 * @param[out]  outProgram    Output parameter for the new program object.
	 * @param[in]   context       Context under which to create the new program.
	 * @param[in]   deviceVector  Vector of devices in the input context.  The built-in kernels will be loaded for these devices.
	 * @param[in]   kernelNames   A semi-colon separated list of built-in kernel names.
	 */
	static cl_int CreateFromBuiltInKernels(
		Program& outProgram,
		const Context& context,
		const std::vector<Device>& deviceVector,
		const char* const kernelNames
	);

	/**
	 * @brief   Compile this program's source code.
	 * @return  OpenCL error code returned by clBuildProgram().
	 * @param[in]  deviceVector         Vector of devices to compile the program for.
	 * @param[in]  headerProgramVector  Vector of programs to use as headers when compiling this program.
	 * @param[in]  headerNameVector     Vector of file path strings that correspond to headerProgramVector that will be used for header look-ups when compiling.
	 * @param[in]  compileOptions       String of options to pass to the OpenCL compiler.
	 * @param[in]  callback             Notifiction callback used to inform the user of compile status.  Use nullptr to ignore status updates.
	 * @param[in]  userData             Pointer to generic data that will be passed to the notify callback.
	 */
	cl_int Compile(
		const std::vector<Device>& deviceVector,
		const std::vector<Program>& headerProgramVector,
		const std::vector<const char*>& headerNameVector,
		const char* const compileOptions,
		const ProgramCallback callback,
		void* const userData
	);

	/**
	 * @brief   Link a set of compiled programs into a single program executable.
	 * @return  OpenCL error code returned by clLinkProgram().
	 * @param[out]  outProgram          Output parameter for the linked program.
	 * @param[in]   deviceVector        Vector of devices to use for performing the link.
	 * @param[in]   inputProgramVector  Vector of compiled programs to be linked.
	 * @param[in]   linkOptions         String of options to pass to the OpenCL linker.
	 * @param[in]   callback            Notifiction callback used to inform the user of link status.  Use nullptr to ignore status updates.
	 * @param[in]   userData            Pointer to generic data that will be passed to the notify callback.
	 */
	static cl_int Link(
		Program& outProgram,
		const Context& context,
		const std::vector<Device>& deviceVector,
		const std::vector<Program>& inputProgramVector,
		const char* const linkOptions,
		const ProgramCallback callback,
		void* const userData
	);

	/**
	 * @brief    Retrieve the total number of kernels declared in this program.
	 * @details  Corresponds to CL_PROGRAM_NUM_KERNELS.  This is only available once a program has been successfully built.
	 * @return   OpenCL error code returned by clGetProgramInfo().
	 * @param[out]  out  Output parameter for the program's total number of kernels.
	 */
	cl_int GetNumKernels(size_t& out) const;

	/**
	 * @brief    Retrieve a string containing a semi-colon seperated list of the kernel names in this program.
	 * @details  Corresponds to CL_PROGRAM_KERNEL_NAMES.  This is only available once a program has been successfully built.
	 * @return   OpenCL error code returned by clGetProgramInfo().
	 * @param[out]  out  Output parameter for the program's kernel names.
	 */
	cl_int GetKernelNames(std::string& out) const;

	/**
	 * @brief    Retrieve the program binary type of this program for a given device.
	 * @details  Corresponds to CL_PROGRAM_BINARY_TYPE.
	 * @return   OpenCL error code returned by clGetProgramBuildInfo().
	 * @param[out]  out     Output parameter for the program's build type.
	 * @param[in]   device  Device associated with the program.
	 */
	cl_int GetBinaryType(cl_program_binary_type& out, const Device& device) const;
#endif

#if CL_CPP_ENABLE_OPENCL_2_0
	/**
	 * @brief    Retrieve the total size in bytes for the global variable storage used by this program for a given device.
	 * @details  Corresponds to CL_PROGRAM_BUILD_GLOBAL_VARIABLE_TOTAL_SIZE.
	 * @return   OpenCL error code returned by clGetProgramBuildInfo().
	 * @param[out]  out     Output parameter for the program's total size of global variables.
	 * @param[in]   device  Device associated with the program.
	 */
	cl_int GetGlobalVariableTotalSize(size_t& out, const Device& device) const;
#endif

#if CL_CPP_ENABLE_OPENCL_2_1
	/**
	 * @brief   Create a program object from precompiled, intermediate language (IL) data.
	 * @return  OpenCL error code returned by clCreateProgramWithIL().
	 * @param[out]  outProgram        Output parameter for the new program object.
	 * @param[in]   context           Context under which to create the new program.
	 * @param[in]   intermediateData  Object representing a pointer and size to a block of memory containing SPIR-V or an implementation-defined intermediate language.
	 */
	static cl_int CreateFromIL(
		Program& outProgram,
		const Context& context,
		const ProgramBinary& intermediateData
	);

	/**
	 * @brief   Create a program object from precompiled, intermediate language (IL) data.
	 * @return  OpenCL error code returned by clCreateProgramWithIL().
	 * @param[out]  outProgram        Output parameter for the new program object.
	 * @param[in]   context           Context under which to create the new program.
	 * @param[in]   intermediateData  String representing a pointer and size to a block of memory containing SPIR-V or an implementation-defined intermediate language.
	 */
	static cl_int CreateFromIL(
		Program& outProgram,
		const Context& context,
		const std::string& intermediateData
	);
#endif


private:

	static cl_int prv_createFromSources(Program&, const Context&, const std::vector<const char*>&, const std::vector<size_t>&);
	static cl_int prv_createFromBinaries(Program&, std::vector<cl_int>&, const Context&, const std::vector<Device>&, const std::vector<cl_uchar*>&, const std::vector<size_t>&);

	cl_int prv_getInfoString(std::string&, const cl_program_info) const;
	cl_int prv_getBuildInfoString(std::string&, const Device&, const cl_program_build_info) const;

	cl_program m_program;
};
