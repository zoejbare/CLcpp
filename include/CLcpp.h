/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

#define CL_CPP_VERSION_MAJOR 2
#define CL_CPP_VERSION_MINOR 1
#define CL_CPP_VERSION_REV   0

#define CL_CPP_STR2(x) #x
#define CL_CPP_STR(x) CL_CPP_STR2(x)

#define CL_CPP_VERSION_STRING CL_CPP_STR(CL_CPP_VERSION_MAJOR) "." CL_CPP_STR(CL_CPP_VERSION_MINOR) "." CL_CPP_STR(CL_CPP_VERSION_REV)

// Include the setup header first since everything needs it.
#include "CLcpp_setup.h"

// CLcpp class declarations.
#include "iface/CLcpp_Context.h"
#include "iface/CLcpp_CommandQueue.h"
#include "iface/CLcpp_Device.h"
#include "iface/CLcpp_Event.h"
#include "iface/CLcpp_Kernel.h"
#include "iface/CLcpp_MemBuffer.h"
#include "iface/CLcpp_Program.h"
#include "iface/CLcpp_Platform.h"
#include "iface/CLcpp_Sampler.h"
#include "iface/CLcpp_Util.h"

// CLcpp class implementations.
#include "impl/CLcpp_Context.inl"
#include "impl/CLcpp_CommandQueue.inl"
#include "impl/CLcpp_Device.inl"
#include "impl/CLcpp_Event.inl"
#include "impl/CLcpp_Kernel.inl"
#include "impl/CLcpp_MemBuffer.inl"
#include "impl/CLcpp_Program.inl"
#include "impl/CLcpp_Platform.inl"
#include "impl/CLcpp_Sampler.inl"
#include "impl/CLcpp_Util.inl"
