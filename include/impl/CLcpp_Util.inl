/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

inline void CLcpp::Util::SetMemoryCallbacks(const AllocCallback allocCallback, const FreeCallback freeCallback)
{
	prv_getAllocCallback() = allocCallback;
	prv_getFreeCallback() = freeCallback;
}


inline void* CLcpp::Util::Alloc(const size_t allocSize)
{
	return prv_getAllocCallback()(allocSize);
}


inline void CLcpp::Util::Free(void* const pMem)
{
	prv_getFreeCallback()(pMem);
}


inline CLcpp::Util::AllocCallback& CLcpp::Util::prv_getAllocCallback()
{
   static AllocCallback cb = malloc;

   return cb;
}


inline CLcpp::Util::FreeCallback& CLcpp::Util::prv_getFreeCallback()
{
   static FreeCallback cb = free;

   return cb;
}
