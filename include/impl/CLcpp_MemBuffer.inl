/*******************************************************************************
Copyright (c) 2013, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

inline CLcpp::MemBuffer::MemBuffer()
	: m_memObject(nullptr)
{
}


inline CLcpp::MemBuffer::MemBuffer(const MemBuffer& other)
	: m_memObject(other.m_memObject)
{
	if(m_memObject)
	{
		clRetainMemObject(m_memObject);
	}
}


inline CLcpp::MemBuffer::MemBuffer(MemBuffer&& other)
	: m_memObject(other.m_memObject)
{
	other.m_memObject = nullptr;
}


inline CLcpp::MemBuffer::MemBuffer(cl_mem mem)
	: m_memObject(mem)
{
}


inline CLcpp::MemBuffer::~MemBuffer()
{
	if(m_memObject)
	{
		clReleaseMemObject(m_memObject);
	}
}


inline CLcpp::MemBuffer::operator cl_mem() const
{
	return m_memObject;
}


inline CLcpp::MemBuffer& CLcpp::MemBuffer::operator=(const MemBuffer& other)
{
	if(m_memObject)
	{
		clReleaseMemObject(m_memObject);
	}

	m_memObject = other.m_memObject;

	if(m_memObject)
	{
		clRetainMemObject(m_memObject);
	}

	return (*this);
}


inline CLcpp::MemBuffer& CLcpp::MemBuffer::operator=(MemBuffer&& other)
{
	if(m_memObject)
	{
		clReleaseMemObject(m_memObject);
	}

	m_memObject = other.m_memObject;
	other.m_memObject = nullptr;

	return (*this);
}


inline cl_int CLcpp::MemBuffer::CreateAsBuffer(
	MemBuffer& outMemBuffer,
	const Context& context,
	const cl_mem_flags accessStorageFlags,
	const size_t bufferSize,
	void* const hostStoragePtr
)
{
	cl_int errorCode = CL_SUCCESS;
	cl_mem mem = clCreateBuffer(context, accessStorageFlags, bufferSize, hostStoragePtr, &errorCode);

	if(errorCode == CL_SUCCESS)
	{
		outMemBuffer = std::move(MemBuffer(mem));
	}

	return 0;
}


inline cl_int CLcpp::MemBuffer::CreateAsImage(
	MemBuffer& outMemBuffer,
	const Context& context,
	const cl_mem_flags accessStorageFlags,
	const cl_image_format& imageFormat,
	const cl_image_desc& imageDesc,
	void* const hostStoragePtr
)
{
	cl_int errorCode = CL_SUCCESS;
	cl_mem mem;

#if CL_CPP_ENABLE_OPENCL_1_2
	mem = clCreateImage(context, accessStorageFlags, &imageFormat, &imageDesc, hostStoragePtr, &errorCode);
#else
	switch(imageDesc.image_type)
	{
		case CL_MEM_OBJECT_IMAGE2D:
			// Create as a 2D image.
			mem = clCreateImage2D(
				context,
				accessStorageFlags,
				&imageFormat,
				imageDesc.image_width,
				imageDesc.image_height,
				imageDesc.image_row_pitch,
				hostStoragePtr, &errorCode
			);
			break;

		case CL_MEM_OBJECT_IMAGE3D:
			// Create as a 3D image.
			mem = clCreateImage3D(
				context,
				accessStorageFlags,
				&imageFormat,
				imageDesc.image_width,
				imageDesc.image_height,
				imageDesc.image_depth,
				imageDesc.image_row_pitch,
				imageDesc.image_slice_pitch,
				hostStoragePtr, &errorCode
			);
			break;

		default:
			// Invalid image type for this version of OpenCL.
			return CL_INVALID_IMAGE_DESCRIPTOR;
	}
#endif

	if(errorCode == CL_SUCCESS)
	{
		outMemBuffer = std::move(MemBuffer(mem));
	}

	return errorCode;
}


inline cl_int CLcpp::MemBuffer::GetMemObjectType(cl_mem_object_type& out) const
{
	return clGetMemObjectInfo(m_memObject, CL_MEM_TYPE, sizeof(cl_mem_object_type), &out, nullptr);
}


inline cl_int CLcpp::MemBuffer::GetMemFlags(cl_mem_flags& out) const
{
	return clGetMemObjectInfo(m_memObject, CL_MEM_FLAGS, sizeof(cl_mem_flags), &out, nullptr);
}


inline cl_int CLcpp::MemBuffer::GetSize(size_t& out) const
{
	return clGetMemObjectInfo(m_memObject, CL_MEM_SIZE, sizeof(size_t), &out, nullptr);
}


inline cl_int CLcpp::MemBuffer::GetHostPtr(void*& out) const
{
	return clGetMemObjectInfo(m_memObject, CL_MEM_HOST_PTR, sizeof(void*), &out, nullptr);
}


inline cl_int CLcpp::MemBuffer::GetMapCount(cl_uint& out) const
{
	return clGetMemObjectInfo(m_memObject, CL_MEM_MAP_COUNT, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::MemBuffer::GetReferenceCount(cl_uint& out) const
{
	return clGetMemObjectInfo(m_memObject, CL_MEM_REFERENCE_COUNT, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::MemBuffer::GetContext(Context& out) const
{
	cl_context ctx;
	const cl_int errorCode = clGetMemObjectInfo(m_memObject, CL_MEM_CONTEXT, sizeof(cl_context), &ctx, nullptr);

	if(errorCode == CL_SUCCESS)
	{
		out = std::move(Context(ctx));
	}

	return errorCode;
}


inline cl_int CLcpp::MemBuffer::GetImageFormat(cl_image_format& out) const
{
	return clGetImageInfo(m_memObject, CL_IMAGE_FORMAT, sizeof(cl_image_format), &out, nullptr);
}


inline cl_int CLcpp::MemBuffer::GetImageElementSize(size_t& out) const
{
	return clGetImageInfo(m_memObject, CL_IMAGE_ELEMENT_SIZE, sizeof(cl_image_format), &out, nullptr);
}


inline cl_int CLcpp::MemBuffer::GetImageRowPitch(size_t& out) const
{
	return clGetImageInfo(m_memObject, CL_IMAGE_ROW_PITCH, sizeof(cl_image_format), &out, nullptr);
}


inline cl_int CLcpp::MemBuffer::GetImageSlicePitch(size_t& out) const
{
	return clGetImageInfo(m_memObject, CL_IMAGE_SLICE_PITCH, sizeof(cl_image_format), &out, nullptr);
}


inline cl_int CLcpp::MemBuffer::GetImageWidth(size_t& out) const
{
	return clGetImageInfo(m_memObject, CL_IMAGE_WIDTH, sizeof(cl_image_format), &out, nullptr);
}


inline cl_int CLcpp::MemBuffer::GetImageHeight(size_t& out) const
{
	return clGetImageInfo(m_memObject, CL_IMAGE_HEIGHT, sizeof(cl_image_format), &out, nullptr);
}


inline cl_int CLcpp::MemBuffer::GetImageDepth(size_t& out) const
{
	return clGetImageInfo(m_memObject, CL_IMAGE_DEPTH, sizeof(cl_image_format), &out, nullptr);
}


#if CL_CPP_ENABLE_OPENCL_1_1
	inline cl_int CLcpp::MemBuffer::GetAssociatedMemObject(MemBuffer& out) const
	{
		cl_mem mem;
		const cl_int errorCode = clGetMemObjectInfo(m_memObject, CL_MEM_ASSOCIATED_MEMOBJECT, sizeof(cl_mem), &mem, nullptr);

		if(errorCode == CL_SUCCESS)
		{
			out = std::move(MemBuffer(mem));
		}

		return errorCode;
	}


	inline cl_int CLcpp::MemBuffer::GetOffset(size_t& out) const
	{
		return clGetMemObjectInfo(m_memObject, CL_MEM_OFFSET, sizeof(size_t), &out, nullptr);
	}
#endif


#if CL_CPP_ENABLE_OPENCL_1_2
	inline cl_int CLcpp::MemBuffer::GetImageArraySize(size_t& out) const
	{
		return clGetImageInfo(m_memObject, CL_IMAGE_ARRAY_SIZE, sizeof(cl_image_format), &out, nullptr);
	}


	#if CL_CPP_ENABLE_DEPRECATED_1_2_APIS
		inline cl_int CLcpp::MemBuffer::GetImageAssociatedMemObject(MemBuffer& out) const
		{
			cl_mem mem;
			const cl_int errorCode = clGetImageInfo(m_memObject, CL_IMAGE_BUFFER, sizeof(cl_mem), &mem, nullptr);

			if(errorCode == CL_SUCCESS)
			{
				out = std::move(MemBuffer(mem));
			}

			return errorCode;
		}
	#endif


	inline cl_int CLcpp::MemBuffer::GetImageNumMipLevels(cl_uint& out) const
	{
		return clGetImageInfo(m_memObject, CL_IMAGE_NUM_MIP_LEVELS, sizeof(cl_image_format), &out, nullptr);
	}


	inline cl_int CLcpp::MemBuffer::GetImageNumSamples(cl_uint& out) const
	{
		return clGetImageInfo(m_memObject, CL_IMAGE_NUM_SAMPLES, sizeof(cl_image_format), &out, nullptr);
	}
#endif


#if CL_CPP_ENABLE_OPENCL_2_0
	inline cl_int CLcpp::MemBuffer::GetUsesSvmPointer(cl_bool& out) const
	{
		return clGetMemObjectInfo(m_memObject, CL_MEM_USES_SVM_POINTER, sizeof(cl_bool), &out, nullptr);
	}
#endif
