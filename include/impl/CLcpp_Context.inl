/*******************************************************************************
Copyright (c) 2013, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

inline CLcpp::Context::Context()
	: m_context()
{
}


inline CLcpp::Context::Context(const Context& other)
	: m_context(other.m_context)
{
	if(m_context)
	{
		clRetainContext(m_context);
	}
}


inline CLcpp::Context::Context(Context&& other)
	: m_context(other.m_context)
{
	other.m_context = nullptr;
}


inline CLcpp::Context::Context(cl_context context)
	: m_context(context)
{
}


inline CLcpp::Context::~Context()
{
	if(m_context)
	{
		clReleaseContext(m_context);
	}
}


inline CLcpp::Context::operator cl_context() const
{
	return m_context;
}


inline CLcpp::Context& CLcpp::Context::operator=(const Context& other)
{
	if(m_context)
	{
		clReleaseContext(m_context);
	}

	m_context = other.m_context;

	if(m_context)
	{
		clRetainContext(m_context);
	}

	return (*this);
}


inline CLcpp::Context& CLcpp::Context::operator=(Context&& other)
{
	if(m_context)
	{
		clReleaseContext(m_context);
	}

	m_context = other.m_context;
	other.m_context = nullptr;

	return (*this);
}


inline cl_int CLcpp::Context::Create(
	Context& outContext,
	const std::vector<Device>& deviceVector,
	const cl_context_properties* const propertiesArray,
	const ContextNotifyCallback notifyCallback,
	void* const userData
)
{
	const cl_uint numDevices = cl_uint(deviceVector.size());
	const cl_device_id* const deviceArray = (const cl_device_id*) &deviceVector[0];

	cl_int errorCode;
	cl_context ctx = clCreateContext(
		propertiesArray,
		numDevices,
		deviceArray,
		notifyCallback,
		userData,
		&errorCode
	);

	if(errorCode == CL_SUCCESS)
	{
		outContext = std::move(Context(ctx));
	}

	return errorCode;
}


inline cl_int CLcpp::Context::CreateFromType(
	Context& outContext,
	const cl_device_type deviceType,
	const cl_context_properties* const propertiesArray,
	const ContextNotifyCallback notifyCallback,
	void* const userData
)
{
	cl_int errorCode;
	cl_context ctx = clCreateContextFromType(
		propertiesArray,
		deviceType,
		notifyCallback,
		userData,
		&errorCode
	);

	if(errorCode == CL_SUCCESS)
	{
		outContext = std::move(Context(ctx));
	}

	return errorCode;
}


inline cl_int CLcpp::Context::GetReferenceCount(cl_uint& out) const
{
	return clGetContextInfo(m_context, CL_CONTEXT_REFERENCE_COUNT, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Context::GetDevices(std::vector<Device>& out) const
{
	cl_int errorCode = CL_SUCCESS;
	CL_CPP_GET_INFO_ARRAY_CUSTOM_TYPE(out, cl_device_id, Device, clGetContextInfo, m_context, CL_CONTEXT_DEVICES, errorCode);

	return errorCode;
}


inline cl_int CLcpp::Context::GetProperties(std::vector<cl_context_properties>& out) const
{
	cl_int errorCode = CL_SUCCESS;
	CL_CPP_GET_INFO_ARRAY(out, cl_context_properties, clGetContextInfo, m_context, CL_CONTEXT_PROPERTIES, errorCode);

	return errorCode;
}


#if CL_CPP_ENABLE_OPENCL_1_1
	inline cl_int CLcpp::Context::GetNumDevices(cl_uint& out) const
	{
		return clGetContextInfo(m_context, CL_CONTEXT_NUM_DEVICES, sizeof(cl_uint), &out, nullptr);
	}
#endif
