/*******************************************************************************
Copyright (c) 2013, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

inline CLcpp::Kernel::Kernel()
	: m_kernel(nullptr)
{
}


inline CLcpp::Kernel::Kernel(const Kernel& other)
	: m_kernel(other.m_kernel)
{
	if(m_kernel)
	{
		clRetainKernel(m_kernel);
	}
}


inline CLcpp::Kernel::Kernel(Kernel&& other)
	: m_kernel(other.m_kernel)
{
	other.m_kernel = nullptr;
}


inline CLcpp::Kernel::Kernel(cl_kernel kernel)
	: m_kernel(kernel)
{
}


inline CLcpp::Kernel::~Kernel()
{
	if(m_kernel)
	{
		clReleaseKernel(m_kernel);
	}
}


inline CLcpp::Kernel::operator cl_kernel() const
{
	return m_kernel;
}


inline CLcpp::Kernel& CLcpp::Kernel::operator=(const Kernel& other)
{
	if(m_kernel)
	{
		clReleaseKernel(m_kernel);
	}

	m_kernel = other.m_kernel;

	if(m_kernel)
	{
		clRetainKernel(m_kernel);
	}

	return (*this);
}


inline CLcpp::Kernel& CLcpp::Kernel::operator=(Kernel&& other)
{
	if(m_kernel)
	{
		clReleaseKernel(m_kernel);
	}

	m_kernel = other.m_kernel;
	other.m_kernel = nullptr;

	return (*this);
}


inline cl_int CLcpp::Kernel::Create(Kernel& outKernel, const Program& program, const char* const functionName)
{
	cl_int errorCode = CL_SUCCESS;
	cl_kernel krnl = clCreateKernel(program, functionName, &errorCode);

	if(errorCode == CL_SUCCESS)
	{
		outKernel = std::move(Kernel(krnl));
	}

	return errorCode;
}


inline cl_int CLcpp::Kernel::CreateAll(std::vector<Kernel>& outKernelVector, const Program& program)
{
	outKernelVector.clear();

	cl_uint numKernels = 0;
	cl_int errorCode = clCreateKernelsInProgram(program, 0, nullptr, &numKernels);

	if(errorCode == CL_SUCCESS && numKernels > 0)
	{
		std::vector<cl_kernel> tempKernelVector;

		outKernelVector.reserve(numKernels);
		tempKernelVector.reserve(numKernels);

		// Create a kernel object for each kernel function in the project.
		errorCode = clCreateKernelsInProgram(program, numKernels, &tempKernelVector[0], nullptr);

		if(errorCode == CL_SUCCESS)
		{
			for(cl_kernel krnl : tempKernelVector)
			{
				// Add each kernel to the output vector.
				outKernelVector.push_back(std::move(Kernel(krnl)));
			}
		}
	}

	return errorCode;
}


inline cl_int CLcpp::Kernel::GetFunctionName(std::string& out) const
{
	return prv_getInfoString(out, CL_KERNEL_FUNCTION_NAME);
}


inline cl_int CLcpp::Kernel::GetNumArgs(cl_uint& out) const
{
	return clGetKernelInfo(m_kernel, CL_KERNEL_NUM_ARGS, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Kernel::GetReferenceCount(cl_uint& out) const
{
	return clGetKernelInfo(m_kernel, CL_KERNEL_REFERENCE_COUNT, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Kernel::GetContext(Context& out) const
{
	cl_context ctx;
	const cl_int errorCode = clGetKernelInfo(m_kernel, CL_KERNEL_CONTEXT, sizeof(cl_context), &ctx, nullptr);

	if(errorCode == CL_SUCCESS)
	{
		out = std::move(Context(ctx));
	}

	return errorCode;
}


inline cl_int CLcpp::Kernel::GetProgram(Program& out) const
{
	cl_program prg;
	const cl_int errorCode = clGetKernelInfo(m_kernel, CL_KERNEL_PROGRAM, sizeof(cl_program), &prg, nullptr);

	if(errorCode == CL_SUCCESS)
	{
		out = std::move(Program(prg));
	}

	return errorCode;
}


inline cl_int CLcpp::Kernel::SetArgument(const cl_uint argIndex, const MemBuffer& memBuffer) const
{
	cl_mem mem = memBuffer;

	return clSetKernelArg(m_kernel, argIndex, sizeof(cl_mem), &mem);
}


inline cl_int CLcpp::Kernel::SetArgument(const cl_uint argIndex, const Sampler& sampler) const
{
	cl_sampler samp = sampler;

	return clSetKernelArg(m_kernel, argIndex, sizeof(cl_sampler), &samp);
}


inline cl_int CLcpp::Kernel::SetArgument(const cl_uint argIndex, const cl_char data) const
{
	return clSetKernelArg(m_kernel, argIndex, sizeof(cl_char), &data);
}


inline cl_int CLcpp::Kernel::SetArgument(const cl_uint argIndex, const cl_uchar data) const
{
	return clSetKernelArg(m_kernel, argIndex, sizeof(cl_uchar), &data);
}


inline cl_int CLcpp::Kernel::SetArgument(const cl_uint argIndex, const cl_short data) const
{
	return clSetKernelArg(m_kernel, argIndex, sizeof(cl_short), &data);
}


inline cl_int CLcpp::Kernel::SetArgument(const cl_uint argIndex, const cl_ushort data) const
{
	return clSetKernelArg(m_kernel, argIndex, sizeof(cl_ushort), &data);
}


inline cl_int CLcpp::Kernel::SetArgument(const cl_uint argIndex, const cl_int data) const
{
	return clSetKernelArg(m_kernel, argIndex, sizeof(cl_int), &data);
}


inline cl_int CLcpp::Kernel::SetArgument(const cl_uint argIndex, const cl_uint data) const
{
	return clSetKernelArg(m_kernel, argIndex, sizeof(cl_uint), &data);
}


inline cl_int CLcpp::Kernel::SetArgument(const cl_uint argIndex, const cl_long data) const
{
	return clSetKernelArg(m_kernel, argIndex, sizeof(cl_long), &data);
}


inline cl_int CLcpp::Kernel::SetArgument(const cl_uint argIndex, const cl_ulong data) const
{
	return clSetKernelArg(m_kernel, argIndex, sizeof(cl_ulong), &data);
}


inline cl_int CLcpp::Kernel::SetArgument(const cl_uint argIndex, const cl_float data) const
{
	return clSetKernelArg(m_kernel, argIndex, sizeof(cl_float), &data);
}


inline cl_int CLcpp::Kernel::SetArgument(const cl_uint argIndex, const cl_double data) const
{
	return clSetKernelArg(m_kernel, argIndex, sizeof(cl_double), &data);
}


inline cl_int CLcpp::Kernel::SetArgument(const cl_uint argIndex, const size_t argSize, const void* const argData) const
{
	return clSetKernelArg(m_kernel, argIndex, argSize, argData);
}


inline cl_int CLcpp::Kernel::prv_getInfoString(std::string& outString, const cl_kernel_info info) const
{
	cl_int errorCode = CL_SUCCESS;
	char* tempString = nullptr;

	CL_CPP_GET_INFO_CSTRING_1ARG(clGetKernelInfo, m_kernel, info, tempString, errorCode);

	if(tempString)
	{
		outString = std::string(std::move(tempString));
		Util::Free(tempString);
	}

	return errorCode;
}


#if CL_CPP_ENABLE_OPENCL_1_2
	inline cl_int CLcpp::Kernel::GetAttributes(std::string& out) const
	{
		return prv_getInfoString(out, CL_KERNEL_ATTRIBUTES);
	}
#endif
