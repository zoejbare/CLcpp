/*******************************************************************************
Copyright (c) 2013, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

inline CLcpp::Device::Device()
	: m_id(nullptr)
{
}


inline CLcpp::Device::Device(const Device& other)
	: m_id(other.m_id)
{
#if CL_CPP_ENABLE_OPENCL_1_2
	if(m_id)
	{
		clRetainDevice(m_id);
	}
#endif
}


inline CLcpp::Device::Device(Device&& other)
	: m_id(other.m_id)
{
	other.m_id = nullptr;
}


inline CLcpp::Device::Device(const cl_device_id id)
	: m_id(id)
{
}


inline CLcpp::Device::~Device()
{
#if CL_CPP_ENABLE_OPENCL_1_2
	if(m_id)
	{
		clReleaseDevice(m_id);
	}
#endif
}


inline CLcpp::Device::operator cl_device_id() const
{
	return m_id;
}


inline CLcpp::Device& CLcpp::Device::operator=(const Device& other)
{
#if CL_CPP_ENABLE_OPENCL_1_2
	if(m_id)
	{
		clReleaseDevice(m_id);
	}
#endif

	m_id = other.m_id;

#if CL_CPP_ENABLE_OPENCL_1_2
	if(m_id)
	{
		clRetainDevice(m_id);
	}
#endif

	return (*this);
}


inline CLcpp::Device& CLcpp::Device::operator=(Device&& other)
{
#if CL_CPP_ENABLE_OPENCL_1_2
	if(m_id)
	{
		clReleaseDevice(m_id);
	}
#endif

	m_id = other.m_id;
	other.m_id = nullptr;

	return (*this);
}


inline cl_int CLcpp::Device::Load(std::vector<Device>& outDeviceVector, const Platform& platform, const cl_device_type typeMask)
{
	outDeviceVector.clear();

	// Get the number of available devices of the specified type.
	cl_uint numDevices = 0;
	cl_int errorCode = clGetDeviceIDs(platform, typeMask, 0, nullptr, &numDevices);
	if(errorCode != CL_SUCCESS)
	{
		return errorCode;
	}

	if(numDevices > 0)
	{
		std::vector<cl_device_id> idVector;

		// Allocate enough space for each device ID.
		idVector.resize(numDevices);

		// Get all available devices of the specified type.
		errorCode = clGetDeviceIDs(platform, typeMask, numDevices, &idVector[0], nullptr);

		// If retrieving the devices was successful, copy the IDs into the output vector.
		if(errorCode == CL_SUCCESS)
		{
			outDeviceVector.reserve(numDevices);

			for(cl_device_id dev : idVector)
			{
				outDeviceVector.push_back(std::move(Device(dev)));
			}
		}
	}

	return errorCode;
}


inline cl_int CLcpp::Device::GetDeviceType(cl_device_type& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_TYPE, sizeof(cl_device_type), &out, nullptr);
}


inline cl_int CLcpp::Device::GetVendorId(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_VENDOR_ID, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetMaxComputeUnits(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetMaxWorkItemDimensions(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetMaxWorkGroupSize(size_t& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &out, nullptr);
}


inline cl_int CLcpp::Device::GetMaxWorkItemSizes(std::vector<size_t>& out) const
{
	cl_int errorCode = CL_SUCCESS;
	CL_CPP_GET_INFO_ARRAY(out, size_t, clGetDeviceInfo, m_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, errorCode);

	return errorCode;
}


inline cl_int CLcpp::Device::GetPreferredVectorWidthChar(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetPreferredVectorWidthShort(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetPreferredVectorWidthInt(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetPreferredVectorWidthLong(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetPreferredVectorWidthFloat(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetPreferredVectorWidthDouble(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetMaxClockFrequency(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetAddressBits(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_ADDRESS_BITS, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetMaxReadImageArgs(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_MAX_READ_IMAGE_ARGS, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetMaxWriteImageArgs(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_MAX_WRITE_IMAGE_ARGS, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetMaxMemAllocSize(cl_ulong& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(cl_ulong), &out, nullptr);
}


inline cl_int CLcpp::Device::GetImage2DMaxWidth(size_t& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_IMAGE2D_MAX_WIDTH, sizeof(size_t), &out, nullptr);
}


inline cl_int CLcpp::Device::GetImage2DMaxHeight(size_t& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_IMAGE2D_MAX_HEIGHT, sizeof(size_t), &out, nullptr);
}


inline cl_int CLcpp::Device::GetImage3DMaxWidth(size_t& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_IMAGE3D_MAX_WIDTH, sizeof(size_t), &out, nullptr);
}


inline cl_int CLcpp::Device::GetImage3DMaxHeight(size_t& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_IMAGE3D_MAX_HEIGHT, sizeof(size_t), &out, nullptr);
}


inline cl_int CLcpp::Device::GetImage3DMaxDepth(size_t& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_IMAGE3D_MAX_DEPTH, sizeof(size_t), &out, nullptr);
}


inline cl_int CLcpp::Device::GetImageSupport(cl_bool& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_IMAGE_SUPPORT, sizeof(cl_bool), &out, nullptr);
}


inline cl_int CLcpp::Device::GetMaxParameterSize(size_t& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_MAX_PARAMETER_SIZE, sizeof(size_t), &out, nullptr);
}


inline cl_int CLcpp::Device::GetMaxSamplers(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_MAX_SAMPLERS, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetMemBaseAddrAlign(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_MEM_BASE_ADDR_ALIGN, sizeof(cl_uint), &out, nullptr);
}


#if CL_CPP_ENABLE_DEPRECATED_1_0_APIS
	inline cl_int CLcpp::Device::GetMinDataTypeAlignSize(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE, sizeof(cl_uint), &out, nullptr);
	}
#endif


inline cl_int CLcpp::Device::GetSingleFpConfig(cl_device_fp_config& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_SINGLE_FP_CONFIG, sizeof(cl_device_fp_config), &out, nullptr);
}


inline cl_int CLcpp::Device::GetGlobalMemCacheType(cl_device_mem_cache_type& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_GLOBAL_MEM_CACHE_TYPE, sizeof(cl_device_mem_cache_type), &out, nullptr);
}


inline cl_int CLcpp::Device::GetGlobalMemCachelineSize(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetGlobalMemCacheSize(cl_ulong& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, sizeof(cl_ulong), &out, nullptr);
}


inline cl_int CLcpp::Device::GetGlobalMemSize(cl_ulong& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), &out, nullptr);
}


inline cl_int CLcpp::Device::GetMaxConstantBufferSize(cl_ulong& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, sizeof(cl_ulong), &out, nullptr);
}


inline cl_int CLcpp::Device::GetMaxConstantArgs(cl_uint& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_MAX_CONSTANT_ARGS, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Device::GetLocalMemType(cl_device_local_mem_type& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_LOCAL_MEM_TYPE, sizeof(cl_device_local_mem_type), &out, nullptr);
}


inline cl_int CLcpp::Device::GetLocalMemSize(cl_ulong& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(cl_ulong), &out, nullptr);
}


inline cl_int CLcpp::Device::GetErrorCorrectionSupport(cl_bool& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_ERROR_CORRECTION_SUPPORT, sizeof(cl_bool), &out, nullptr);
}


inline cl_int CLcpp::Device::GetProfilingTimerResolution(size_t& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_PROFILING_TIMER_RESOLUTION, sizeof(size_t), &out, nullptr);
}


inline cl_int CLcpp::Device::GetEndianLittle(cl_bool& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_ENDIAN_LITTLE, sizeof(cl_bool), &out, nullptr);
}


inline cl_int CLcpp::Device::GetDeviceAvailable(cl_bool& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_AVAILABLE, sizeof(cl_bool), &out, nullptr);
}


inline cl_int CLcpp::Device::GetCompilerAvailable(cl_bool& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_COMPILER_AVAILABLE, sizeof(cl_bool), &out, nullptr);
}


inline cl_int CLcpp::Device::GetExecutionCapabilities(cl_device_exec_capabilities& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_EXECUTION_CAPABILITIES, sizeof(cl_device_exec_capabilities), &out, nullptr);
}


#if CL_CPP_ENABLE_DEPRECATED_1_0_APIS
	inline cl_int CLcpp::Device::GetCommandQueueProperties(cl_command_queue_properties& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_QUEUE_PROPERTIES, sizeof(cl_command_queue_properties), &out, nullptr);
	}
#endif


inline cl_int CLcpp::Device::GetName(std::string& out) const
{
	return prv_getString(out, CL_DEVICE_NAME);
}


inline cl_int CLcpp::Device::GetVendor(std::string& out) const
{
	return prv_getString(out, CL_DEVICE_VENDOR);
}


inline cl_int CLcpp::Device::GetDriverVersion(std::string& out) const
{
	return prv_getString(out, CL_DRIVER_VERSION);
}


inline cl_int CLcpp::Device::GetProfile(std::string& out) const
{
	return prv_getString(out, CL_DEVICE_PROFILE);
}


inline cl_int CLcpp::Device::GetApiVersion(std::string& out) const
{
	return prv_getString(out, CL_DEVICE_VERSION);
}


inline cl_int CLcpp::Device::GetExtensions(std::string& out) const
{
	return prv_getString(out, CL_DEVICE_EXTENSIONS);
}


inline cl_int CLcpp::Device::GetPlatform(Platform& out) const
{
	cl_platform_id plat;
	const cl_int errorCode = clGetDeviceInfo(m_id, CL_DEVICE_PLATFORM, sizeof(cl_platform_id), &plat, nullptr);

	if(errorCode == CL_SUCCESS)
	{
		out = std::move(Platform(plat));
	}

	return errorCode;
}


inline cl_int CLcpp::Device::GetDoubleFpConfig(cl_device_fp_config& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_DOUBLE_FP_CONFIG, sizeof(cl_device_fp_config), &out, nullptr);
}


inline cl_int CLcpp::Device::GetHalfFpConfig(cl_device_fp_config& out) const
{
	return clGetDeviceInfo(m_id, CL_DEVICE_HALF_FP_CONFIG, sizeof(cl_device_fp_config), &out, nullptr);
}


#if CL_CPP_ENABLE_OPENCL_1_1
	inline cl_int CLcpp::Device::GetPreferredVectorWidthHalf(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF, sizeof(cl_uint), &out, nullptr);
	}


	#if CL_CPP_ENABLE_DEPRECATED_1_1_APIS
		inline cl_int CLcpp::Device::GetHostUnifiedMemory(cl_bool& out) const
		{
			return clGetDeviceInfo(m_id, CL_DEVICE_HOST_UNIFIED_MEMORY, sizeof(cl_bool), &out, nullptr);
		}
	#endif


	inline cl_int CLcpp::Device::GetNativeVectorWidthChar(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetNativeVectorWidthShort(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetNativeVectorWidthInt(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_NATIVE_VECTOR_WIDTH_INT, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetNativeVectorWidthLong(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetNativeVectorWidthFloat(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetNativeVectorWidthDouble(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetNativeVectorWidthHalf(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetOpenClCVersion(std::string& out) const
	{
		return prv_getString(out, CL_DEVICE_OPENCL_C_VERSION);
	}
#endif


#if CL_CPP_ENABLE_OPENCL_1_2
	inline cl_int CLcpp::Device::GetLinkerAvailable(cl_bool& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_LINKER_AVAILABLE, sizeof(cl_bool), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetBuiltInKernels(std::string& out) const
	{
		return prv_getString(out, CL_DEVICE_BUILT_IN_KERNELS);
	}


	inline cl_int CLcpp::Device::GetImageMaxBufferSize(size_t& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_IMAGE_MAX_BUFFER_SIZE, sizeof(size_t), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetImageMaxArraySize(size_t& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_IMAGE_MAX_ARRAY_SIZE, sizeof(size_t), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetParentDevice(Device& out) const
	{
		cl_device_id dev;
		const cl_int errorCode = clGetDeviceInfo(m_id, CL_DEVICE_PARENT_DEVICE, sizeof(cl_device_id), &dev, nullptr);

		if(errorCode == CL_SUCCESS)
		{
			out = std::move(Device(dev));
		}

		return errorCode;
	}


	inline cl_int CLcpp::Device::GetPartitionProperties(std::vector<cl_device_partition_property>& out)
	{
		cl_int errorCode = CL_SUCCESS;
		CL_CPP_GET_INFO_ARRAY(out, cl_device_partition_property, clGetDeviceInfo, m_id, CL_DEVICE_PARTITION_PROPERTIES, errorCode);

		return errorCode;
	}


	inline cl_int CLcpp::Device::GetPartitionMaxSubDevices(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_PARTITION_MAX_SUB_DEVICES, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetAffinityDomain(cl_device_affinity_domain& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_PARTITION_AFFINITY_DOMAIN, sizeof(cl_device_affinity_domain), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetPartitionTypes(std::vector<cl_device_partition_property>& out)
	{
		cl_int errorCode = CL_SUCCESS;
		CL_CPP_GET_INFO_ARRAY(out, cl_device_partition_property, clGetDeviceInfo, m_id, CL_DEVICE_PARTITION_TYPE, errorCode);

		return errorCode;
	}


	inline cl_int CLcpp::Device::GetReferenceCount(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_REFERENCE_COUNT, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetPreferredInteropUserSync(cl_bool& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_PREFERRED_INTEROP_USER_SYNC, sizeof(cl_bool), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetPrintfBufferSize(size_t& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_PRINTF_BUFFER_SIZE, sizeof(size_t), &out, nullptr);
	}
#endif


#if CL_CPP_ENABLE_OPENCL_2_0
	inline cl_int CLcpp::Device::GetQueueOnHostProperties(cl_command_queue_properties& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_QUEUE_ON_HOST_PROPERTIES, sizeof(cl_command_queue_properties), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetImagePitchAlignment(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_IMAGE_PITCH_ALIGNMENT, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetImageBaseAddressAlignment(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_IMAGE_BASE_ADDRESS_ALIGNMENT, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetMaxReadWriteImageArgs(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_MAX_READ_WRITE_IMAGE_ARGS, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetMaxGlobalVariableSize(size_t& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_MAX_GLOBAL_VARIABLE_SIZE, sizeof(size_t), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetQueueOnDeviceProperties(cl_command_queue_properties& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_QUEUE_ON_DEVICE_PROPERTIES, sizeof(cl_command_queue_properties), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetQueueOnDevicePreferredSize(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_QUEUE_ON_DEVICE_PREFERRED_SIZE, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetQueueOnDeviceMaxSize(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_QUEUE_ON_DEVICE_MAX_SIZE, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetMaxOnDeviceQueues(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_MAX_ON_DEVICE_QUEUES, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetMaxOnDeviceEvents(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_MAX_ON_DEVICE_EVENTS, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetSvmCapabilities(cl_device_svm_capabilities& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_SVM_CAPABILITIES, sizeof(cl_device_svm_capabilities), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetGlobalVariablePreferredTotalSize(size_t& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE, sizeof(size_t), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetMaxPipeArgs(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_MAX_PIPE_ARGS, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetPipeMaxActiveReservations(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_PIPE_MAX_ACTIVE_RESERVATIONS, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetPipeMaxPacketSize(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_PIPE_MAX_PACKET_SIZE, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetPreferredPlatformAtomicAlignment(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetPreferredGlobalAtomicAlignment(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetPreferredLocalAtomicAlignment(cl_uint& out) const
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_PREFERRED_LOCAL_ATOMIC_ALIGNMENT, sizeof(cl_uint), &out, nullptr);
	}
#endif


#if CL_CPP_ENABLE_OPENCL_2_1
	inline cl_int CLcpp::Device::GetILVersion(std::string& out)
	{
		return prv_getString(out, CL_DEVICE_IL_VERSION);
	}


	inline cl_int CLcpp::Device::GetMaxNumSubGroups(cl_uint& out)
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_MAX_NUM_SUB_GROUPS, sizeof(cl_uint), &out, nullptr);
	}


	inline cl_int CLcpp::Device::GetSubGroupIndependentForwardProgress(cl_bool& out)
	{
		return clGetDeviceInfo(m_id, CL_DEVICE_SUB_GROUP_INDEPENDENT_FORWARD_PROGRESS, sizeof(cl_bool), &out, nullptr);
	}
#endif


inline cl_int CLcpp::Device::prv_getString(std::string& outString, const cl_device_info info) const
{
	cl_int errorCode = CL_SUCCESS;
	char* tempString = nullptr;

	CL_CPP_GET_INFO_CSTRING_1ARG(clGetDeviceInfo, m_id, info, tempString, errorCode);

	if(tempString)
	{
		outString = std::string(std::move(tempString));
		Util::Free(tempString);
	}

	return errorCode;
}
