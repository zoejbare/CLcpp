/*******************************************************************************
Copyright (c) 2013, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

inline CLcpp::Event::Event()
	: m_event(nullptr)
{
}


inline CLcpp::Event::Event(const Event& other)
	: m_event(other.m_event)
{
	if(m_event)
	{
		clRetainEvent(m_event);
	}
}


inline CLcpp::Event::Event(Event&& other)
	: m_event(other.m_event)
{
	other.m_event = nullptr;
}


inline CLcpp::Event::Event(cl_event evt)
	: m_event(evt)
{
}


inline CLcpp::Event::~Event()
{
	if(m_event)
	{
		clReleaseEvent(m_event);
	}
}


inline CLcpp::Event::operator cl_event() const
{
	return m_event;
}


inline CLcpp::Event& CLcpp::Event::operator=(const Event& other)
{
	if(m_event)
	{
		clReleaseEvent(m_event);
	}

	m_event = other.m_event;

	if(m_event)
	{
		clRetainEvent(m_event);
	}

	return (*this);
}


inline CLcpp::Event& CLcpp::Event::operator=(Event&& other)
{
	if(m_event)
	{
		clReleaseEvent(m_event);
	}

	m_event = other.m_event;
	other.m_event = nullptr;

	return (*this);
}


inline cl_int CLcpp::Event::WaitForEvents(const std::vector<Event>& eventVector)
{
	cl_int errorCode = CL_SUCCESS;

	// Only wait if there are events to wait on.
	if(eventVector.size() > 0)
	{
		const cl_uint numEvents = cl_uint(eventVector.size());

		errorCode = clWaitForEvents(numEvents, (const cl_event*) &eventVector[0]);
	}

	return errorCode;
}


inline cl_int CLcpp::Event::GetProfileTime(cl_ulong& out, const cl_profiling_info profilingInfo) const
{
	return clGetEventProfilingInfo(m_event, profilingInfo, sizeof(cl_ulong), &out, nullptr);
}


inline cl_int CLcpp::Event::GetCommandQueue(CommandQueue& out) const
{
	cl_command_queue queue;
	const cl_int errorCode = clGetEventInfo(m_event, CL_EVENT_COMMAND_QUEUE, sizeof(cl_command_queue), &queue, nullptr);

	if(errorCode == CL_SUCCESS)
	{
		out = std::move(CommandQueue(queue));
	}

	return errorCode;
}


inline cl_int CLcpp::Event::GetCommandType(cl_command_type& out) const
{
	return clGetEventInfo(m_event, CL_EVENT_COMMAND_TYPE, sizeof(cl_command_type), &out, nullptr);
}


inline cl_int CLcpp::Event::GetReferenceCount(cl_uint& out) const
{
	return clGetEventInfo(m_event, CL_EVENT_REFERENCE_COUNT, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Event::GetCommandExecutionStatus(cl_int& out) const
{
	return clGetEventInfo(m_event, CL_EVENT_COMMAND_EXECUTION_STATUS, sizeof(cl_int), &out, nullptr);
}


#if CL_CPP_ENABLE_OPENCL_1_1
	inline cl_int CLcpp::Event::GetContext(Context& out) const
	{
		cl_context ctx;
		const cl_int errorCode = clGetEventInfo(m_event, CL_EVENT_CONTEXT, sizeof(cl_context), &ctx, nullptr);

		if(errorCode == CL_SUCCESS)
		{
			out = std::move(Context(ctx));
		}

		return errorCode;
	}


	inline cl_int CLcpp::Event::CreateUserEvent(Event& outEvent, const Context& context)
	{
		cl_int errorCode = CL_SUCCESS;
		cl_event evt = clCreateUserEvent(context, &errorCode);

		if(errorCode == CL_SUCCESS)
		{
			outEvent = std::move(Event(evt));
		}

		return errorCode;
	}


	inline cl_int CLcpp::Event::SetUserEventStatus(const cl_int status) const
	{
		return clSetUserEventStatus(m_event, status);
	}


	inline cl_int CLcpp::Event::SetNotifyCallback(const cl_int execStatus, const EventNotifyCallback callback, void* const userData) const
	{
		return clSetEventCallback(m_event, execStatus, callback, userData);
	}
#endif
