/*******************************************************************************
Copyright (c) 2013, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

inline CLcpp::Platform::Platform()
	: m_id(nullptr)
{
}


inline CLcpp::Platform::Platform(const cl_platform_id id)
	: m_id(id)
{
}


inline CLcpp::Platform::operator cl_platform_id() const
{
	return m_id;
}


inline cl_int CLcpp::Platform::Load(std::vector<Platform>& outPlatformVector)
{
	outPlatformVector.clear();

	cl_uint numPlatforms = 0;
	cl_int errorCode = CL_SUCCESS;

	// Get the number of available platforms.
	errorCode = clGetPlatformIDs(0, nullptr, &numPlatforms);
	if(errorCode != CL_SUCCESS)
	{
		return errorCode;
	}

	if(numPlatforms > 0)
	{
		std::vector<cl_platform_id> idVector;

		// Allocate enough space for each platform ID.
		idVector.resize(numPlatforms);

		// Get all available platforms.
		errorCode = clGetPlatformIDs(numPlatforms, &idVector[0], nullptr);

		// If retrieving the platforms was successful, copy the IDs into the output vector.
		if(errorCode == CL_SUCCESS)
		{
			outPlatformVector.reserve(numPlatforms);

			for(cl_platform_id plat : idVector)
			{
				outPlatformVector.push_back(std::move(Platform(plat)));
			}
		}
	}

	return errorCode;
}


inline cl_int CLcpp::Platform::GetProfile(std::string& out) const
{
	return prv_getString(out, CL_PLATFORM_PROFILE);
}


inline cl_int CLcpp::Platform::GetVersion(std::string& out) const
{
	return prv_getString(out, CL_PLATFORM_VERSION);
}


inline cl_int CLcpp::Platform::GetName(std::string& out) const
{
	return prv_getString(out, CL_PLATFORM_NAME);
}


inline cl_int CLcpp::Platform::GetVendor(std::string& out) const
{
	return prv_getString(out, CL_PLATFORM_VENDOR);
}


inline cl_int CLcpp::Platform::GetExtensions(std::string& out) const
{
	return prv_getString(out, CL_PLATFORM_EXTENSIONS);
}


inline cl_int CLcpp::Platform::prv_getString(std::string& outString, const cl_platform_info info) const
{
	cl_int errorCode = CL_SUCCESS;
	char* tempString = nullptr;

	CL_CPP_GET_INFO_CSTRING_1ARG(clGetPlatformInfo, m_id, info, tempString, errorCode);

	if(tempString)
	{
		outString = std::string(std::move(tempString));
		Util::Free(tempString);
	}

	return errorCode;
}
