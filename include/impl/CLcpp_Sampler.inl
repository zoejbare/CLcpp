/*******************************************************************************
Copyright (c) 2013, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

inline CLcpp::Sampler::Sampler()
	: m_sampler(nullptr)
{
}


inline CLcpp::Sampler::Sampler(const Sampler& other)
	: m_sampler(other.m_sampler)
{
	if(m_sampler)
	{
		clRetainSampler(m_sampler);
	}
}


inline CLcpp::Sampler::Sampler(Sampler&& other)
	: m_sampler(other.m_sampler)
{
	other.m_sampler = nullptr;
}


inline CLcpp::Sampler::Sampler(cl_sampler samp)
	: m_sampler(samp)
{
}


inline CLcpp::Sampler::~Sampler()
{
	if(m_sampler)
	{
		clReleaseSampler(m_sampler);
	}
}


inline CLcpp::Sampler::operator cl_sampler() const
{
	return m_sampler;
}


inline CLcpp::Sampler& CLcpp::Sampler::operator=(const Sampler& other)
{
	if(m_sampler)
	{
		clReleaseSampler(m_sampler);
	}

	m_sampler = other.m_sampler;

	if(m_sampler)
	{
		clRetainSampler(m_sampler);
	}

	return (*this);
}


inline CLcpp::Sampler& CLcpp::Sampler::operator=(Sampler&& other)
{
	if(m_sampler)
	{
		clReleaseSampler(m_sampler);
	}

	m_sampler = other.m_sampler;
	other.m_sampler = nullptr;

	return (*this);
}


inline cl_int CLcpp::Sampler::Create(
	Sampler& outSampler,
	const Context& context,
	const bool useNormalizedCoords,
	const cl_addressing_mode addressingMode,
	const cl_filter_mode filterMode
)
{
	cl_int errorCode = CL_SUCCESS;

#if CL_CPP_ENABLE_OPENCL_2_0
	const cl_sampler_properties propertyArray[] =
	{
		CL_SAMPLER_NORMALIZED_COORDS, cl_sampler_properties((useNormalizedCoords) ? CL_TRUE : CL_FALSE),
		CL_SAMPLER_ADDRESSING_MODE, addressingMode,
		CL_SAMPLER_FILTER_MODE, filterMode,

		0,
	};

	cl_sampler samp = clCreateSamplerWithProperties(context, propertyArray, &errorCode);
#else
	cl_sampler samp = clCreateSampler(
		context,
		(useNormalizedCoords) ? CL_TRUE : CL_FALSE,
		addressingMode,
		filterMode,
		&errorCode
	);
#endif

	if(errorCode == CL_SUCCESS)
	{
		outSampler = std::move(Sampler(samp));
	}

	return errorCode;
}


inline cl_int CLcpp::Sampler::GetReferenceCount(cl_uint& out) const
{
	return clGetSamplerInfo(m_sampler, CL_SAMPLER_REFERENCE_COUNT, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Sampler::GetContext(Context& out) const
{
	cl_context ctx;
	const cl_int errorCode = clGetSamplerInfo(m_sampler, CL_SAMPLER_CONTEXT, sizeof(cl_context), &ctx, nullptr);

	if(errorCode == CL_SUCCESS)
	{
		out = std::move(Context(ctx));
	}

	return errorCode;
}


inline cl_int CLcpp::Sampler::GetNormalizedCoords(cl_bool& out) const
{
	return clGetSamplerInfo(m_sampler, CL_SAMPLER_NORMALIZED_COORDS, sizeof(cl_bool), &out, nullptr);
}


inline cl_int CLcpp::Sampler::GetAddressingMode(cl_addressing_mode& out) const
{
	return clGetSamplerInfo(m_sampler, CL_SAMPLER_ADDRESSING_MODE, sizeof(cl_addressing_mode), &out, nullptr);
}


inline cl_int CLcpp::Sampler::GetFilterMode(cl_filter_mode& out) const
{
	return clGetSamplerInfo(m_sampler, CL_SAMPLER_FILTER_MODE, sizeof(cl_filter_mode), &out, nullptr);
}
