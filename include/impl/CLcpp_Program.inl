/*******************************************************************************
Copyright (c) 2013, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

inline CLcpp::Program::Program()
	: m_program(nullptr)
{
}


inline CLcpp::Program::Program(const Program& other)
	: m_program(other.m_program)
{
	if(m_program)
	{
		clRetainProgram(m_program);
	}
}


inline CLcpp::Program::Program(Program&& other)
	: m_program(other.m_program)
{
	other.m_program = nullptr;
}


inline CLcpp::Program::Program(cl_program program)
	: m_program(program)
{
}


inline CLcpp::Program::~Program()
{
	if(m_program)
	{
		clReleaseProgram(m_program);
	}
}


inline CLcpp::Program::operator cl_program() const
{
	return m_program;
}


inline CLcpp::Program& CLcpp::Program::operator=(const Program& other)
{
	if(m_program)
	{
		clReleaseProgram(m_program);
	}

	m_program = other.m_program;

	if(m_program)
	{
		clRetainProgram(m_program);
	}

	return (*this);
}


inline CLcpp::Program& CLcpp::Program::operator=(Program&& other)
{
	if(m_program)
	{
		clReleaseProgram(m_program);
	}

	m_program = other.m_program;
	other.m_program = nullptr;

	return (*this);
}


inline cl_int CLcpp::Program::CreateFromSources(Program& outProgram, const Context& context, const std::vector<ProgramSource>& programSourceVector)
{
	std::vector<const char*> fileBufferVector;
	std::vector<size_t> fileSizeVector;

	// Allocate enough space for each program source.
	fileBufferVector.reserve(programSourceVector.size());
	fileSizeVector.reserve(programSourceVector.size());

	// Add the data for each program to the temporary vectors.
	for(const ProgramSource& source : programSourceVector)
	{
		fileBufferVector.push_back(source.fileBuffer);
		fileSizeVector.push_back(source.fileSize);
	}

	return prv_createFromSources(outProgram, context, fileBufferVector, fileSizeVector);
}


inline cl_int CLcpp::Program::CreateFromSources(Program& outProgram, const Context& context, const std::vector<std::string>& programSourceVector)
{
	std::vector<const char*> fileBufferVector;
	std::vector<size_t> fileSizeVector;

	// Allocate enough space for each program source.
	fileBufferVector.reserve(programSourceVector.size());
	fileSizeVector.reserve(programSourceVector.size());

	// Add the data for each program to the temporary vectors.
	for(const std::string& source : programSourceVector)
	{
		fileBufferVector.push_back(source.c_str());
		fileSizeVector.push_back(source.size());
	}

	return prv_createFromSources(outProgram, context, fileBufferVector, fileSizeVector);
}


inline cl_int CLcpp::Program::CreateFromBinaries(
	Program& outProgram,
	std::vector<cl_int>& outBinaryStatusVector,
	const Context& context,
	const std::vector<Device>& deviceVector,
	const std::vector<ProgramBinary>& programBinaryVector
)
{
	std::vector<cl_uchar*> binaryBufferVector;
	std::vector<size_t> binarySizeVector;

	// Allocate enough space for each program binary.
	binaryBufferVector.reserve(programBinaryVector.size());
	binarySizeVector.reserve(programBinaryVector.size());

	// Add the data for each program to the temporary vectors.
	for(const ProgramBinary& data : programBinaryVector)
	{
		binaryBufferVector.push_back(data.binaryBuffer);
		binarySizeVector.push_back(data.binarySize);
	}

	return prv_createFromBinaries(outProgram, outBinaryStatusVector, context, deviceVector, binaryBufferVector, binarySizeVector);
}


inline cl_int CLcpp::Program::CreateFromBinaries(
	Program& outProgram,
	std::vector<cl_int>& outBinaryStatusVector,
	const Context& context,
	const std::vector<Device>& deviceVector,
	const std::vector<std::string>& programBinaryVector
)
{
	std::vector<cl_uchar*> binaryBufferVector;
	std::vector<size_t> binarySizeVector;

	// Allocate enough space for each program binary.
	binaryBufferVector.reserve(programBinaryVector.size());
	binarySizeVector.reserve(programBinaryVector.size());

	// Add the data for each program to the temporary vectors.
	for(const std::string& data : programBinaryVector)
	{
		binaryBufferVector.push_back((cl_uchar*) data.c_str());
		binarySizeVector.push_back(data.size());
	}

	return prv_createFromBinaries(outProgram, outBinaryStatusVector, context, deviceVector, binaryBufferVector, binarySizeVector);
}


inline cl_int CLcpp::Program::Build(
	const std::vector<Device>& deviceVector,
	const char* const buildOptions,
	const ProgramCallback callback,
	void* const userData
)
{
	const cl_uint numDevices = cl_uint(deviceVector.size());
	const cl_device_id* const deviceArray = (numDevices > 0) ? (const cl_device_id*) &deviceVector[0] : nullptr;

	return clBuildProgram(m_program, numDevices, deviceArray, buildOptions, callback, userData);
}


inline cl_int CLcpp::Program::GetReferenceCount(cl_uint& out) const
{
	return clGetProgramInfo(m_program, CL_PROGRAM_REFERENCE_COUNT, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Program::GetContext(Context& out) const
{
	cl_context ctx;
	const cl_int errorCode = clGetProgramInfo(m_program, CL_PROGRAM_CONTEXT, sizeof(cl_context), &ctx, nullptr);

	if(errorCode == CL_SUCCESS)
	{
		out = std::move(Context(ctx));
	}

	return errorCode;
}


inline cl_int CLcpp::Program::GetDevices(std::vector<Device>& out) const
{
	cl_int errorCode = CL_SUCCESS;
	CL_CPP_GET_INFO_ARRAY_CUSTOM_TYPE(out, cl_device_id, Device, clGetProgramInfo, m_program, CL_PROGRAM_DEVICES, errorCode);

	return errorCode;
}


inline cl_int CLcpp::Program::GetNumDevices(cl_uint& out) const
{
	return clGetProgramInfo(m_program, CL_PROGRAM_NUM_DEVICES, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::Program::GetSource(std::string& out) const
{
	return prv_getInfoString(out, CL_PROGRAM_SOURCE);
}


inline cl_int CLcpp::Program::GetBinaries(std::vector<std::string>& out) const
{
	out.clear();

	size_t sizeInBytes = 0;
	cl_uint errorCode = clGetProgramInfo(m_program, CL_PROGRAM_BINARY_SIZES, 0, nullptr, &sizeInBytes);

	if(errorCode == CL_SUCCESS && sizeInBytes > 0)
	{
		const size_t numBinaries = sizeInBytes / sizeof(size_t);

		out.reserve(numBinaries);

		// Allocate enough space for each binary size.
		size_t* const sizeArray = (size_t*) Util::Alloc(sizeInBytes);

		errorCode = clGetProgramInfo(m_program, CL_PROGRAM_BINARY_SIZES, sizeInBytes, sizeArray, nullptr);

		if(errorCode == CL_SUCCESS)
		{
			// Allocate an array of pointers for each binary.
			cl_uchar** const binaryDataArray = (cl_uchar**) Util::Alloc(sizeof(cl_uchar*) * numBinaries);

			for(size_t i = 0; i < numBinaries; ++i)
			{
				// Allocate space for the binary data.
				binaryDataArray[i] = (cl_uchar*) Util::Alloc(sizeArray[i]);
			}

			errorCode = clGetProgramInfo(m_program, CL_PROGRAM_BINARIES, sizeof(cl_uchar*) * numBinaries, (void*) binaryDataArray, nullptr);

			for(size_t i = 0; i < numBinaries; ++i)
			{
				// Add each binary to the output vector.
				if(errorCode == CL_SUCCESS)
				{
					out.push_back(std::move(std::string((char*) binaryDataArray[i], sizeArray[i])));
				}

				// Free the binary data.
				Util::Free(binaryDataArray[i]);
			}

			// Free the array of binary data pointers.
			Util::Free(binaryDataArray);
		}

		// Free the size array.
		Util::Free(sizeArray);
	}

	return errorCode;
}


inline cl_int CLcpp::Program::GetBuildStatus(cl_build_status& out, const Device& device) const
{
	return clGetProgramBuildInfo(m_program, device, CL_PROGRAM_BUILD_STATUS, sizeof(cl_build_status), &out, nullptr);
}


inline cl_int CLcpp::Program::GetBuildOptions(std::string& out, const Device& device) const
{
	return prv_getBuildInfoString(out, device, CL_PROGRAM_BUILD_OPTIONS);
}


inline cl_int CLcpp::Program::GetBuildLog(std::string& out, const Device& device) const
{
	return prv_getBuildInfoString(out, device, CL_PROGRAM_BUILD_LOG);
}


#if CL_CPP_ENABLE_OPENCL_1_2
	inline cl_int CLcpp::Program::CreateFromBuiltInKernels(
		Program& outProgram,
		const Context& context,
		const std::vector<Device>& deviceVector,
		const char* const kernelNames
	)
	{
		cl_int errorCode = CL_SUCCESS;
		cl_program prg = clCreateProgramWithBuiltInKernels(
			context,
			cl_uint(deviceVector.size()),
			(const cl_device_id*) &deviceVector[0],
			kernelNames,
			&errorCode
		);

		if(errorCode == CL_SUCCESS)
		{
			outProgram = std::move(Program(prg));
		}

		return errorCode;
	}


	inline cl_int CLcpp::Program::Compile(
		const std::vector<Device>& deviceVector,
		const std::vector<Program>& headerProgramVector,
		const std::vector<const char*>& headerNameVector,
		const char* const compileOptions,
		const ProgramCallback callback,
		void* const userData
	)
	{
		const cl_uint numDevices = cl_uint(deviceVector.size());
		const cl_device_id* const deviceArray = (numDevices > 0) ? (const cl_device_id*) &deviceVector[0] : nullptr;

		const cl_uint numHeaders = cl_uint(headerProgramVector.size());
		const cl_program* const headerProgramArray = (numHeaders > 0) ? (const cl_program*) &headerProgramVector[0] : nullptr;
		const char** headerNameArray = (numHeaders > 0) ? (const char**) &headerNameVector[0] : nullptr;

		return clCompileProgram(
			m_program,
			numDevices,
			deviceArray,
			compileOptions,
			numHeaders,
			headerProgramArray,
			headerNameArray,
			callback,
			userData
		);
	}


	inline cl_int CLcpp::Program::Link(
		Program& outProgram,
		const Context& context,
		const std::vector<Device>& deviceVector,
		const std::vector<Program>& inputProgramVector,
		const char* const linkOptions,
		const ProgramCallback callback,
		void* const userData
	)
	{
		const cl_uint numDevices = cl_uint(deviceVector.size());
		const cl_device_id* const deviceArray = (numDevices > 0) ? (const cl_device_id*) &deviceVector[0] : nullptr;

		const cl_uint numPrograms = cl_uint(inputProgramVector.size());
		const cl_program* const programArray = (numPrograms > 0) ? (const cl_program*) &inputProgramVector[0] : nullptr;

		cl_int errorCode = CL_SUCCESS;
		cl_program prg = clLinkProgram(context, numDevices, deviceArray, linkOptions, numPrograms, programArray, callback, userData, &errorCode);

		if(errorCode == CL_SUCCESS)
		{
			outProgram = std::move(Program(prg));
		}

		return errorCode;
	}


	inline cl_int CLcpp::Program::GetNumKernels(size_t& out) const
	{
		return clGetProgramInfo(m_program, CL_PROGRAM_NUM_KERNELS, sizeof(size_t), &out, nullptr);
	}


	inline cl_int CLcpp::Program::GetKernelNames(std::string& out) const
	{
		return prv_getInfoString(out, CL_PROGRAM_KERNEL_NAMES);
	}


	inline cl_int CLcpp::Program::GetBinaryType(cl_program_binary_type& out, const Device& device) const
	{
		return clGetProgramBuildInfo(m_program, device, CL_PROGRAM_BINARY_TYPE, sizeof(cl_program_binary_type), &out, nullptr);
	}
#endif


#if CL_CPP_ENABLE_OPENCL_2_0
	inline cl_int CLcpp::Program::GetGlobalVariableTotalSize(size_t& out, const Device& device) const
	{
		return clGetProgramBuildInfo(m_program, device, CL_PROGRAM_BUILD_GLOBAL_VARIABLE_TOTAL_SIZE, sizeof(size_t), &out, nullptr);
	}
#endif


#if CL_CPP_ENABLE_OPENCL_2_1
	inline cl_int CLcpp::Program::CreateFromIL(
		Program& outProgram,
		const Context& context,
		const ProgramBinary& intermediateData
	)
	{
		cl_int errorCode = CL_SUCCESS;
		cl_program prg = clCreateProgramWithIL(context, intermediateData.binaryBuffer, intermediateData.binarySize, &errorCode);

		if(errorCode == CL_SUCCESS)
		{
			outProgram = std::move(Program(prg));
		}

		return errorCode;
	}

	inline cl_int CLcpp::Program::CreateFromIL(
		Program& outProgram,
		const Context& context,
		const std::string& intermediateData
	)
	{
		cl_int errorCode = CL_SUCCESS;
		cl_program prg = clCreateProgramWithIL(context, intermediateData.c_str(), intermediateData.size(), &errorCode);

		if(errorCode == CL_SUCCESS)
		{
			outProgram = std::move(Program(prg));
		}

		return errorCode;
	}
#endif


inline cl_int CLcpp::Program::prv_createFromSources(
	Program& outProgram,
	const Context& context,
	const std::vector<const char*>& fileBufferVector,
	const std::vector<size_t>& fileSizeVector
)
{
	const cl_uint numFileBuffers = cl_uint(fileBufferVector.size());
	const char** const fileBufferArray = (const char**) &fileBufferVector[0];
	const size_t* const fileSizeArray = (size_t*) &fileSizeVector[0];

	cl_int errorCode = CL_SUCCESS;
	cl_program prg = clCreateProgramWithSource(
		context,
		numFileBuffers,
		fileBufferArray,
		fileSizeArray,
		&errorCode
	);

	if(errorCode == CL_SUCCESS)
	{
		outProgram = std::move(Program(prg));
	}

	return errorCode;
}


inline cl_int CLcpp::Program::prv_createFromBinaries(
	Program& outProgram,
	std::vector<cl_int>& outBinaryStatusVector,
	const Context& context,
	const std::vector<Device>& deviceVector,
	const std::vector<cl_uchar*>& binaryBufferVector,
	const std::vector<size_t>& binarySizeVector
)
{
	outBinaryStatusVector.clear();
	outBinaryStatusVector.resize(deviceVector.size());

	const cl_uint numDevices = cl_uint(deviceVector.size());
	const cl_device_id* const deviceArray = (numDevices > 0) ? (const cl_device_id*) &deviceVector[0] : nullptr;

	cl_int errorCode = CL_SUCCESS;
	cl_program prg = clCreateProgramWithBinary(
		context,
		numDevices,
		deviceArray,
		&binarySizeVector[0],
		(const cl_uchar**) &binaryBufferVector[0],
		&outBinaryStatusVector[0],
		&errorCode
	);

	if(errorCode == CL_SUCCESS)
	{
		outProgram = std::move(Program(prg));
	}

	return errorCode;
}


inline cl_int CLcpp::Program::prv_getInfoString(std::string& outString, const cl_program_info info) const
{
	cl_int errorCode = CL_SUCCESS;
	char* tempString = nullptr;

	CL_CPP_GET_INFO_CSTRING_1ARG(clGetProgramInfo, m_program, info, tempString, errorCode);

	if(tempString)
	{
		outString = std::string(std::move(tempString));
		Util::Free(tempString);
	}

	return errorCode;
}


inline cl_int CLcpp::Program::prv_getBuildInfoString(std::string& outString, const Device& device, const cl_program_build_info info) const
{
	cl_int errorCode = CL_SUCCESS;
	char* tempString = nullptr;

	CL_CPP_GET_INFO_CSTRING_2ARG(clGetProgramBuildInfo, m_program, device, info, tempString, errorCode);

	if(tempString)
	{
		outString = std::string(std::move(tempString));
		Util::Free(tempString);
	}

	return errorCode;
}
