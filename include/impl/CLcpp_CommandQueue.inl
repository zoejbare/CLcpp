/*******************************************************************************
Copyright (c) 2013, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

inline CLcpp::CommandQueue::CommandQueue()
	: m_queue(nullptr)
{
}


inline CLcpp::CommandQueue::CommandQueue(const CommandQueue& other)
	: m_queue(other.m_queue)
{
	if(m_queue)
	{
		clRetainCommandQueue(m_queue);
	}
}


inline CLcpp::CommandQueue::CommandQueue(CommandQueue&& other)
	: m_queue(other.m_queue)
{
	other.m_queue = nullptr;
}


inline CLcpp::CommandQueue::CommandQueue(cl_command_queue queue)
	: m_queue(queue)
{
}


inline CLcpp::CommandQueue::~CommandQueue()
{
	if(m_queue)
	{
		clReleaseCommandQueue(m_queue);
	}
}


inline CLcpp::CommandQueue::operator cl_command_queue() const
{
	return m_queue;
}


inline CLcpp::CommandQueue& CLcpp::CommandQueue::operator=(const CommandQueue& other)
{
	if(m_queue)
	{
		clReleaseCommandQueue(m_queue);
	}

	m_queue = other.m_queue;

	if(m_queue)
	{
		clRetainCommandQueue(m_queue);
	}

	return (*this);
}


inline CLcpp::CommandQueue& CLcpp::CommandQueue::operator=(CommandQueue&& other)
{
	if(m_queue)
	{
		clReleaseCommandQueue(m_queue);
	}

	m_queue = other.m_queue;
	other.m_queue = nullptr;

	return (*this);
}


inline cl_int CLcpp::CommandQueue::Create(
	CommandQueue& outQueue,
	const Context& context,
	const Device& device,
	const cl_command_queue_properties propertyMask,
	const cl_uint queueSize
)
{
	cl_int errorCode = CL_SUCCESS;

	cl_command_queue queue;

#if CL_CPP_ENABLE_OPENCL_2_0
	cl_command_queue_properties propsArray[5];
	cl_uint index = 0;

	propsArray[index] = CL_QUEUE_PROPERTIES; ++index;
	propsArray[index] = propertyMask; ++index;

	// Only insert the queue size if it's greater than 0.  When the queue size is omitted the default
	// value of 0 is implied when creating the queue.
	if(queueSize > 0)
	{
		propsArray[index] = CL_QUEUE_SIZE; ++index;
		propsArray[index] = queueSize; ++index;
	}

	// The property array must have a null terminator.
	propsArray[index] = 0;

	queue = clCreateCommandQueueWithProperties(context, device, propsArray, &errorCode);
#else
	(void)(queueSize); // The queue size cannot be used prior to OpenCL 2.0.
	queue = clCreateCommandQueue(context, device, propertyMask, &errorCode);
#endif

	if(errorCode == CL_SUCCESS)
	{
		outQueue = std::move(CommandQueue(queue));
	}

	return errorCode;
}


inline cl_int CLcpp::CommandQueue::GetContext(Context& out) const
{
	cl_context ctx;
	const cl_int errorCode = clGetCommandQueueInfo(m_queue, CL_QUEUE_CONTEXT, sizeof(cl_context), &ctx, nullptr);

	if(errorCode == CL_SUCCESS)
	{
		out = std::move(Context(ctx));
	}

	return errorCode;
}


inline cl_int CLcpp::CommandQueue::GetDevice(Device& out) const
{
	cl_device_id dev;
	const cl_int errorCode = clGetCommandQueueInfo(m_queue, CL_QUEUE_DEVICE, sizeof(cl_device_id), &dev, nullptr);

	if(errorCode == CL_SUCCESS)
	{
		out = std::move(Device(dev));
	}

	return errorCode;
}


inline cl_int CLcpp::CommandQueue::GetReferenceCount(cl_uint& out) const
{
	return clGetCommandQueueInfo(m_queue, CL_QUEUE_REFERENCE_COUNT, sizeof(cl_uint), &out, nullptr);
}


inline cl_int CLcpp::CommandQueue::GetProperties(cl_command_queue_properties& out) const
{
	return clGetCommandQueueInfo(m_queue, CL_QUEUE_PROPERTIES, sizeof(cl_command_queue_properties), &out, nullptr);
}


inline cl_int CLcpp::CommandQueue::Flush() const
{
	return clFlush(m_queue);
}


inline cl_int CLcpp::CommandQueue::Finish() const
{
	return clFinish(m_queue);
}


inline cl_int CLcpp::CommandQueue::EnqueueNDKernel(
	Event& outEvent,
	const std::vector<Event>& waitListEventVector,
	const Kernel& kernel,
	const cl_uint numWorkDimensions,
	const size_t* globalWorkOffset,
	const size_t* globalWorkSize,
	const size_t* localWorkSize
) const
{
	const cl_uint numWaitListEvents = cl_uint(waitListEventVector.size());
	const cl_event* const waitList = (numWaitListEvents > 0) ? (const cl_event*) &waitListEventVector[0] : nullptr;

	cl_event evt;

	const cl_int errorCode = clEnqueueNDRangeKernel(
		m_queue,
		kernel,
		numWorkDimensions,
		globalWorkOffset,
		globalWorkSize,
		localWorkSize,
		numWaitListEvents,
		waitList,
		&evt
	);

	if(errorCode == CL_SUCCESS)
	{
		outEvent = std::move(Event(evt));
	}

	return errorCode;
}


inline cl_int CLcpp::CommandQueue::EnqueueCopyBuffer(
	Event& outEvent,
	const std::vector<Event>& waitListEventVector,
	const MemBuffer& destBuffer,
	const MemBuffer& srcBuffer,
	const size_t destOffset,
	const size_t srcOffset,
	const size_t copySize
)
{
	const cl_uint numWaitListEvents = cl_uint(waitListEventVector.size());
	const cl_event* const waitListEventArray = (numWaitListEvents > 0) ? (const cl_event*) &waitListEventVector[0] : nullptr;

	cl_event evt;

	const cl_int errorCode = clEnqueueCopyBuffer(
		m_queue,
		srcBuffer,
		destBuffer,
		srcOffset,
		destOffset,
		copySize,
		numWaitListEvents,
		waitListEventArray,
		&evt
	);

	if(errorCode == CL_SUCCESS)
	{
		outEvent = std::move(Event(evt));
	}

	return errorCode;
}


inline cl_int CLcpp::CommandQueue::EnqueueWriteBuffer(
	Event& outEvent,
	const std::vector<Event>& waitListEventVector,
	const MemBuffer& memBuffer,
	const bool isBlocking,
	const size_t destOffset,
	const size_t srcSize,
	const void* const srcBuffer
) const
{
	const cl_uint numWaitListEvents = cl_uint(waitListEventVector.size());
	const cl_event* const pWaitList = (numWaitListEvents > 0) ? (const cl_event*) &waitListEventVector[0] : nullptr;

	cl_event evt;
	cl_int errorCode = clEnqueueWriteBuffer(
		m_queue,
		memBuffer,
		(isBlocking) ? CL_TRUE : CL_FALSE,
		destOffset,
		srcSize,
		srcBuffer,
		numWaitListEvents,
		pWaitList,
		&evt
	);

	if(errorCode == CL_SUCCESS)
	{
		outEvent = std::move(Event(evt));
	}

	return errorCode;
}


inline cl_int CLcpp::CommandQueue::EnqueueWriteImage(
	Event& outEvent,
	const std::vector<Event>& waitListEventVector,
	const MemBuffer& memBuffer,
	const bool isBlocking,
	const size_t origin[3],
	const size_t regionSize[3],
	const size_t rowPitch,
	const size_t slicePitch,
	const void* const srcImageData
) const
{
	const cl_uint numWaitListEvents = cl_uint(waitListEventVector.size());
	const cl_event* const pWaitList = (numWaitListEvents > 0) ? (const cl_event*) &waitListEventVector[0] : nullptr;

	cl_event evt;
	cl_int errorCode = clEnqueueWriteImage(
		m_queue,
		memBuffer,
		(isBlocking) ? CL_TRUE : CL_FALSE,
		origin,
		regionSize,
		rowPitch,
		slicePitch,
		srcImageData,
		numWaitListEvents,
		pWaitList,
		&evt
	);

	if(errorCode == CL_SUCCESS)
	{
		outEvent = std::move(Event(evt));
	}

	return errorCode;
}


inline cl_int CLcpp::CommandQueue::EnqueueReadBuffer(
	Event& outEvent,
	const std::vector<Event>& waitListEventVector,
	const MemBuffer& memBuffer,
	const bool isBlocking,
	const size_t srcOffset,
	const size_t destSize,
	void* const destBuffer
) const
{
	const cl_uint numWaitListEvents = cl_uint(waitListEventVector.size());
	const cl_event* const pWaitList = (numWaitListEvents > 0) ? (const cl_event*) &waitListEventVector[0] : nullptr;

	cl_event evt;
	cl_int errorCode = clEnqueueReadBuffer(
		m_queue,
		memBuffer,
		(isBlocking) ? CL_TRUE : CL_FALSE,
		srcOffset,
		destSize,
		destBuffer,
		numWaitListEvents,
		pWaitList,
		&evt
	);

	if(errorCode == CL_SUCCESS)
	{
		outEvent = std::move(Event(evt));
	}

	return errorCode;
}


inline cl_int CLcpp::CommandQueue::EnqueueReadImage(
	Event& outEvent,
	const std::vector<Event>& waitListEventVector,
	const MemBuffer& memBuffer,
	const bool isBlocking,
	const size_t origin[3],
	const size_t regionSize[3],
	const size_t rowPitch,
	const size_t slicePitch,
	void* const destImageData
) const
{
	const cl_uint numWaitListEvents = cl_uint(waitListEventVector.size());
	const cl_event* const pWaitList = (numWaitListEvents > 0) ? (const cl_event*) &waitListEventVector[0] : nullptr;

	cl_event evt;
	cl_int errorCode = clEnqueueReadImage(
		m_queue,
		memBuffer,
		(isBlocking) ? CL_TRUE : CL_FALSE,
		origin,
		regionSize,
		rowPitch,
		slicePitch,
		destImageData,
		numWaitListEvents,
		pWaitList,
		&evt
	);

	if(errorCode == CL_SUCCESS)
	{
		outEvent = std::move(Event(evt));
	}

	return errorCode;
}


inline cl_int CLcpp::CommandQueue::EnqueueMapBuffer(
	void** const outMappedRegion,
	Event& outEvent,
	const std::vector<Event>& waitListEventVector,
	const MemBuffer& memBuffer,
	const bool isBlocking,
	const cl_mem_flags mapFlags,
	const size_t bufferOffset,
	const size_t regionSize
) const
{
	const cl_uint numWaitListEvents = cl_uint(waitListEventVector.size());
	const cl_event* const pWaitList = (numWaitListEvents > 0) ? (const cl_event*) &waitListEventVector[0] : nullptr;

	cl_event evt;
	cl_int errorCode;

	void* const pRegion = clEnqueueMapBuffer(
		m_queue,
		memBuffer,
		(isBlocking) ? CL_TRUE : CL_FALSE,
		mapFlags,
		bufferOffset,
		regionSize,
		numWaitListEvents,
		pWaitList,
		&evt,
		&errorCode
	);

	if(errorCode == CL_SUCCESS)
	{
		(*outMappedRegion) = pRegion;
		outEvent = std::move(Event(evt));
	}

	return errorCode;
}


inline cl_int CLcpp::CommandQueue::EnqueueMapImage(
	void** const outMappedRegion,
	size_t& outRowPitch,
	size_t& outSlicePitch,
	Event& outEvent,
	const std::vector<Event>& waitListEventVector,
	const MemBuffer& memBuffer,
	const bool isBlocking,
	const cl_mem_flags mapFlags,
	const size_t origin[3],
	const size_t regionSize[3]
) const
{
	const cl_uint numWaitListEvents = cl_uint(waitListEventVector.size());
	const cl_event* const pWaitList = (numWaitListEvents > 0) ? (const cl_event*) &waitListEventVector[0] : nullptr;

	cl_event evt;
	size_t rowPitch;
	size_t slicePitch;
	cl_int errorCode;

	void* const pRegion = clEnqueueMapImage(
		m_queue,
		memBuffer,
		(isBlocking) ? CL_TRUE : CL_FALSE,
		mapFlags,
		origin,
		regionSize,
		&rowPitch,
		&slicePitch,
		numWaitListEvents,
		pWaitList,
		&evt,
		&errorCode
	);

	if(errorCode == CL_SUCCESS)
	{
		(*outMappedRegion) = pRegion;
		outRowPitch = rowPitch;
		outSlicePitch = slicePitch;
		outEvent = std::move(Event(evt));
	}

	return errorCode;
}


inline cl_int CLcpp::CommandQueue::EnqueueUnmapMemObject(
	Event& outEvent,
	const std::vector<Event>& waitListEventVector,
	const MemBuffer& memBuffer,
	void* const mappedRegion
) const
{
	const cl_uint numWaitListEvents = cl_uint(waitListEventVector.size());
	const cl_event* const pWaitList = (numWaitListEvents > 0) ? (const cl_event*) &waitListEventVector[0] : nullptr;

	cl_event evt;
	cl_int errorCode = clEnqueueUnmapMemObject(
		m_queue,
		memBuffer,
		mappedRegion,
		numWaitListEvents,
		pWaitList,
		&evt
	);

	if(errorCode == CL_SUCCESS)
	{
		outEvent = std::move(Event(evt));
	}

	return errorCode;
}


#if CL_CPP_ENABLE_OPENCL_1_1
	#if CL_CPP_ENABLE_DEPRECATED_1_1_APIS
		inline cl_int CLcpp::CommandQueue::EnqueueBarrier() const
		{
			return clEnqueueBarrier(m_queue);
		}


		inline cl_int CLcpp::CommandQueue::EnqueueWaitForEvents(const std::vector<Event>& waitListEventVector) const
		{
			const cl_uint numWaitListEvents = cl_uint(waitListEventVector.size());
			const cl_event* const waitListEventArray = (numWaitListEvents > 0) ? (const cl_event*) &waitListEventVector[0] : nullptr;

			return clEnqueueWaitForEvents(m_queue, numWaitListEvents, waitListEventArray);
		}


		inline cl_int CLcpp::CommandQueue::EnqueueMarker(Event& outEvent) const
		{
			cl_event evt;

			const cl_int errorCode = clEnqueueMarker(m_queue, &evt);

			if(errorCode == CL_SUCCESS)
			{
				outEvent = std::move(Event(evt));
			}

			return errorCode;
		}
	#endif
#endif


#if CL_CPP_ENABLE_OPENCL_1_2
	inline cl_int CLcpp::CommandQueue::EnqueueBarrierWithWaitList(Event& outEvent, const std::vector<Event>& waitListEventVector) const
	{
		const cl_uint numWaitListEvents = cl_uint(waitListEventVector.size());
		const cl_event* const waitListEventArray = (numWaitListEvents > 0) ? (const cl_event*) &waitListEventVector[0] : nullptr;

		cl_event evt;

		const cl_int errorCode = clEnqueueBarrierWithWaitList(m_queue, numWaitListEvents, waitListEventArray, &evt);

		if(errorCode == CL_SUCCESS)
		{
			outEvent = std::move(Event(evt));
		}

		return errorCode;
	}


	inline cl_int CLcpp::CommandQueue::EnqueueMarkerWithWaitList(Event& outEvent, const std::vector<Event>& waitListEventVector) const
	{
		const cl_uint numWaitListEvents = cl_uint(waitListEventVector.size());
		const cl_event* const waitListEventArray = (numWaitListEvents > 0) ? (const cl_event*) &waitListEventVector[0] : nullptr;

		cl_event evt;

		const cl_int errorCode = clEnqueueMarkerWithWaitList(m_queue, numWaitListEvents, waitListEventArray, &evt);

		if(errorCode == CL_SUCCESS)
		{
			outEvent = std::move(Event(evt));
		}

		return errorCode;
	}


	inline cl_int CLcpp::CommandQueue::EnqueueMigrateMemObjects(
		Event& outEvent,
		const std::vector<Event>& waitListEventVector,
		const std::vector<MemBuffer>& memBufferVector,
		const cl_mem_migration_flags migrationFlags
	) const
	{
		const cl_uint numWaitListEvents = cl_uint(waitListEventVector.size());
		const cl_event* const waitListEventArray = (numWaitListEvents > 0) ? (const cl_event*) &waitListEventVector[0] : nullptr;

		const cl_uint numMemObjects = cl_uint(memBufferVector.size());
		const cl_mem* const memObjectArray = (numMemObjects > 0) ? (const cl_mem*) &memBufferVector[0] : nullptr;

		cl_event evt;

		const cl_int errorCode = clEnqueueMigrateMemObjects(
			m_queue,
			numMemObjects,
			memObjectArray,
			migrationFlags,
			numWaitListEvents,
			waitListEventArray,
			&evt
		);

		if(errorCode == CL_SUCCESS)
		{
			outEvent = std::move(Event(evt));
		}

		return errorCode;
	}


	inline cl_int CLcpp::CommandQueue::EnqueueFillBuffer(
		Event& outEvent,
		const std::vector<Event>& waitListEventVector,
		const MemBuffer& memBuffer,
		const void* const patternData,
		const size_t patternSize,
		const size_t regionOffset,
		const size_t regionSize
	) const
	{
		const cl_uint numWaitListEvents = cl_uint(waitListEventVector.size());
		const cl_event* const waitListEventArray = (numWaitListEvents > 0) ? (const cl_event*) &waitListEventVector[0] : nullptr;

		cl_event evt;

		const cl_int errorCode = clEnqueueFillBuffer(
			m_queue,
			memBuffer,
			patternData,
			patternSize,
			regionOffset,
			regionSize,
			numWaitListEvents,
			waitListEventArray,
			&evt
		);

		if(errorCode == CL_SUCCESS)
		{
			outEvent = std::move(Event(evt));
		}

		return errorCode;
	}
#endif


#if CL_CPP_ENABLE_OPENCL_2_0
	inline cl_int CLcpp::CommandQueue::GetSize(cl_uint& out) const
	{
		return clGetCommandQueueInfo(m_queue, CL_QUEUE_SIZE, sizeof(cl_uint), &out, nullptr);
	}
#endif
