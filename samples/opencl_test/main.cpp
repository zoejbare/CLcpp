/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#define USE_WRAPPER
#define VERBOSE_OUTPUT

#ifdef _WIN32
#	define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include <CLcpp.h>

#ifdef VERBOSE_OUTPUT
#	define OCL_CHECK_ERROR(expr) \
	{ \
		const cl_int errorCode = (expr); \
		if(errorCode != CL_SUCCESS) \
		{ \
			printf("<< Error Code (%d) >> " #expr "\n", errorCode); \
		}\
		else \
		{ \
			printf("[[success]]  " #expr "\n"); \
		} \
	}
#else
#	define OCL_CHECK_ERROR(expr) expr
#endif

#define TOTAL_NUM_ELEMENTS 10000
#define MEM_SIZE_IN_BYTES  (TOTAL_NUM_ELEMENTS * sizeof(cl_float))

// Begin entry point.
int main(int argc, char* argv[])
{
	(void)(argc);
	(void)(argv);

	auto loadSourceBuffer = [](const char* const filePath) -> std::string
	{
		if(!filePath)
			return std::string("");

		// Open the file.
		FILE* const fileHandle = fopen(filePath, "rb");

		if(!fileHandle)
		{
			// Return an empty string if the file doesn't exist.
			return std::string("");
		}

		// Go to the end of the file.
		fseek(fileHandle, 0, SEEK_END);

		// Get the size of the file and allocate space for the entire file.
		const size_t fileSize = (size_t) ftell(fileHandle);
		char* const sourceBuffer = new char[fileSize];

		// Return to the start of the file.
		fseek(fileHandle, 0, SEEK_SET);

		// Read the file contents into the temporary buffer, then close the file.
		const size_t numBytesRead = fread(sourceBuffer, 1, fileSize, fileHandle);
		fclose(fileHandle);

		// Copy the temporary buffer into the output string, then delete the buffer.
		std::string outString(sourceBuffer, numBytesRead);
		delete[] sourceBuffer;

		return outString;
	};

	auto saveInfoToFile = [](const char* const filePath, const std::string& infoString) -> bool
	{
		if(!filePath || infoString.size() == 0)
		{
			return false;
		}

		FILE* const fileHandle = fopen(filePath, "wb");

		if(!fileHandle)
		{
			return false;
		}

		// Write the info string to the file, then close it.
		fwrite(infoString.c_str(), 1, infoString.size(), fileHandle);
		fclose(fileHandle);

		return true;
	};

	auto roundUpByGroupSize = [](const size_t globalSize, const size_t groupSize) -> size_t
	{
		const size_t r = globalSize % groupSize;

		if(r == 0)
		{
			return globalSize;
		}

		return (globalSize + groupSize - r);
	};


	cl_float array1[TOTAL_NUM_ELEMENTS];
	cl_float array2[TOTAL_NUM_ELEMENTS];
	cl_float resultArray[TOTAL_NUM_ELEMENTS];

	// Initial array setup.
	for(cl_int i = 0; i < TOTAL_NUM_ELEMENTS; ++i)
	{
		array1[i] = (cl_float) i;
		array2[i] = (cl_float) i * 1.5f;
	}

	const size_t localWorkSize = 64;
	const size_t globalWorkSize = roundUpByGroupSize(TOTAL_NUM_ELEMENTS, localWorkSize);

	const size_t localWorkSizeArray[3] = { localWorkSize, localWorkSize, localWorkSize };
	const size_t globalWorkSizeArray[3] = { globalWorkSize, globalWorkSize, globalWorkSize };
	const size_t globalWorkOffsetArray[3] = { 0, 0, 0 };

	std::string fileBuffer = loadSourceBuffer("../../../../samples/opencl_test/programs/array.cl");

#if !defined(USE_WRAPPER)

	cl_int errorCode = 0;

	cl_platform_id plat;
	cl_device_id dev;
	cl_context ctx;
	cl_command_queue cmdQueue;
	cl_program prg;
	cl_kernel krnl;

	cl_uint numDevices = 0;

	errorCode = clGetPlatformIDs(1, &plat, nullptr);
	errorCode = clGetDeviceIDs(plat, CL_DEVICE_TYPE_GPU, 0, nullptr, &numDevices);
	errorCode = clGetDeviceIDs(plat, CL_DEVICE_TYPE_GPU, 1, &dev, nullptr);

	ctx = clCreateContext(nullptr, 1, &dev, nullptr, nullptr, &errorCode);
	cmdQueue = clCreateCommandQueue(ctx, dev, 0, &errorCode);

	cl_mem buffer1 = clCreateBuffer(ctx, CL_MEM_READ_ONLY, MEM_SIZE_IN_BYTES, nullptr, &errorCode);
	cl_mem buffer2 = clCreateBuffer(ctx, CL_MEM_READ_ONLY, MEM_SIZE_IN_BYTES, nullptr, &errorCode);
	cl_mem resultBuffer = clCreateBuffer(ctx, CL_MEM_WRITE_ONLY, MEM_SIZE_IN_BYTES, nullptr, &errorCode);

	errorCode = clEnqueueWriteBuffer(cmdQueue, buffer1, CL_TRUE, 0, MEM_SIZE_IN_BYTES, array1, 0, nullptr, nullptr);
	errorCode = clEnqueueWriteBuffer(cmdQueue, buffer2, CL_TRUE, 0, MEM_SIZE_IN_BYTES, array2, 0, nullptr, nullptr);

	size_t logLength = 0;

	const char* const fileBufferPtr = fileBuffer.c_str();
	const size_t fileBufferSize = fileBuffer.size();

	prg = clCreateProgramWithSource(ctx, 1, (const char**) &fileBufferPtr, (const size_t*) &fileBufferSize, &errorCode);
	errorCode = clBuildProgram(prg, 1, &dev, nullptr, nullptr, nullptr);
	errorCode = clGetProgramBuildInfo(prg, dev, CL_PROGRAM_BUILD_LOG, 0, nullptr, &logLength);

	char* const logBuffer = new char[logLength];

	errorCode = clGetProgramBuildInfo(prg, dev, CL_PROGRAM_BUILD_LOG, logLength, logBuffer, nullptr);

	printf("------ Build Log ------\n%s\n", logBuffer);

	size_t* binaryLengthArray = new size_t[numDevices];
	cl_uchar** binaryDataArray = new cl_uchar*[numDevices];


	errorCode = clGetProgramInfo(prg, CL_PROGRAM_BINARY_SIZES, sizeof(size_t) * numDevices, binaryLengthArray, nullptr);

	size_t totalBinarySize = 0;

	for(cl_uint i = 0; i < numDevices; ++i)
	{
		const size_t binarySize = binaryLengthArray[i];

		binaryDataArray[i] = new cl_uchar[binarySize];
		totalBinarySize += binarySize;
	}

	errorCode = clGetProgramInfo(prg, CL_PROGRAM_BINARIES, sizeof(cl_uchar*) * numDevices, (void*) binaryDataArray, nullptr);

	for(cl_uint i = 0; i < numDevices; ++i)
	{
		char filename[1024];

		std::string binaryString((char*) binaryDataArray[i], binaryLengthArray[i]);

		sprintf(filename, "bin_%02d.txt", i);
		saveInfoToFile(filename, binaryString);

		delete[] binaryDataArray[i];
	}

	delete[] binaryDataArray;
	delete[] binaryLengthArray;

	const cl_uint totalNumElements = TOTAL_NUM_ELEMENTS;

	krnl = clCreateKernel(prg, "ArrayAdd", &errorCode);
	errorCode = clSetKernelArg(krnl, 0, sizeof(cl_mem), (void*) &buffer1);
	errorCode = clSetKernelArg(krnl, 1, sizeof(cl_mem), (void*) &buffer2);
	errorCode = clSetKernelArg(krnl, 2, sizeof(cl_mem), (void*) &resultBuffer);
	errorCode = clSetKernelArg(krnl, 3, sizeof(cl_uint), &totalNumElements);

	errorCode = clEnqueueNDRangeKernel(cmdQueue, krnl, 1, nullptr, &globalWorkSize, &localWorkSize, 0, nullptr, nullptr);

	clFinish(cmdQueue);
	errorCode = clEnqueueReadBuffer(cmdQueue, resultBuffer, CL_TRUE, 0, MEM_SIZE_IN_BYTES, resultArray, 0, nullptr, nullptr);

	clReleaseKernel(krnl);
	clReleaseProgram(prg);

	clReleaseMemObject(buffer1);
	clReleaseMemObject(buffer2);
	clReleaseMemObject(resultBuffer);

	clReleaseCommandQueue(cmdQueue);
	clReleaseContext(ctx);

#else

	using namespace CLcpp;

	auto customAlloc = [](size_t sizeInBytes) -> void*
	{
		return malloc(sizeInBytes);
	};

	auto customFree = [](void* const data)
	{
		free(data);
	};

	auto displayPlatformInfo = [](const std::vector<Platform>& platformVector)
	{
		printf("\n");

		for(const Platform& platform : platformVector)
		{
			std::string name, profile, vendor, version, extensions;

			platform.GetName(name);
			platform.GetProfile(profile);
			platform.GetVendor(vendor);
			platform.GetVersion(version);
			platform.GetExtensions(extensions);

			printf(
				"---- Platform ----\n"
				"\tName: %s\n"
				"\tProfile: %s\n"
				"\tVendor: %s\n"
				"\tVersion: %s\n"
				"\tExtensions: %s\n\n",
				name.c_str(),
				profile.c_str(),
				vendor.c_str(),
				version.c_str(),
				extensions.c_str()
			);
		}
	};

	auto displayDeviceInfo = [](const std::vector<Device>& deviceVector)
	{
		printf("\n");

		for(const Device& device : deviceVector)
		{
			std::string name, vendor, profile, apiVersion, driverVersion, extensions;
			cl_device_type deviceType;

			device.GetName(name);
			device.GetVendor(vendor);
			device.GetProfile(profile);
			device.GetApiVersion(apiVersion);
			device.GetDriverVersion(driverVersion);
			device.GetExtensions(extensions);
			device.GetDeviceType(deviceType);

			printf("---- Device ----\n");
			printf("\tName: %s\n", name.c_str());
			printf("\tVendor: %s\n", vendor.c_str());
			printf("\tProfile: %s\n", profile.c_str());
			printf("\tAPI Version: %s\n", apiVersion.c_str());
			printf("\tDriver Version: %s\n", driverVersion.c_str());
			printf("\tExtensions: %s\n", extensions.c_str());
			printf("\tDeviceType: 0x%02X\n", cl_uint(deviceType));
			printf("\n");
		}
	};

	printf("CLcpp version: %s\n", CL_CPP_VERSION_STRING);

	Util::SetMemoryCallbacks(customAlloc, customFree);

	std::vector<std::string> sourceVector;
	sourceVector.push_back(fileBuffer);

	std::vector<Platform> platformVector;
	std::vector<Device> deviceVector;

	Platform platform;
	Device device;
	Context context;
	CommandQueue commandQueue;
	Program program;
	Kernel kernel;
	MemBuffer buffer1;
	MemBuffer buffer2;
	MemBuffer resultBuffer;
	Event event1;
	Event event2;

	OCL_CHECK_ERROR(Platform::Load(platformVector));
	platform = platformVector[0];

	for(const Platform& platform_ : platformVector)
	{
		std::vector<Device> tempDeviceVector;

		Device::Load(tempDeviceVector, platform_, CL_DEVICE_TYPE_ALL);
		deviceVector.reserve(deviceVector.size() + tempDeviceVector.size());

		for(const Device& device_ : tempDeviceVector)
		{
			deviceVector.push_back(std::move(device_));
		}
	}

	displayPlatformInfo(platformVector);
	displayDeviceInfo(deviceVector);

	OCL_CHECK_ERROR(Device::Load(deviceVector, platform, CL_DEVICE_TYPE_ALL));
	device = deviceVector[0];

	OCL_CHECK_ERROR(Context::Create(context, deviceVector, nullptr, nullptr, nullptr));
	OCL_CHECK_ERROR(CommandQueue::Create(commandQueue, context, device, 0, 0));
	OCL_CHECK_ERROR(Program::CreateFromSources(program, context, sourceVector));
	OCL_CHECK_ERROR(program.Build(deviceVector, "-cl-fast-relaxed-math", nullptr, nullptr));

	std::vector<std::string> programBinaryDataVector;
	OCL_CHECK_ERROR(program.GetBinaries(programBinaryDataVector));

	for(int i = 0; i < int(deviceVector.size()); ++i)
	{
		char filename[1024];

		sprintf(filename, "bin_%02d.txt", i);
		saveInfoToFile(filename, programBinaryDataVector[i]);
	}

	OCL_CHECK_ERROR(Kernel::Create(kernel, program, "ArrayAdd"));
	OCL_CHECK_ERROR(MemBuffer::CreateAsBuffer(buffer1, context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, MEM_SIZE_IN_BYTES, array1));
	OCL_CHECK_ERROR(MemBuffer::CreateAsBuffer(buffer2, context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, MEM_SIZE_IN_BYTES, array2));
	OCL_CHECK_ERROR(MemBuffer::CreateAsBuffer(resultBuffer, context, CL_MEM_WRITE_ONLY, MEM_SIZE_IN_BYTES, nullptr));

	OCL_CHECK_ERROR(kernel.SetArgument(0, buffer1));
	OCL_CHECK_ERROR(kernel.SetArgument(1, buffer2));
	OCL_CHECK_ERROR(kernel.SetArgument(2, resultBuffer));
	OCL_CHECK_ERROR(kernel.SetArgument(3, cl_uint(TOTAL_NUM_ELEMENTS)));
	OCL_CHECK_ERROR(commandQueue.EnqueueNDKernel(event1, std::vector<CLcpp::Event>(), kernel, 1, globalWorkOffsetArray, globalWorkSizeArray, localWorkSizeArray));
	OCL_CHECK_ERROR(commandQueue.EnqueueReadBuffer(event2, std::vector<Event>(), resultBuffer, false, 0, MEM_SIZE_IN_BYTES, resultArray));
	OCL_CHECK_ERROR(commandQueue.Finish());
#endif

	return 0;
}
