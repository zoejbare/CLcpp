/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

//	Add the elements of 2 arrays.
__kernel void ArrayAdd(	__global const float* a1,
						__global const float* a2,
						__global float* a_out,
						const int uNumElements)
{
	//	element index
	const int i = get_global_id(0);

	if(i < uNumElements)
		a_out[i] = a1[i] + a2[i];
}


//	Subtract the elements of 2 arrays.
__kernel void ArraySub(	__global const float* a1,
						__global const float* a2,
						__global float* a_out,
						const int uNumElements)
{
	//	element index
	const int i = get_global_id(0);

	if(i < uNumElements)
		a_out[i] = a1[i] - a2[i];
}