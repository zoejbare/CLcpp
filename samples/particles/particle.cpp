/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#include "particle.h"
#include "metrics.h"
#include "utility.h"


template<>
ParticleSystem* ISingleton<ParticleSystem>::ms_pSingleton = nullptr;


ParticleSystem::ParticleSystem()
	: m_particleProcessor(nullptr)
	, m_minParticleSize(1.0f)
	, m_maxParticleSize(1.0f)
	, m_minParticleLife(3.0f)
	, m_maxParticleLife(3.0f)
	, m_gravityStrength(9.81f)
	, m_numParticles(0)
{
	m_emitterPosition.x = 0.0f;
	m_emitterPosition.y = 0.0f;
	m_emitterPosition.z = 0.0f;

	m_gravityCenter.x = 0.0f;
	m_gravityCenter.y = -100000.0f;
	m_gravityCenter.z = 0.0f;

	// Initialize the indices up front since they never change.
	for(cl_int i = 0; i < MAX_PARTICLES; ++i)
	{
		const cl_uint baseIndex = cl_uint(i * 6);
		const cl_uint vertIndex = cl_uint(i * 4);

		m_particleIndex[baseIndex + 0] = vertIndex + 0;
		m_particleIndex[baseIndex + 1] = vertIndex + 2;
		m_particleIndex[baseIndex + 2] = vertIndex + 1;
		m_particleIndex[baseIndex + 3] = vertIndex + 1;
		m_particleIndex[baseIndex + 4] = vertIndex + 2;
		m_particleIndex[baseIndex + 5] = vertIndex + 3;
	}
}


void ParticleSystem::Reset()
{
	ParticleData zeroData =
	{
		{ 0.0f, 0.0f, 0.0f },
		{ 0.0f, 0.0f, 0.0f },
		{ 0.0f, 0.0f, 0.0f },
		0.0f,
		0.0f,
		0.0f,
		0.0f,
		0.0f
	};

	ParticleVertex zeroVertex =
	{
		{ 0.0f, 0.0f, 0.0f },
		{ 0.0f, 0.0f },
		{ 1.0f, 1.0f, 1.0f, 1.0f }
	};

	const Vector4 texCoord_UpperLeft = { 0.0f, 1.0f };
	const Vector4 texCoord_UpperRight = { 1.0f, 1.0f };
	const Vector4 texCoord_LowerLeft = { 0.0f, 0.0f };
	const Vector4 texCoord_LowerRight = { 1.0f, 0.0f };

	for(cl_int i = 0; i < MAX_PARTICLES; ++i)
	{
		const cl_int baseIndex = (i * 4);

		// The blue color value will be determined by the particle processors.
		const cl_float colorR = Utility::RandomNumberF(0.05f, 0.8f);
		const cl_float colorG = Utility::RandomNumberF(0.05f, 0.8f);

		// Calculate an initial velocity direction for the current particle.
		Vector4 initialVelocity =
		{
			Utility::RandomNumberF(-0.35f, 0.35f),
			1.0f,
			Utility::RandomNumberF(-0.35f, 0.35f),
		};

		// Calculate the a random magnitude for the initial velocity.
		initialVelocity.Normalize();
		initialVelocity *= Utility::RandomNumberF(50.0f, 80.0f);

		zeroData.velocity = initialVelocity;
		zeroData.initialVelocity = initialVelocity;

		zeroData.size = Utility::RandomNumberF(m_minParticleSize, m_maxParticleSize);
		zeroData.startingLife = Utility::RandomNumberF(m_minParticleLife, m_maxParticleLife);
		zeroData.currentLife = zeroData.startingLife;

		//	Calculate a random rotation and rotational velocity.
		zeroData.rotation = Utility::RandomNumberF(0.0f, MATH_2_PI);
		zeroData.rotationVelocity = Utility::RandomNumberF(-MATH_PI, MATH_PI);

		zeroVertex.color.x = colorR;
		zeroVertex.color.y = colorG;

		m_particleData[i] = zeroData;

		m_particleVertex[baseIndex + 0] = zeroVertex;
		m_particleVertex[baseIndex + 1] = zeroVertex;
		m_particleVertex[baseIndex + 2] = zeroVertex;
		m_particleVertex[baseIndex + 3] = zeroVertex;

		m_particleVertex[baseIndex + 0].texCoord = texCoord_UpperLeft;
		m_particleVertex[baseIndex + 1].texCoord = texCoord_UpperRight;
		m_particleVertex[baseIndex + 2].texCoord = texCoord_LowerLeft;
		m_particleVertex[baseIndex + 3].texCoord = texCoord_LowerRight;
	}

	m_numParticles = 0;
}


void ParticleSystem::Update()
{
	if(!m_particleProcessor)
		return;

	Metrics* const metrics = Metrics::Get();

	metrics->StartProcessingTimer();
	m_particleProcessor->DoWork(m_particleData, m_particleVertex, m_numParticles);
	metrics->StopProcessingTimer();
}


void ParticleSystem::Draw()
{
	if(m_numParticles > 0)
	{
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		glVertexPointer(3, GL_FLOAT, sizeof(ParticleVertex), (const GLvoid*) &m_particleVertex->position);
		glTexCoordPointer(2, GL_FLOAT, sizeof(ParticleVertex), (const GLvoid*) &m_particleVertex->texCoord);
		glColorPointer(4, GL_FLOAT, sizeof(ParticleVertex), (const GLvoid*) &m_particleVertex->color);

		glDrawElements(GL_TRIANGLES, m_numParticles * 6, GL_UNSIGNED_INT, m_particleIndex);

		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
	}
}
