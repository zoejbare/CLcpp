/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#include "particle_processor_cl.h"
#include "particle.h"
#include "camera.h"
#include "metrics.h"
#include "utility.h"


template<>
ParticleProcessorOpenCL* ISingleton<ParticleProcessorOpenCL>::ms_pSingleton = nullptr;


struct FrameConstant
{
	Matrix cameraBasis;

	Vector4 gravityCenter;
	Vector4 emitterPosition;

	cl_float gravityStrength;
	cl_float timeDelta;

	cl_uint numParticles;

	// Need padding to the next multiple of 16 bytes.
	cl_float padding[1];
};


bool ParticleProcessorOpenCL::SetupOpenCL()
{
	using namespace CLcpp;

	// Set the CLcpp memory callbacks.
	Util::SetMemoryCallbacks(Utility::CustomAlloc, Utility::CustomFree);

	const char* const programFilePath = "../../../../samples/particles/programs/particle_processing.cl";
	std::string programFileBuffer = Utility::LoadSourceBuffer(programFilePath);

	//	Try to open and read the OpenCL program file.
	if(programFileBuffer.size() == 0)
	{
		fprintf(stderr, "ERROR: Unable to open OpenCL program file [%s]!", programFileBuffer.c_str());
		return false;
	}

	std::vector<Platform> platformVector;
	std::vector<Device> deviceVector;

	// Load the available platforms.
	Platform::Load(platformVector);
	if(platformVector.size() == 0)
	{
		fprintf(stderr, "ERROR: No OpenCL platforms found!\n");
		return false;
	}

	// Default to the first platform.
	m_platform = platformVector[0];

	// Load the available devices from the selected platform.
	Device::Load(deviceVector, m_platform, CL_DEVICE_TYPE_ALL);
	if(deviceVector.size() == 0)
	{
		fprintf(stderr, "ERROR: No OpenCL devices found!\n");
		return false;
	}

	// Default to the first device.
	m_device = deviceVector[0];
	bool status = true;

	const size_t dataBufferMaxSize = sizeof(ParticleData) * MAX_PARTICLES;
	const size_t vertexBufferMaxSize = sizeof(ParticleVertex) * MAX_PARTICLES * 4;
	const size_t constantBufferMaxSize = sizeof(FrameConstant);

	std::vector<std::string> programSourceVector;
	programSourceVector.push_back(programFileBuffer);

	// Initialize our CLpp objects and load the OpenCL kernel.
	OCL_CHECK_ERROR_WITH_STATUS(status, Context::Create(m_context, deviceVector, nullptr, nullptr, nullptr));
	OCL_CHECK_ERROR_WITH_STATUS(status, CommandQueue::Create(m_commandQueue, m_context, m_device, 0, 0));

	OCL_CHECK_ERROR_WITH_STATUS(status, Program::CreateFromSources(m_program, m_context, programSourceVector));
	OCL_CHECK_ERROR_WITH_STATUS(status, m_program.Build(deviceVector, "-cl-fast-relaxed-math", nullptr, nullptr));
	OCL_CHECK_ERROR_WITH_STATUS(status, Kernel::Create(m_kernel, m_program, "Process"));

	auto displayProgramBuildLog = [](const CLcpp::Program& program)
	{
		std::vector<Device> deviceVector;
		program.GetDevices(deviceVector);

		cl_build_status buildStatus;
		std::string buildLog;
		std::string deviceName;

		for(const Device& device : deviceVector)
		{
			program.GetBuildStatus(buildStatus, device);
			if(buildStatus != CL_BUILD_SUCCESS)
			{
				device.GetName(deviceName);
				program.GetBuildLog(buildLog, device);

				printf("\n---- Program Build Log (device %s) ----\n%s\n\n", deviceName.c_str(), buildLog.c_str());
			}
		}
	};

	// Output the program build status.
	displayProgramBuildLog(m_program);

	OCL_CHECK_ERROR_WITH_STATUS(status, MemBuffer::CreateAsBuffer(m_particleDataBuffer, m_context, CL_MEM_READ_WRITE, dataBufferMaxSize, nullptr));
	OCL_CHECK_ERROR_WITH_STATUS(status, MemBuffer::CreateAsBuffer(m_particleVertexBuffer, m_context, CL_MEM_WRITE_ONLY, vertexBufferMaxSize, nullptr));
	OCL_CHECK_ERROR_WITH_STATUS(status, MemBuffer::CreateAsBuffer(m_constantBuffer, m_context, CL_MEM_READ_ONLY, constantBufferMaxSize, nullptr));

	// Set the kernel arguments.
	OCL_CHECK_ERROR_WITH_STATUS(status, m_kernel.SetArgument(0, m_particleDataBuffer));
	OCL_CHECK_ERROR_WITH_STATUS(status, m_kernel.SetArgument(1, m_particleVertexBuffer));
	OCL_CHECK_ERROR_WITH_STATUS(status, m_kernel.SetArgument(2, m_constantBuffer));

	return status;
}


const char* ParticleProcessorOpenCL::GetName() const
{
	return "OpenCL Particle Processor";
}


void ParticleProcessorOpenCL::DoWork(ParticleData* const particleDataArray, ParticleVertex* const particleVertexArray, const cl_uint numParticles)
{
	// The general flow here is:
	// 1) Write data to OpenCL buffers.
	// 2) Execute kernel.
	// 3) Read data back from OpenCL buffers.
	// 4) Block until all reads have completed.

	using namespace CLcpp;

	Camera* const camera = Camera::Get();
	ParticleSystem* const particleSystem = ParticleSystem::Get();
	Metrics* const metrics = Metrics::Get();

	// Construct the constant data for this frame.
	const FrameConstant constantData =
	{
		camera->GetBasisAxes(),
		particleSystem->GetGravityCenter(),
		particleSystem->GetEmitterPosition(),
		particleSystem->GetGravityStrength(),
		metrics->GetTimeDelta(),
		numParticles
	};

	const size_t dataBufferSize = sizeof(ParticleData) * numParticles;
	const size_t vertexBufferSize = sizeof(ParticleVertex) * numParticles * 4;
	const size_t constantBufferSize = sizeof(FrameConstant);

	// The global work size will be the number of particles we need to process.
	const size_t globalWorkSize[3] = { numParticles, 0, 0 };

	std::vector<Event> eventVector;

	Event dataWriteEvent;
	Event vertexWriteEvent;
	Event constantWriteEvent;
	Event kernelExecuteEvent;
	Event dataReadEvent;
	Event vertexReadEvent;

	// Write the data needed by the kernel.
	OCL_CHECK_ERROR(m_commandQueue.EnqueueWriteBuffer(dataWriteEvent, eventVector, m_particleDataBuffer, false, 0, dataBufferSize, particleDataArray));
	OCL_CHECK_ERROR(m_commandQueue.EnqueueWriteBuffer(constantWriteEvent, eventVector, m_constantBuffer, false, 0, constantBufferSize, &constantData));

	if(!m_hasPushedVertexData)
	{
		// The vertex data only needs to be pushed to the device once.  This is so the buffer contains the static vertex data and doesn't
		// overwrite it with incorrect data upon reading the buffer back after the kernel has been run.
		OCL_CHECK_ERROR(m_commandQueue.EnqueueWriteBuffer(vertexWriteEvent, eventVector, m_particleVertexBuffer, false, 0, sizeof(ParticleVertex) * MAX_PARTICLES * 4, particleVertexArray));
		eventVector.push_back(std::move(vertexWriteEvent));

		m_hasPushedVertexData = true;
	}

	eventVector.push_back(std::move(dataWriteEvent));
	eventVector.push_back(std::move(constantWriteEvent));

	// Run the kernel Process function.
	OCL_CHECK_ERROR(m_commandQueue.EnqueueNDKernel(kernelExecuteEvent, eventVector, m_kernel, 1, nullptr, globalWorkSize, nullptr));

	eventVector.clear();
	eventVector.push_back(std::move(kernelExecuteEvent));

	// Read back the modified data.
	OCL_CHECK_ERROR(m_commandQueue.EnqueueReadBuffer(dataReadEvent, eventVector, m_particleDataBuffer, false, 0, dataBufferSize, particleDataArray));
	OCL_CHECK_ERROR(m_commandQueue.EnqueueReadBuffer(vertexReadEvent, eventVector,m_particleVertexBuffer, false, 0, vertexBufferSize, particleVertexArray));

	eventVector.clear();
	eventVector.push_back(std::move(dataReadEvent));
	eventVector.push_back(std::move(vertexReadEvent));

	// Wait for the read events to finish before continuing.
	OCL_CHECK_ERROR(Event::WaitForEvents(eventVector));
}
