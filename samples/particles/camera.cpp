/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#include "camera.h"


template<>
Camera* ISingleton<Camera>::ms_pSingleton = nullptr;


Camera::Camera()
	: m_aspectRatio(1.0f)
	, m_fovY(90.0f)
	, m_nearZ(0.0f)
	, m_farZ(1.0f)
{
	m_position.x = 0.0f;
	m_position.y = 0.0f;
	m_position.z = 0.0f;

	m_target.x = 0.0f;
	m_target.y = 0.0f;
	m_target.z = 1.0f;

	m_up.x = 0.0f;
	m_up.y = 1.0f;
	m_up.z = 0.0f;
}


void Camera::GenerateViewMatrix(cl_float matrixData[16]) const
{
	Vector4 forward = m_target - m_position;

	// Make sure the forward direction isn't a zero length vector.
	if(forward * forward > .00001f)
		forward.Normalize();
	else
		forward.z = 1.0f;

	Vector4 axisZ = forward;
	Vector4 axisX = m_up ^ axisZ;
	Vector4 axisY = axisZ ^ axisX;

	Vector4 dotPosition =
	{
		axisX * m_position,
		axisY * m_position,
		axisZ * m_position
	};

	// Save the basis vectors.
	const_cast<Camera*>(this)->m_basisAxes.row[0] = axisX;
	const_cast<Camera*>(this)->m_basisAxes.row[1] = axisY;
	const_cast<Camera*>(this)->m_basisAxes.row[2] = axisZ;

	matrixData[0] = axisX.x;
	matrixData[1] = axisY.x;
	matrixData[2] = axisZ.x;
	matrixData[3] = 0.0f;

	matrixData[4] = axisX.y;
	matrixData[5] = axisY.y;
	matrixData[6] = axisZ.y;
	matrixData[7] = 0.0f;

	matrixData[8] = axisX.z;
	matrixData[9] = axisY.z;
	matrixData[10] = axisZ.z;
	matrixData[11] = 0.0f;

	matrixData[12] = -dotPosition.x;
	matrixData[13] = -dotPosition.y;
	matrixData[14] = -dotPosition.z;
	matrixData[15] = 1.0f;
}


void Camera::GenerateProjectionMatrix(cl_float matrixData[16]) const
{
	cl_float angle = MATH_DEG_TO_RAD(m_fovY);
	cl_float scaleY = 1.0f / tanf(angle * 0.5f);
	cl_float scaleX = scaleY / m_aspectRatio;
	cl_float diffZ = m_farZ - m_nearZ;

	matrixData[0] = scaleX;
	matrixData[1] = 0.0f;
	matrixData[2] = 0.0f;
	matrixData[3] = 0.0f;

	matrixData[4] = 0.0f;
	matrixData[5] = scaleY;
	matrixData[6] = 0.0f;
	matrixData[7] = 0.0f;

	matrixData[8] = 0.0f;
	matrixData[9] = 0.0f;
	matrixData[10] = m_farZ / diffZ;
	matrixData[11] = 1.0f;

	matrixData[12] = 0.0f;
	matrixData[13] = 0.0f;
	matrixData[14] = -m_nearZ * m_farZ / diffZ;
	matrixData[15] = 0.0f;
}
