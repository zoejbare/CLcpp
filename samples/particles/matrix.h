/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

#include "vector.h"


struct Matrix
{
	Vector4 row[3];


	static Matrix Identity()
	{
		const Matrix m =
		{
			{
			{ 1.0f, 0.0f, 0.0f, 0.0f },
			{ 0.0f, 1.0f, 0.0f, 0.0f },
			{ 0.0f, 0.0f, 1.0f, 0.0f },
			}
		};

		return m;
	}

	static inline Matrix ArbitraryRotation(const Vector4& axis, const cl_float angle)
	{
		// Reference: http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToMatrix/index.htm

		const cl_float cosAngle = cosf(angle);
		const cl_float sinAngle = sinf(angle);
		const cl_float oneMinusCos = 1.0f - cosAngle;

		const Matrix m =
		{
			{
				// row[0]
				{
					(oneMinusCos * axis.x * axis.x) + cosAngle,
					(oneMinusCos * axis.x * axis.y) + (axis.z * sinAngle),
					(oneMinusCos * axis.x * axis.z) - (axis.y * sinAngle),
					0
				},

				// row[1]
				{
					(oneMinusCos * axis.x * axis.y) - (axis.z * sinAngle),
					(oneMinusCos * axis.y * axis.y) + cosAngle,
					(oneMinusCos * axis.y * axis.z) + (axis.x * sinAngle),
					0
				},

				// row[2]
				{
					(oneMinusCos * axis.x * axis.z) + (axis.y * sinAngle),
					(oneMinusCos * axis.y * axis.z) - (axis.x * sinAngle),
					(oneMinusCos * axis.z * axis.z) + cosAngle,
					0
				},
			}
		};

		return m;
	}

	inline Matrix& Transpose()
	{
		const Matrix m =
		{
			{
				{ row[0].x, row[1].x, row[2].x, 0 },
				{ row[0].y, row[1].y, row[2].y, 0 },
				{ row[0].z, row[1].z, row[2].z, 0 },
			}
		};

		(*this) = m;

		return (*this);
	}

	inline Matrix operator +(const Matrix& other) const
	{
		const Matrix m =
		{
			{
				{ row[0].x + other.row[0].x, row[0].y + other.row[0].y, row[0].z + other.row[0].z, 0 },
				{ row[1].x + other.row[1].x, row[1].y + other.row[1].y, row[1].z + other.row[1].z, 0 },
				{ row[2].x + other.row[2].x, row[2].y + other.row[2].y, row[2].z + other.row[2].z, 0 },
			}
		};

		return m;
	}

	inline Matrix operator -(const Matrix& other) const
	{
		const Matrix m =
		{
			{
				{ row[0].x - other.row[0].x, row[0].y - other.row[0].y, row[0].z - other.row[0].z, 0 },
				{ row[1].x - other.row[1].x, row[1].y - other.row[1].y, row[1].z - other.row[1].z, 0 },
				{ row[2].x - other.row[2].x, row[2].y - other.row[2].y, row[2].z - other.row[2].z, 0 },
			}
		};

		return m;
	}

	inline Matrix operator *(const Matrix& other) const
	{
		const Matrix m =
		{
			{
				// row[0]
				{
					(row[0].x * other.row[0].x) + (row[0].y * other.row[1].x) + (row[0].z * other.row[2].x),
					(row[0].x * other.row[0].y) + (row[0].y * other.row[1].y) + (row[0].z * other.row[2].y),
					(row[0].x * other.row[0].z) + (row[0].y * other.row[1].z) + (row[0].z * other.row[2].z),
					0
				},

				// row[1]
				{
					(row[1].x * other.row[0].x) + (row[1].y * other.row[1].x) + (row[1].z * other.row[2].x),
					(row[1].x * other.row[0].y) + (row[1].y * other.row[1].y) + (row[1].z * other.row[2].y),
					(row[1].x * other.row[0].z) + (row[1].y * other.row[1].z) + (row[1].z * other.row[2].z),
					0
				},

				// row[2]
				{
					(row[2].x * other.row[0].x) + (row[2].y * other.row[1].x) + (row[2].z * other.row[2].x),
					(row[2].x * other.row[0].y) + (row[2].y * other.row[1].y) + (row[2].z * other.row[2].y),
					(row[2].x * other.row[0].z) + (row[2].y * other.row[1].z) + (row[2].z * other.row[2].z),
					0
				},
			}
		};

		return m;
	}

	inline Matrix& operator +=(const Matrix& other)
	{
		row[0].x = row[0].x + other.row[0].x;
		row[0].y = row[0].y + other.row[0].y;
		row[0].z = row[0].z + other.row[0].z;

		row[1].x = row[1].x + other.row[1].x;
		row[1].y = row[1].y + other.row[1].y;
		row[1].z = row[1].z + other.row[1].z;

		row[2].x = row[2].x + other.row[2].x;
		row[2].y = row[2].y + other.row[2].y;
		row[2].z = row[2].z + other.row[2].z;

		return (*this);
	}

	inline Matrix& operator -=(const Matrix& other)
	{
		row[0].x = row[0].x - other.row[0].x;
		row[0].y = row[0].y - other.row[0].y;
		row[0].z = row[0].z - other.row[0].z;

		row[1].x = row[1].x - other.row[1].x;
		row[1].y = row[1].y - other.row[1].y;
		row[1].z = row[1].z - other.row[1].z;

		row[2].x = row[2].x - other.row[2].x;
		row[2].y = row[2].y - other.row[2].y;
		row[2].z = row[2].z - other.row[2].z;

		return (*this);
	}
};
