/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

#define VERBOSE_OUTPUT

#ifdef _WIN32
#	define _CRT_SECURE_NO_WARNINGS
#	define WIN32_LEAN_AND_MEAN

#	include <windows.h>
#endif

#include <CLcpp.h>

#include <stdlib.h>
#include <stdio.h>

#include <GLFW/glfw3.h>

#ifndef GL_CLAMP_TO_EDGE
#	define GL_CLAMP_TO_EDGE 0x812F
#endif

#ifdef VERBOSE_OUTPUT
#	define OCL_CHECK_ERROR(expr) \
	{ \
		const cl_int errorCode = (expr); \
		if(errorCode != CL_SUCCESS) \
		{ \
			printf("<< Error Code (%d) >> " #expr "\n", errorCode); \
		}\
	}
#	define OCL_CHECK_ERROR_WITH_STATUS(status, expr) \
	if(status) \
	{ \
		const cl_int errorCode = (expr); \
		if(errorCode != CL_SUCCESS) \
		{ \
			printf("<< Error Code (%d) >> " #expr "\n", errorCode); \
			status = false; \
		}\
	}
#else
#	define OCL_CHECK_ERROR(expr) expr
#	define OCL_CHECK_ERROR_WITH_STATUS(sts, expr) \
	if(status) \
	{ \
		const cl_int errorCode = (expr); \
		if(errorCode != CL_SUCCESS) \
		{ \
			status = false; \
		}\
	}

#endif

#define SAFE_DELETE(x)       if(x) { delete (x); (x) = NULL; }
#define SAFE_DELETE_ARRAY(x) if(x) { delete[] (x); (x) = NULL; }

#define MATH_PI            3.1415926535897932384626433832795f
#define MATH_2_PI          (MATH_PI * 2.0f)
#define MATH_EPSILON       0.0000001f
#define MATH_DEG_TO_RAD(x) ((x) * MATH_PI * 0.00555555555555555555555555555556f) // (x * PI / 180)
