/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#include "particle_processor_cpu.h"
#include "particle.h"
#include "camera.h"
#include "metrics.h"


template<>
ParticleProcessorCPU* ISingleton<ParticleProcessorCPU>::ms_pSingleton = nullptr;


const char* ParticleProcessorCPU::GetName() const
{
	return "CPU Particle Processor";
}


void ParticleProcessorCPU::DoWork(ParticleData* const particleDataArray, ParticleVertex* const particleVertexArray, const cl_uint numParticles)
{
	Camera* const camera = Camera::Get();
	Metrics* const metrics = Metrics::Get();
	ParticleSystem* const particleSystem = ParticleSystem::Get();

	const cl_float timeDelta = metrics->GetTimeDelta();
	const cl_float gravityStrength = particleSystem->GetGravityStrength();

	const Vector4 gravityCenter = particleSystem->GetGravityCenter();
	const Vector4 emitterPosition = particleSystem->GetEmitterPosition();
	const Matrix cameraBasis = camera->GetBasisAxes();

	// Update each particle individually.
	for(cl_uint dataIndex = 0; dataIndex < numParticles; ++dataIndex)
	{
		ParticleData& particleData = particleDataArray[dataIndex];

		Vector4 upperLeft = -cameraBasis.row[0] + cameraBasis.row[1];
		Vector4 upperRight = cameraBasis.row[0] + cameraBasis.row[1];
		Vector4 lowerLeft = -cameraBasis.row[0] - cameraBasis.row[1];
		Vector4 lowerRight = cameraBasis.row[0] - cameraBasis.row[1];

		// Calculate the rotation matrix based on the camera's Z axis.
		// Transposing the matrix will make applying it to the verts much easier (it becomes a series of dot products).
		Matrix rotation = Matrix::ArbitraryRotation(cameraBasis.row[2], particleData.rotation).Transpose();

		// Update the particle's rotation.
		particleData.rotation += particleData.rotationVelocity * timeDelta;

		// Rotate the upper left vert.
		cl_float tempX = upperLeft * rotation.row[0];
		cl_float tempY = upperLeft * rotation.row[1];
		cl_float tempZ = upperLeft * rotation.row[2];

		upperLeft.x = tempX;
		upperLeft.y = tempY;
		upperLeft.z = tempZ;

		// Rotate the upper right vert.
		tempX = upperRight * rotation.row[0];
		tempY = upperRight * rotation.row[1];
		tempZ = upperRight * rotation.row[2];

		upperRight.x = tempX;
		upperRight.y = tempY;
		upperRight.z = tempZ;

		// Rotate the lower left vert.
		tempX = lowerLeft * rotation.row[0];
		tempY = lowerLeft * rotation.row[1];
		tempZ = lowerLeft * rotation.row[2];

		lowerLeft.x = tempX;
		lowerLeft.y = tempY;
		lowerLeft.z = tempZ;

		// Rotate the lower right vert.
		tempX = lowerRight * rotation.row[0];
		tempY = lowerRight * rotation.row[1];
		tempZ = lowerRight * rotation.row[2];

		lowerRight.x = tempX;
		lowerRight.y = tempY;
		lowerRight.z = tempZ;

		// Is the particle still alive?
		if(particleData.currentLife > timeDelta)
		{
			Vector4 acceleration = gravityCenter - particleData.position;

			// No velocity change if the particle is very close to (or at) the gravity center.
			if(acceleration * acceleration > MATH_EPSILON)
			{
				acceleration.Normalize();
				acceleration *= (timeDelta * gravityStrength) / particleData.size; // Use the particle size to simulate mass.
			}

			// Apply the rules of linear motion to the particle using a very simple Euler integrator.
			particleData.velocity += acceleration;
			particleData.position += particleData.velocity * timeDelta;
			particleData.currentLife -= timeDelta;
		}
		else
		{
			// When a particle dies, just recycle it.
			particleData.position = emitterPosition;
			particleData.velocity = particleData.initialVelocity;
			particleData.currentLife = particleData.startingLife;
		}

		// Use the particle's life to determine its alpha.
		const cl_float particleAlpha = particleData.currentLife / particleData.startingLife;
		const cl_uint vertIndex = dataIndex * 4;

		// Construct the particle's vertex positions.
		particleVertexArray[vertIndex + 0].position = (upperLeft * particleData.size * particleAlpha) + particleData.position;
		particleVertexArray[vertIndex + 1].position = (upperRight * particleData.size * particleAlpha) + particleData.position;
		particleVertexArray[vertIndex + 2].position = (lowerLeft * particleData.size * particleAlpha) + particleData.position;
		particleVertexArray[vertIndex + 3].position = (lowerRight * particleData.size * particleAlpha) + particleData.position;

		// Remove all blue color data to indicate to the user that we're using the CPU particle update.
		particleVertexArray[vertIndex + 0].color.z = 0.0f;
		particleVertexArray[vertIndex + 1].color.z = 0.0f;
		particleVertexArray[vertIndex + 2].color.z = 0.0f;
		particleVertexArray[vertIndex + 3].color.z = 0.0f;

		particleVertexArray[vertIndex + 0].color.w = particleAlpha;
		particleVertexArray[vertIndex + 1].color.w = particleAlpha;
		particleVertexArray[vertIndex + 2].color.w = particleAlpha;
		particleVertexArray[vertIndex + 3].color.w = particleAlpha;
	}
}
