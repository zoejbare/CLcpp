/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#include "utility.h"
#include "mersenne_twister.h"

#ifndef APIENTRY
#	define APIENTRY
#endif

typedef void (APIENTRY *PFNGENERATEMIPMAP)(GLenum);

PFNGENERATEMIPMAP glGenerateMipmapFunc = nullptr;


bool Utility::LoadExtensions()
{
	glGenerateMipmapFunc = (PFNGENERATEMIPMAP) glfwGetProcAddress("glGenerateMipmap");
	if(!glGenerateMipmapFunc)
		return false;

	return true;
}


std::string Utility::LoadSourceBuffer(const char* const filePath)
{
	if(!filePath)
		return std::string("");

	// Open the file.
	FILE* const fileHandle = fopen(filePath, "rb");

	if(!fileHandle)
	{
		// Return an empty string if the file doesn't exist.
		return std::string("");
	}

	// Go to the end of the file.
	fseek(fileHandle, 0, SEEK_END);

	// Get the size of the file and allocate space for the entire file.
	const size_t fileSize = (size_t) ftell(fileHandle);
	char* const sourceBuffer = new char[fileSize];

	// Return to the start of the file.
	fseek(fileHandle, 0, SEEK_SET);

	// Read the file contents into the temporary buffer, then close the file.
	const size_t numBytesRead = fread(sourceBuffer, 1, fileSize, fileHandle);
	fclose(fileHandle);

	// Copy the temporary buffer into the output string, then delete the buffer.
	std::string outString(sourceBuffer, numBytesRead);
	delete[] sourceBuffer;

	return outString;
}


void Utility::RandomSeed(const cl_uint seed)
{
	MersenneTwister::Seed(seed);
}


cl_int Utility::RandomNumber(const cl_int minValue, const cl_int maxValue)
{
	cl_int range = maxValue - minValue;
	cl_int realMin = minValue;

	// If the range is 0, there is nothing to generate!
	if(range == 0)
		return minValue;

	// The logic needs to be flipped if the range is negative.
	else if(range < 0)
	{
		range = -range;
		realMin = maxValue;
	}

	return (MersenneTwister::Generate() % range) + realMin;
}


cl_float Utility::RandomNumberF(const cl_float minValue, const cl_float maxValue)
{
	cl_float scale = (cl_float) RandomNumber(0, 100000) / 100000;
	cl_float range = maxValue - minValue;
	cl_float realMin = minValue;

	if(minValue > maxValue)
	{
		range = -range;
		realMin = maxValue;
	}

	return (scale * range) + realMin;
}


void* Utility::CustomAlloc(size_t size)
{
	return malloc(size);
}


void Utility::CustomFree(void* data)
{
	free(data);
}


GLuint Utility::LoadTga(const char* filePath)
{
	if(!filePath || filePath[0] == '\0')
		return 0;

	printf("Loading texture: %s\n", filePath);

	GLubyte header[18];
	GLsizei width = 0;
	GLsizei height = 0;
	GLsizei numChannels = 0;
	GLsizei imgSize = 0;
	GLubyte* pixelData = NULL;
	size_t temp = 0;

	FILE* fileHandle = fopen(filePath, "rb");
	if(!fileHandle)
	{
		fprintf(stderr, "ERROR: Texture file [%s] not found!\n", filePath);
		return 0;
	}

	temp = fread(header, 1, 18, fileHandle);

	// Only uncompressed, non-colormapped, true color images are supported currently.
	if(temp != 18 || header[1] != 0 || header[2] != 2)
	{
		fprintf(stderr, "ERROR: Texture file [%s] contains unsupported features!\n", filePath);
		fclose(fileHandle);
		return 0;
	}

	width = header[12] | (((GLsizei) header[13]) << 8);
	height = header[14] | (((GLsizei) header[15]) << 8);
	numChannels = (GLsizei)(header[16] / 8);
	imgSize = width * height * numChannels;
	pixelData = new GLubyte[imgSize];

	temp = fread(pixelData, 1, imgSize, fileHandle);
	fclose(fileHandle);

	if(temp != size_t(imgSize))
	{
		fprintf(stderr, "ERROR: Texture file [%s] is corrupted!\n", filePath);
		return 0;
	}

	for(GLsizei i = 0; i < imgSize; i += numChannels)
	{
		GLubyte swapValue = pixelData[i];

		pixelData[i] = pixelData[i + 2];
		pixelData[i + 2] = swapValue;
	}

	const GLenum format = (numChannels == 4) ? GL_RGBA : GL_RGB;

	GLuint texture = 0;

	// Create the OpenGL texture.
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	// Setup the wrapping and filtering properties for this texture.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	// VERY IMPORTANT!  Make sure the vertex alpha and texture alpha are modulated.
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	// Upload the pixel data to the texture and generate its mipmaps.
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, pixelData);
	glGenerateMipmapFunc(GL_TEXTURE_2D);

	// We're done, unbind the texture.
	glBindTexture(GL_TEXTURE_2D, 0);

	SAFE_DELETE_ARRAY(pixelData);

	return texture;
}


void Utility::FreeTexture(GLuint& texture)
{
	if(texture == 0)
		return;

	glDeleteTextures(1, &texture);
	texture = 0;
}
