/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

typedef struct tag_particle_data
{
	float4 position;
	float4 velocity;
	float4 initialVelocity;

	float rotation;
	float rotationVelocity;

	float startingLife;
	float currentLife;
	float size;

	float colorRed;
	float colorGreen;
} ParticleData;

typedef struct tag_particle_vertex
{
	float4 position;
	float4 texCoord;
	float4 color;
} ParticleVertex;

typedef struct tag_frame_constant
{
	float4 cameraBasis[3];

	float4 gravityCenter;
	float4 emitterPosition;

	float gravityStrength;
	float timeDelta;

	uint numParticles;
} FrameConstant;


// Create a rotation matrix from an axis and angle.
// Code based on Matrix::ArbitraryRotation() in matrix.h.
inline void ArbitraryRotation(float4* const matrixRow, const float4 axis, const float angle)
{
	const float cosAngle = cos(angle);
	const float sinAngle = sin(angle);
	const float oneMinusCos = 1 - cosAngle;

	// Store matrix pre-transposed for easier vector transformations.

	matrixRow[0] = (float4)(
		(oneMinusCos * axis.x * axis.x) + cosAngle,
		(oneMinusCos * axis.x * axis.y) - (axis.z * sinAngle),
		(oneMinusCos * axis.x * axis.z) + (axis.y * sinAngle),
		0
	);

	matrixRow[1] = (float4)(
		(oneMinusCos * axis.x * axis.y) + (axis.z * sinAngle),
		(oneMinusCos * axis.y * axis.y) + cosAngle,
		(oneMinusCos * axis.y * axis.z) - (axis.x * sinAngle),
		0
	);

	matrixRow[2] = (float4)(
		(oneMinusCos * axis.x * axis.z) - (axis.y * sinAngle),
		(oneMinusCos * axis.y * axis.z) + (axis.x * sinAngle),
		(oneMinusCos * axis.z * axis.z) + cosAngle,
		0
	);
}


// Kernel to process a set of particles.
// Code based on ParticleProcessorCPU::Process() in particle_processor_cpu.cpp.
__kernel void Process(
	__global ParticleData* const particleDataBuffer,
	__global ParticleVertex* const particleVertexBuffer,
	__constant FrameConstant* const frameConstantBuffer
)
{
	const int dataIndex = get_global_id(0);
	const int vertIndex = dataIndex * 4;

	// Do not exceed the bounds of the particle arrays!
	if(dataIndex >= frameConstantBuffer->numParticles)
		return;

	__global ParticleData* const particleData = &particleDataBuffer[dataIndex];

	// Construct the untransformed vert positions of a billboarded quad.
	float4 upperLeft = -frameConstantBuffer->cameraBasis[0] + frameConstantBuffer->cameraBasis[1];
	float4 upperRight = frameConstantBuffer->cameraBasis[0] + frameConstantBuffer->cameraBasis[1];
	float4 lowerLeft = -frameConstantBuffer->cameraBasis[0] - frameConstantBuffer->cameraBasis[1];
	float4 lowerRight = frameConstantBuffer->cameraBasis[0] - frameConstantBuffer->cameraBasis[1];

	float4 matrixRow[3];
	float4 temp;

	// Calculate the rotation matrix based on the camera's Z axis.
	ArbitraryRotation(matrixRow, frameConstantBuffer->cameraBasis[2], particleData->rotation);

	// Update the particle's rotation.
	particleData->rotation += particleData->rotationVelocity * frameConstantBuffer->timeDelta;

	// Rotate the upper left vert.
	temp.x = dot(upperLeft, matrixRow[0]);
	temp.y = dot(upperLeft, matrixRow[1]);
	temp.z = dot(upperLeft, matrixRow[2]);

	upperLeft = temp;

	// Rotate the upper right vert.
	temp.x = dot(upperRight, matrixRow[0]);
	temp.y = dot(upperRight, matrixRow[1]);
	temp.z = dot(upperRight, matrixRow[2]);

	upperRight = temp;

	// Rotate the lower left vert.
	temp.x = dot(lowerLeft, matrixRow[0]);
	temp.y = dot(lowerLeft, matrixRow[1]);
	temp.z = dot(lowerLeft, matrixRow[2]);

	lowerLeft = temp;

	// Rotate the lower right vert.
	temp.x = dot(lowerRight, matrixRow[0]);
	temp.y = dot(lowerRight, matrixRow[1]);
	temp.z = dot(lowerRight, matrixRow[2]);

	lowerRight = temp;

	// Is the particle still alive?
	if(particleData->currentLife > frameConstantBuffer->timeDelta)
	{
		float4 acceleration = frameConstantBuffer->gravityCenter - particleData->position;

		// No velocity change if the particle is very close to (or at) the gravity center.
		if(fast_length(acceleration) > .0001f)
		{
			acceleration = fast_normalize(acceleration);
			acceleration *= (frameConstantBuffer->timeDelta * frameConstantBuffer->gravityStrength) / particleData->size; // Use the particle size to simulate mass.
		}

		// Apply the rules of linear motion to the particle using a very simple Euler integrator.
		particleData->velocity += acceleration;
		particleData->position += particleData->velocity * frameConstantBuffer->timeDelta;
		particleData->currentLife -= frameConstantBuffer->timeDelta;
	}
	else
	{
		// When a particle dies, just recycle it.
		particleData->position = frameConstantBuffer->emitterPosition;
		particleData->velocity = particleData->initialVelocity;
		particleData->currentLife = particleData->startingLife;
	}

	// Use the particle's life to determine its  alpha.
	const float particleAlpha = particleData->currentLife / particleData->startingLife;

	// Construct the particle's vertex positions.
	particleVertexBuffer[vertIndex + 0].position = (upperLeft * particleData->size * particleAlpha) + particleData->position;
	particleVertexBuffer[vertIndex + 1].position = (upperRight * particleData->size * particleAlpha) + particleData->position;
	particleVertexBuffer[vertIndex + 2].position = (lowerLeft * particleData->size * particleAlpha) + particleData->position;
	particleVertexBuffer[vertIndex + 3].position = (lowerRight * particleData->size * particleAlpha) + particleData->position;

	// Add blue color data to indicate to the user that we're using the OpenCL particle update.
	particleVertexBuffer[vertIndex + 0].color.z = .5f;
	particleVertexBuffer[vertIndex + 1].color.z = .5f;
	particleVertexBuffer[vertIndex + 2].color.z = .5f;
	particleVertexBuffer[vertIndex + 3].color.z = .5f;

	particleVertexBuffer[vertIndex + 0].color.w = particleAlpha;
	particleVertexBuffer[vertIndex + 1].color.w = particleAlpha;
	particleVertexBuffer[vertIndex + 2].color.w = particleAlpha;
	particleVertexBuffer[vertIndex + 3].color.w = particleAlpha;
}