/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

#include "singleton.h"
#include "matrix.h"


class Camera : public ISingleton<Camera>
{
public:

	Camera();

	void SetPosition(const Vector4& position) { m_position = position; }
	void SetTarget(const Vector4& Target) { m_target = Target; }
	void SetUp(const Vector4& Up) { m_up = Up; m_up.Normalize(); }

	void SetAspectRatio(cl_float fAspect) { m_aspectRatio = fAspect; }
	void SetFovY(cl_float fFov) { m_fovY = fFov; }
	void SetNearZ(cl_float fZ) { m_nearZ = fZ; }
	void SetFarZ(cl_float fZ) { m_farZ = fZ; }

	Vector4 GetPosition() const { return m_position; }
	Vector4 GetTarget() const { return m_target; }
	Vector4 GetUp() const { return m_up; }
	Matrix GetBasisAxes() const { return m_basisAxes; }

	void GenerateViewMatrix(cl_float matrixData[16]) const;
	void GenerateProjectionMatrix(cl_float matrixData[16]) const;


private:

	Matrix m_basisAxes;

	Vector4 m_position;
	Vector4 m_target;
	Vector4 m_up;

	cl_float m_aspectRatio;
	cl_float m_fovY;
	cl_float m_nearZ;
	cl_float m_farZ;
};
