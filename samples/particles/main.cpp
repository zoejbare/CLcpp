/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#include "camera.h"
#include "metrics.h"
#include "particle.h"
#include "particle_processor_cpu.h"
#include "particle_processor_cl.h"
#include "utility.h"

#if defined(_WIN32) && defined(_MSC_VER)
#	include <crtdbg.h>
#endif


const cl_float MIN_CAMERA_DISTANCE = 20.0f;
const cl_float MAX_CAMERA_DISTANCE = 225.0f;

GLuint g_particleTexture = 0;
GLuint g_indicatorTexture = 0;

bool g_enableParticleProcessing = true;
bool g_isMouseLeftDown = false;


void SwitchParticleProcessor();
void RotateCamera(double, double);
void MoveParticleEmitter();
void MoveGravityCenter();
void DrawGravityCenter();
void DrawParticles();

int main(int argc, char* argv[])
{
	(void)(argc);
	(void)(argv);

#if defined(_WIN32) && defined(_MSC_VER)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	auto errorCallback = [](int errorCode, const char* desc)
	{
		(void)(errorCode);
		fputs(desc, stderr);
	};

	auto keyCallback = [](GLFWwindow* window, int key, int scanCode, int action, int modMask)
	{
		(void)(scanCode);
		(void)(modMask);

		if(action == GLFW_PRESS)
		{
			switch(key)
			{
				case GLFW_KEY_ESCAPE:
					glfwSetWindowShouldClose(window, GL_TRUE);
					break;

				case GLFW_KEY_SPACE:
					SwitchParticleProcessor();
					break;

				case GLFW_KEY_BACKSPACE:
					ParticleSystem::Get()->Reset();
					break;

				case GLFW_KEY_ENTER:
					g_enableParticleProcessing = !g_enableParticleProcessing;
					printf("Enable Particle Processing: %s\n", g_enableParticleProcessing ? "TRUE" : "FALSE");
					break;
			}
		}
	};

	auto mouseButtonCallback = [](GLFWwindow* window, int button, int action, int modMask)
	{
		(void)(window);
		(void)(modMask);

		if(button == GLFW_MOUSE_BUTTON_LEFT)
			g_isMouseLeftDown = (action == GLFW_PRESS);
	};

	auto mouseMoveCallback = [](GLFWwindow* window, double posX, double posY)
	{
		(void)(window);

		static double s_lastMouseX = posX;
		static double s_lastMouseY = posY;

		if(g_isMouseLeftDown)
			RotateCamera(posX - s_lastMouseX, posY - s_lastMouseY);

		s_lastMouseX = posX;
		s_lastMouseY = posY;
	};


	glfwSetErrorCallback(errorCallback);

	if(glfwInit() == 0)
	{
		fputs("ERROR: Failed to initialize GLFW!", stderr);
		return -1;
	}

	GLFWwindow* const appWindow = glfwCreateWindow(1024, 768, "CLcpp Particles Demo", nullptr, nullptr);
	if(!appWindow)
	{
		glfwTerminate();
		return -1;
	}

	glfwSetKeyCallback(appWindow, keyCallback);
	glfwSetMouseButtonCallback(appWindow, mouseButtonCallback);
	glfwSetCursorPosCallback(appWindow, mouseMoveCallback);

	glfwMakeContextCurrent(appWindow);
	glfwSwapInterval(1);

	if(!Utility::LoadExtensions())
	{
		fputs("ERROR: Failed to load OpenGL extensions!", stderr);
		return -1;
	}

	Utility::RandomSeed(987654321);

	// Initialize our singletons.
	Camera::InitializeSingleton();
	Metrics::InitializeSingleton();
	ParticleSystem::InitializeSingleton();
	ParticleProcessorCPU::InitializeSingleton();
	ParticleProcessorOpenCL::InitializeSingleton();

	// Setup the OpenCL particle processor.
	if(!ParticleProcessorOpenCL::Get()->SetupOpenCL())
	{
		exit(-1);
	}

	Camera* const camera = Camera::Get();
	Metrics* const metrics = Metrics::Get();
	ParticleSystem* const particleSystem = ParticleSystem::Get();

	const Vector4 cameraTarget = { 0.0f, 5.0f, 0.0f };
	const Vector4 cameraUp = { 0.0f, 1.0f, 0.0f };

	camera->SetTarget(cameraTarget);
	camera->SetUp(cameraUp);
	camera->SetFovY(60.0f);
	camera->SetNearZ(0.1f);
	camera->SetFarZ(400.0f);

	// Set the camera to its default position.
	RotateCamera(0.0f, 0.0f);

	// Setup some basic properties for the particle system.
	particleSystem->SetMinParticleSize(0.75f);
	particleSystem->SetMaxParticleSize(1.5f);
	particleSystem->SetMinParticleLife(4.0f);
	particleSystem->SetMaxParticleLife(9.0f);
	particleSystem->SetGravityStrength(70.0f);
	particleSystem->Reset();

	// The initial call to switch the particle processor will set it to use CPU processor.
	SwitchParticleProcessor();

	// Setup some render state defaults.
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);

	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_CULL_FACE);

	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	// Load the textures.
	g_particleTexture = Utility::LoadTga("../../../../samples/particles/data/particle.tga");
	g_indicatorTexture = Utility::LoadTga("../../../../samples/particles/data/indicator.tga");

	// Main loop.
	while(!glfwWindowShouldClose(appWindow))
	{
		int framebufferWidth, framebufferHeight;
		glfwGetFramebufferSize(appWindow, &framebufferWidth, &framebufferHeight);

		if(framebufferWidth > 0 && framebufferHeight > 0)
		{
			const cl_float aspectRatio = cl_float(framebufferWidth) / cl_float(framebufferHeight);
			cl_float projMatrixData[16];

			camera->SetAspectRatio(aspectRatio);
			camera->GenerateProjectionMatrix(projMatrixData);

			glMatrixMode(GL_PROJECTION);
			glLoadMatrixf(projMatrixData);
		}

		cl_float viewMatrixData[16];

		camera->GenerateViewMatrix(viewMatrixData);
		metrics->Update();

		glViewport(0, 0, framebufferWidth, framebufferHeight);

		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glLoadMatrixf(viewMatrixData);

		DrawGravityCenter();
		DrawParticles();

		glfwSwapBuffers(appWindow);
		glfwPollEvents();
	}

	// Free the loaded textures.
	Utility::FreeTexture(g_particleTexture);
	Utility::FreeTexture(g_indicatorTexture);

	ParticleProcessorOpenCL::DestroySingleton();
	ParticleProcessorCPU::DestroySingleton();
	ParticleSystem::DestroySingleton();
	Metrics::DestroySingleton();
	Camera::DestroySingleton();

	glfwDestroyWindow(appWindow);
	glfwTerminate();

	return 0;
}


void SwitchParticleProcessor()
{
	static cl_int s_processorIndex = 0;

	static IParticleProcessor* const processorArray[] =
	{
		ParticleProcessorCPU::Get(),
		ParticleProcessorOpenCL::Get(),
	};

	IParticleProcessor* const selectedProcessor = processorArray[s_processorIndex];

	printf("Switching to: %s\n", selectedProcessor->GetName());
	ParticleSystem::Get()->SetProcessor(selectedProcessor);

	// Toggle between the processor indices for the next switch.
	s_processorIndex = (s_processorIndex + 1) % 2;
}


void RotateCamera(double mouseRelX, double mouseRelY)
{
	static cl_float s_currentRotAmt = 0.0f;
	static cl_float s_cameraDistance = (MAX_CAMERA_DISTANCE + MIN_CAMERA_DISTANCE) * 0.5f;

	cl_float rotAmt = 0.001f * MATH_PI * (cl_float) mouseRelX;
	cl_float zoomAmt = (cl_float) mouseRelY * 0.25f;

	s_currentRotAmt -= rotAmt;
	s_cameraDistance += zoomAmt;

	// Correct the rotation amount (0 <= s_currentRotAmt < MATH_2_PI).
	if(s_currentRotAmt >= MATH_2_PI)
		s_currentRotAmt -= MATH_2_PI;
	else if(s_currentRotAmt < 0.0f)
		s_currentRotAmt += MATH_2_PI;

	// Don't allow the camera to zoom past certain thresholds.
	if(s_cameraDistance < MIN_CAMERA_DISTANCE)
		s_cameraDistance = MIN_CAMERA_DISTANCE;
	else if(s_cameraDistance > MAX_CAMERA_DISTANCE)
		s_cameraDistance = MAX_CAMERA_DISTANCE;

	const cl_float cosAngle = cosf(s_currentRotAmt);
	const cl_float sinAngle = sinf(s_currentRotAmt);

	Vector4 cameraPosition =
	{
		cosAngle,
		0.25f,
		sinAngle
	};

	cameraPosition.Normalize();
	cameraPosition *= s_cameraDistance;

	Camera::Get()->SetPosition(cameraPosition);
}


void MoveParticleEmitter()
{
	Metrics* const metrics = Metrics::Get();
	ParticleSystem* const particleSystem = ParticleSystem::Get();

	static cl_float posX = 0.0f;
	static cl_float posZ = 0.0f;
	static cl_float time = 0.0f;

	posX = cosf(time) * 40.0f;
	posZ = sinf(time) * 50.0f;
	time += metrics->GetTimeDelta() * 0.15f;

	// Wrap time to be in the range of 0 <= time < (2 * PI).
	if(time > MATH_2_PI)
		time -= MATH_2_PI;

	const Vector4 emitterPosition = { posX, 0.0f, posZ - 5.0f };

	particleSystem->SetEmitterPosition(emitterPosition);
}


void MoveGravityCenter()
{
	Metrics* const metrics = Metrics::Get();
	ParticleSystem* const particleSystem = ParticleSystem::Get();

	static cl_float posX = 0.0f;
	static cl_float posY = 0.0f;
	static cl_float time = 0.0f;

	posX = cosf(time) * 35.0f;
	posY = sinf(time) * 10.0f;
	time += metrics->GetTimeDelta() * 0.85f;

	// Wrap time to be in the range of 0 <= time < (2 * PI).
	if(time > MATH_2_PI)
		time -= MATH_2_PI;

	const Vector4 gravityCenter = { posX, posY, -20.0f };

	particleSystem->SetGravityCenter(gravityCenter);
}


void DrawGravityCenter()
{
	if(g_enableParticleProcessing)
	{
		MoveGravityCenter();
	}

	Camera* const camera = Camera::Get();
	ParticleSystem* const particleSystem = ParticleSystem::Get();

	const Matrix cameraBasis = camera->GetBasisAxes();

	// Calculate the billboarded vert positions.
	const Vector4 pos_UpperLeft = (-cameraBasis.row[0] + cameraBasis.row[1]) * 2.0f;
	const Vector4 pos_UpperRight = (cameraBasis.row[0] + cameraBasis.row[1]) * 2.0f;
	const Vector4 pos_LowerLeft = (-cameraBasis.row[0] - cameraBasis.row[1]) * 2.0f;
	const Vector4 pos_LowerRight = (cameraBasis.row[0] - cameraBasis.row[1]) * 2.0f;

	const Vector4 gravityCenter = particleSystem->GetGravityCenter();

	const GLubyte indexData[6] = { 0, 2, 1, 1, 2, 3 };
	const ParticleVertex vertexData[4] =
	{
		{ pos_UpperLeft + gravityCenter, { 0.0f, 1.0f }, { 1.0f, 0.0f, 0.0f, 1.0f } },
		{ pos_UpperRight + gravityCenter, { 1.0f, 1.0f }, { 1.0f, 0.0f, 0.0f, 1.0f } },
		{ pos_LowerLeft + gravityCenter, { 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f, 1.0f } },
		{ pos_LowerRight + gravityCenter, { 1.0f, 0.0f }, { 1.0f, 0.0f, 0.0f, 1.0f } }
	};

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, g_indicatorTexture);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	glVertexPointer(3, GL_FLOAT, sizeof(ParticleVertex), &vertexData[0].position);
	glTexCoordPointer(2, GL_FLOAT, sizeof(ParticleVertex), &vertexData[0].texCoord);
	glColorPointer(4, GL_FLOAT, sizeof(ParticleVertex), &vertexData[0].color);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, indexData);

	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}


void DrawParticles()
{
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glBindTexture(GL_TEXTURE_2D, g_particleTexture);

	ParticleSystem* const particleSystem = ParticleSystem::Get();

	if(g_enableParticleProcessing)
	{
		MoveParticleEmitter();

		particleSystem->AddParticle(10);
		particleSystem->Update();
	}

	particleSystem->Draw();
}
