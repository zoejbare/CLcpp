/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

#include "globals.h"

// Utility function declarations.
class Utility
{
public:

	static bool LoadExtensions();

	static std::string LoadSourceBuffer(const char* const filePath);

	static void RandomSeed(const cl_uint seed);
	static cl_int RandomNumber(const cl_int minValue, const cl_int maxValue);
	static cl_float RandomNumberF(const cl_float minValue, const cl_float maxValue);

	static void* CustomAlloc(size_t size);
	static void CustomFree(void* data);

	static GLuint LoadTga(const char* const filePath);
	static void FreeTexture(GLuint& texture);
};
