/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

#include "globals.h"
#include <math.h>


struct Vector4
{
	cl_float x;
	cl_float y;
	cl_float z;
	cl_float w;


	Vector4& Normalize()
	{
		const cl_float dot = (*this) * (*this);

		// A zero-length vector will result in an inverse length of NaN!
		if(dot > MATH_EPSILON)
		{
			const cl_float invLen = 1.0f / sqrtf(dot);

			x *= invLen;
			y *= invLen;
			z *= invLen;
		}

		return (*this);
	}

	// Dot product
	cl_float operator *(const Vector4& other) const
	{
		return (x * other.x) + (y * other.y) + (z * other.z);
	}

	// Cross product
	Vector4 operator ^(const Vector4& other) const
	{
		const Vector4 out =
		{
			(y * other.z) - (z * other.y),
			(z * other.x) - (x * other.z),
			(x * other.y) - (y * other.x)
		};

		return out;
	}

	Vector4 operator -() const
	{
		const Vector4 out =
		{
			-x,
			-y,
			-z
		};

		return out;
	}

	Vector4 operator +(const Vector4& other) const
	{
		const Vector4 out =
		{
			x + other.x,
			y + other.y,
			z + other.z
		};

		return out;
	}

	Vector4 operator -(const Vector4& other) const
	{
		const Vector4 out =
		{
			x - other.x,
			y - other.y,
			z - other.z
		};

		return out;
	}

	Vector4 operator *(const cl_float scalar) const
	{
		const Vector4 out =
		{
			x * scalar,
			y * scalar,
			z * scalar
		};

		return out;
	}

	Vector4& operator +=(const Vector4& other)
	{
		x += other.x;
		y += other.y;
		z += other.z;

		return (*this);
	}

	Vector4& operator -=(const Vector4& other)
	{
		x -= other.x;
		y -= other.y;
		z -= other.z;

		return (*this);
	}

	Vector4& operator *=(const cl_float scalar)
	{
		x *= scalar;
		y *= scalar;
		z *= scalar;

		return (*this);
	}
};
