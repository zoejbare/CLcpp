/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#include "metrics.h"
#include "particle.h"


template<>
Metrics* ISingleton<Metrics>::ms_pSingleton = nullptr;

// For tracking the times used to calculate the FPS.
static cl_uint gs_lastFrameTime = 0;


Metrics::Metrics()
	: m_avgFPS(0)
	, m_timeDelta(0)
	, m_accumTimeDelta(0)
	, m_numFpsSamples(0)
	, m_startTime(0)
	, m_endTime(0)
	, m_accumTimeDiff(0)
	, m_numTimerSamples(0)
{
}


void Metrics::Update()
{
	const cl_uint currentTime = (cl_uint)(glfwGetTime() * 1000);
	const cl_uint timeDiff = currentTime - gs_lastFrameTime;

	++m_numFpsSamples;

	gs_lastFrameTime = currentTime;

	// Calculate the current time delta and add it to the delta accumulator.
	m_timeDelta = cl_float(timeDiff) / 1000;
	m_accumTimeDelta += m_timeDelta;

	// Output our metric data every 2 seconds.
	if(m_accumTimeDelta >= 2.0f)
	{
		m_avgFPS = cl_float(m_numFpsSamples) / m_accumTimeDelta;
		m_accumTimeDelta = 0.0f;
		m_numFpsSamples = 0;

		prv_outputMetrics();
	}
}


void Metrics::StartProcessingTimer()
{
	m_startTime = (cl_float) glfwGetTime() * 1000.0f;
}


void Metrics::StopProcessingTimer()
{
	m_endTime = (cl_float) glfwGetTime() * 1000.0f;
	m_accumTimeDiff += m_endTime - m_startTime;

	++m_numTimerSamples;
}


void Metrics::prv_outputMetrics()
{
	ParticleSystem* const particleSystem = ParticleSystem::Get();

	const cl_float avgTime = (m_numTimerSamples > 0) ? (m_accumTimeDiff / cl_float(m_numTimerSamples)) : 0.0f;

	printf(
		"\n"
		"-----------------------------------------------\n"
		"  Avg FPS: %.3f\n"
		"  Avg Processing Time: %.3f ms\n"
		"  Num Particles: %d\n"
		"-----------------------------------------------\n\n",
		m_avgFPS,
		avgTime,
		particleSystem->GetNumParticles()
	);

	m_accumTimeDiff = 0;
	m_numTimerSamples = 0;
}
