## PURPOSE
This sample demonstrates the differences between processing particles on the CPU (single threaded) and in OpenCL.  On the CPU, the processing time is very fast when there are a small number of particles and gets progressively slower as we add particles.  With OpenCL, the processing time is expected to be more constant despite the number of particles.  The reason is that OpenCL parallelizes the work by distributing the work of updating the particles to as many compute units as it has available.  And latency of transferring data from OpenCL host to device is also factored into that time.


## REQUIREMENTS
- CLcpp
- OpenGL
- [GLFW](http://www.glfw.org)


## CONTROLS
#### Keyboard
- `Escape`: Exit the program.
- `Space`: Switch between CPU processing and OpenCL processing.
- `Enter`: Toggle particle updating.
- `Backspace`: Reset the number of particles to zero.

#### Mouse
Hold the left mouse button and move the mouse.

- `Move left/right`: Rotate camera.
- `Move up/down`: Zoom camera.