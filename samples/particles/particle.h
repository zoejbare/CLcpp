/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

#include "singleton.h"
#include "vector.h"
#include "matrix.h"
#include "particle_processor.h"

#define MAX_PARTICLES 15000


struct ParticleData
{
	Vector4 position;
	Vector4 velocity;
	Vector4 initialVelocity;

	cl_float rotation;
	cl_float rotationVelocity;

	cl_float startingLife;
	cl_float currentLife;
	cl_float size;

	// Needs to be padded out to the next multiple of 16 bytes.
	cl_float padding[3];
};


struct ParticleVertex
{
	Vector4 position;
	Vector4 texCoord;
	Vector4 color;
};


class IParticleProcessor;

class ParticleSystem
	: public ISingleton<ParticleSystem>
{
public:

	ParticleSystem();

	Vector4 GetEmitterPosition() const { return m_emitterPosition; }
	Vector4 GetGravityCenter() const { return m_gravityCenter; }
	cl_float GetMinParticleSize() const { return m_minParticleSize; }
	cl_float GetMaxParticleSize() const { return m_maxParticleSize; }
	cl_float GetMinParticleLife() const { return m_minParticleLife; }
	cl_float GetMaxParticleLife() const { return m_maxParticleLife; }
	cl_float GetGravityStrength() const { return m_gravityStrength; }
	cl_uint GetNumParticles() const { return m_numParticles; }

	void SetProcessor(IParticleProcessor* const processor) { m_particleProcessor = processor; }
	void SetEmitterPosition(const Vector4& position) { m_emitterPosition = position; }
	void SetGravityCenter(const Vector4& position) { m_gravityCenter = position; }
	void SetMinParticleSize(const cl_float minSize) { m_minParticleSize = minSize; }
	void SetMaxParticleSize(const cl_float maxSize) { m_maxParticleSize = maxSize; }
	void SetMinParticleLife(const cl_float minLife) { m_minParticleLife = minLife; }
	void SetMaxParticleLife(const cl_float maxLife) { m_maxParticleLife = maxLife; }
	void SetGravityStrength(const cl_float strength) { m_gravityStrength = strength; }

	void AddParticle(const cl_uint numToAdd)
	{
		cl_uint numRemaining = numToAdd;

		while(numRemaining > 0)
		{
			// Keep adding until we've either exhausted the number we wish to add or we've maxed out the number allowed.
			if(m_numParticles < MAX_PARTICLES)
			{
				m_particleData[m_numParticles].position = m_emitterPosition;
				++m_numParticles;
			}
			else
				break;

			--numRemaining;
		}
	}

	void Reset();
	void Update();
	void Draw();


private:

	IParticleProcessor* m_particleProcessor;

	ParticleData m_particleData[MAX_PARTICLES];
	ParticleVertex m_particleVertex[MAX_PARTICLES * 4];
	cl_uint m_particleIndex[MAX_PARTICLES * 6];

	Vector4 m_emitterPosition;
	Vector4 m_gravityCenter;

	cl_float m_minParticleSize;
	cl_float m_maxParticleSize;
	cl_float m_minParticleLife;
	cl_float m_maxParticleLife;
	cl_float m_gravityStrength;

	cl_uint m_numParticles;
};
