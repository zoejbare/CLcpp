/*******************************************************************************
Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
********************************************************************************/

#pragma once

#include "particle_processor.h"
#include "singleton.h"


class ParticleProcessorOpenCL
	: public ISingleton<ParticleProcessorOpenCL>
	, public IParticleProcessor
{
public:

	ParticleProcessorOpenCL();
	virtual ~ParticleProcessorOpenCL() {}

	bool SetupOpenCL();

	const char* GetName() const;
	void DoWork(ParticleData* const particleDataArray, ParticleVertex* const particleVertexArray, const cl_uint numParticles);


private:

	CLcpp::Platform m_platform;
	CLcpp::Device m_device;
	CLcpp::Context m_context;
	CLcpp::CommandQueue m_commandQueue;

	CLcpp::Program m_program;
	CLcpp::Kernel m_kernel;

	CLcpp::MemBuffer m_particleDataBuffer;
	CLcpp::MemBuffer m_particleVertexBuffer;
	CLcpp::MemBuffer m_constantBuffer;

	bool m_hasPushedVertexData;
};


inline ParticleProcessorOpenCL::ParticleProcessorOpenCL()
	: m_platform()
	, m_device()
	, m_context()
	, m_commandQueue()
	, m_program()
	, m_kernel()
	, m_particleDataBuffer()
	, m_particleVertexBuffer()
	, m_constantBuffer()
	, m_hasPushedVertexData(false)
{
}
