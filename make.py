#!/usr/bin/python3
# Copyright (c) 2013-2016, Zoe J. Bare (zoe.j.bare@gmail.com)
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
#    1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
#
#    2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
#
#    3. This notice may not be removed or altered from any source
#    distribution.
#

import platform
import os
import sys
import csbuild

from csbuild import log

###################################################################################################

oclSdkPath = os.getenv("OPENCL_SDK_PATH", None)

# Verify the OpenCL SDK path since it much be set by the user.  However, on macOS, it'll exist as
# part of a framework, and for Linux, it should be installed via package manager so the path is
# already known to those platforms implicitly.  Windows is the only one that doesn't have a default
# install location.
if platform.system() == "Windows":
	if not oclSdkPath:
		log.Error("Please define OPENCL_SDK_PATH; it should point to the root path of the OpenCL installation directory")
		sys.exit(1)


# If the OpenCL path is provided, make sure it exists.
if oclSdkPath and not os.access(oclSdkPath, os.F_OK):
	log.Error("Path at OPENCL_SDK_PATH (\"{}\") does not exist; please point to a valid OpenCL installation directory".format(oclSdkPath))
	sys.exit(1)

###################################################################################################

with csbuild.Target("debug"):
	csbuild.AddDefines("_DEBUG")
	csbuild.SetDebugLevel(csbuild.DebugLevel.ExternalSymbolsPlus)
	csbuild.SetOptimizationLevel(csbuild.OptimizationLevel.Disabled)
	csbuild.SetDebugRuntime(True)

with csbuild.Target("fastdebug"):
	csbuild.AddDefines("NDEBUG")
	csbuild.SetDebugLevel(csbuild.DebugLevel.ExternalSymbolsPlus)
	csbuild.SetOptimizationLevel(csbuild.OptimizationLevel.Speed)
	csbuild.SetDebugRuntime(True)

with csbuild.Target("release"):
	csbuild.AddDefines("NDEBUG")
	csbuild.SetDebugLevel(csbuild.DebugLevel.Disabled)
	csbuild.SetOptimizationLevel(csbuild.OptimizationLevel.Max)
	csbuild.SetDebugRuntime(False)

###################################################################################################

csbuild.SetUserData("outPath", "{userData.outDirName}/{architectureName}/{targetName}")
csbuild.SetUserData("outDirName", platform.system().lower())

csbuild.SetOutputDirectory("_out/{userData.outPath}")
csbuild.SetIntermediateDirectory("_int/{userData.outPath}")

csbuild.AddIncludeDirectories("include")

with csbuild.Platform("Windows"):
	with csbuild.Architecture("x86"):
		glfwSdkPath = os.path.join(os.getenv("PROGRAMFILES(X86)", ""), "GLFW")
		csbuild.SetUserData("glfwSdkPath", glfwSdkPath)

	with csbuild.Architecture("x64"):
			glfwSdkPath = os.path.join(os.getenv("PROGRAMFILES", ""), "GLFW")
			csbuild.SetUserData("glfwSdkPath", glfwSdkPath)

if oclSdkPath:
	with csbuild.Architecture("x86"):
		csbuild.SetUserData("oclArchName", "x86")

	with csbuild.Architecture("x64"):
		csbuild.SetUserData("oclArchName", "x86_64")

	csbuild.SetUserData("oclSdkPath", oclSdkPath)
	csbuild.AddIncludeDirectories("{userData.oclSdkPath}/include")
	csbuild.AddLibraryDirectories("{userData.oclSdkPath}/lib/{userData.oclArchName}")

###################################################################################################

with csbuild.Toolchain("clang", "gcc"):
	csbuild.AddCompilerCxxFlags("-std=c++11", "-Wall")

with csbuild.Toolchain("gcc"):
	csbuild.AddCompilerCxxFlags("-Wno-ignored-attributes")

with csbuild.Platform("Linux"):
	with csbuild.Toolchain("clang"):
		csbuild.AddLinkerFlags("-stdlib=libstdc++")

with csbuild.Platform("Windows", "Linux"):
	csbuild.AddCompilerCxxFlags("/EHsc", "/W4")
	csbuild.AddLibraries("OpenCL")

# with csbuild.Platform("Darwin"):
# 	csbuild.AddFrameworks("OpenCL")

###################################################################################################

testAppName = "opencl_test"
with csbuild.Project(testAppName, "samples/opencl_test"):
	csbuild.SetOutput(testAppName, csbuild.ProjectType.Application)

###################################################################################################

particlesAppName = "particles"
with csbuild.Project(particlesAppName, "samples/particles"):
	csbuild.SetOutput(particlesAppName, csbuild.ProjectType.Application)

	with csbuild.Platform("Windows"):
		csbuild.AddLibraries("OpenGL32", "glfw3")

	with csbuild.Platform("Linux"):
		csbuild.AddLibraries(
			"GL",
			"glfw",
			"dl",
			"X11",
			"Xxf86vm",
			"Xrandr",
			"Xcursor",
			"Xinerama",
			"pthread",
		)

	# with csbuild.Platform("Darwin"):
	# 	csbuild.AddFrameworks("Foundation", "OpenGL")

###################################################################################################
