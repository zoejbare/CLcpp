
## WHAT IS CLCPP?
CLcpp is intended to be a portable library for moderately simplifying the usage of OpenCL by wrapping its functionality with C++ classes.  The library itself also intends to solve the issue of fragmented versioning across different platforms and OpenCL API distributions.

The underlying `cl_*` objects for each class object can interact seamlessly with the raw OpenCL API and existing `cl_*` objects may be wrapped with CLcpp objects.  This works through two mechanisms, the first of which being that every class in CLcpp corresponding to a `cl_*` object type has an implicit cast operator to its associated type.  For example, this means if you have a `CLcpp::Context` object and need to call a raw `cl*()` function that takes a `cl_context`, you can pass the `CLcpp::Context` object as if it were a `cl_context`.  That enables CLcpp-to-OpenCL cross-functional interaction.

The second mechanism is a special, explicit constructor in each CLcpp class, accepting a single `cl_*` object of the associated type.  This constructor is special in that it assumes ownership of the `cl_*` object, eliminating the need for the user to manage the lifetime of the object.  This enables OpenCL-to-CLcpp cross-functional interaction, making it fairly easy and straight-forward to supplement existing OpenCL code.


## HOW IS IT LICENSED?
CLcpp uses the zlib/libpng license.  See the LICENSE file for more details.


## REQUIREMENTS
The OpenCL API must be installed in order to build and use CLcpp.  OpenCL doesn't come standard with Windows or Linux, though Apple's OSX does have it own version available upon installing the Xcode development suite.  In many cases the internal capabilities of OpenCL with differ depending on which distribution you're using; for example, the AMD distribution will enable OpenCL on your CPU and supported AMD GPUs whereas the Nvidia distribution will enable it on Nvidia chipsets using CUDA as a backend.  Also available is Intel's SDK which enables OpenCL on Core i5 and i7 CPUs and the Xeon CPU in conjunction with the Xeon Phi co-processor, if present.

Another consideration to keep in mind is that the different API distributions may be built against different versions of OpenCL and the versions for a single distribution are not guaranteed to be consistent between different platforms.

Luckily, neither CLcpp nor OpenCL itself actually care which one of these SDKs are installed, so it's more a matter of user preference.  The issue of version differences are covered below in **VERSION SWITCHING**.

As of this writing, these are the links to find the aforementioned SDKs:

- [Intel](http://software.intel.com/en-us/intel-opencl "Intel® SDK for OpenCL™ Applications")
- [Nvidia](https://developer.nvidia.com/cuda-toolkit "CUDA Toolkit")
- [AMD](http://developer.amd.com/tools-and-sdks/opencl-zone "AMD APP SDK")

#### Platform-specific
*Windows*

- Visual Studio 2017 (prior versions down to 2013 will also work, but earlier versions will not due to poor C++11 support)

*Linux*

- clang

*macOS*

- Xcode 


## COMPILING INSTRUCTIONS
As of CLcpp 2.0, the build system has been unified for all platforms using csbuild.  For more information about csbuild, visit the [csbuild homepage](http://www.csbuild.org).

### INSTALLING CSBUILD
Download csbuild from the mainline branch at [Github](https://github.com/zoejbare/csbuild2).  Then, from the csbuild root directory, run:

*Windows*

	python.exe setup.py sdist install

*Linux/macOS*

	sudo python setup.py sdist install

### COMPILING CLCPP SAMPLES
From the root CLcpp directory, run the following command from the command line:

*Windows*

	python.exe make.py

*Linux/macOS*

	./make.py

The build script relies on the environment variable `OPENCL_SDK_PATH` being set, but only for Windows.  For other platforms, it is not needed if OpenCL is already available through the system path.  When present, it should point to the directory containing the OpenCL library file (`OpenCL.lib` for Windows, `libOpenCL.so` for Linux).

Additional arguments may be used to further configure the build.  For a full list of available command line arguments, run with the `--help` argument.


## VERSION SWITCHING
The problem of using an older version of OpenCL with the current version of CLcpp is one that CLcpp was re-engineered to easily manage starting with version 2.0.  For instance, if your SDK only supports OpenCL 1.1, you would tell CLcpp to only compile features up to OpenCL 1.1 by opening `CLcpp_internal.h` and changing `#define CL_CPP_ENABLE_OPENCL_1_2 1` to `#define CL_CPP_ENABLE_OPENCL_1_2 0` or removing that line entirely, then rebuilding the library.  This will effectively disable inclusion of any feature from OpenCL 1.2 and up.  The same principle may be applied to any version of OpenCL exception OpenCL 1.0.  The reason for this is that non-deprecated features of OpenCL 1.0 are assumed to be implicitly supported across all versions and therefore cannot be disabled. 